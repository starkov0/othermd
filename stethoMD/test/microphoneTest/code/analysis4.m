[data,Fs] = audioread('../sound/YES.m4a');

a = 2.925*10^5;
b = 2.955 *10^5;
c = b + (b-a);

[Frequency, abs_FFT] = time2FFT(data, 1/Fs);

figure;
subplot(3,1,1);
semilogx(Frequency,abs_FFT);
xlabel('Frequency [Hz]');
ylabel('Normalized Weight of Frequency');

[Frequency, abs_FFT] = time2FFT(data(a:b), 1/Fs);
subplot(3,1,2);
semilogx(Frequency,abs_FFT);
xlabel('Frequency [Hz]');
ylabel('Normalized Weight of Frequency');

[Frequency, abs_FFT] = time2FFT(data(b:c), 1/Fs);
subplot(3,1,3);
semilogx(Frequency,abs_FFT);
xlabel('Frequency [Hz]');
ylabel('Normalized Weight of Frequency');