% load data
[yes, Fs] = audioread('../sound/YES.m4a');
% [no, Fs] = audioread('../sound/NO.m4a');

% sound 2
s_start = 259900;
s_end = 262400;
sound = yes(s_start:s_end);

T = 40/size(sound,1);
soundT = round(size(sound,1) * T);
yesT = round(size(yes,1) * T);

fft_yes = fft(yes);
fft_yes_test = fft(yes);
fft_sound = fft(sound);
fft_sound_test = fft(sound);

fft_sound_test(soundT:end-soundT) = 0;
% fft_yes_test(yesT:end-yesT) = 0;

ifft_sound_test = real(ifft(fft_sound_test));
ifft_yes_test = real(ifft(fft_yes_test));

% figure;
% subplot(221);
% plot(sound);
% subplot(222);
% plot(ifft_sound_test);
% subplot(223);
% plot(real(fft_sound));
% subplot(224);
% plot(real(fft_sound_test));
% 
% figure;
% subplot(221);
% plot(yes);
% subplot(222);
% plot(ifft_yes_test);
% subplot(223);
% plot(real(fft_yes));
% subplot(224);
% plot(real(fft_yes_test));

h = 30;
for i=0:(size(ifft_yes_test,1)-2)/h
    ifft_yes_test((i*h)+1:(i+1)*h) = median(ifft_yes_test((i*h)+1:(i+1)*h));
end

audiowrite('../sound/YES_test.m4a', ifft_yes_test1, Fs);

