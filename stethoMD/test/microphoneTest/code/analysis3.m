%% compute
[yes,Fs] = audioread('../sound/YES.m4a');

% low pass filter
% order = 2;
% threshold = 0.25;
% [bb,aa] = butter(order,threshold,'low');
% yes = filter(bb,aa,yes);

% high pass filter
% order = 2;
% threshold = 0.0001;
% [bb,aa] = butter(order,threshold,'high');
% yes = filter(bb,aa,yes);

m_yes = max(abs(yes));
yes = yes / m_yes;

%% display
figure;
subplot(311); hold on
plot(yes);
title('POLYMD - Auscultation sound', 'FontSize', 20)

%
L = length(yes);
NFFT = 2^nextpow2(L);
yes_fft = fft(yes,NFFT)/L;
f = Fs/2*linspace(0,1,NFFT/2+1);

subplot(312);
semilogx(f,2*abs(yes_fft(1:NFFT/2+1)), 'r') 
title('POLYMD - FFT of cardiac sound', 'FontSize', 20)

%
yes_2 = yes(b:c);
L = length(yes_2);
NFFT = 2^nextpow2(L);
yes_2_fft = fft(yes_2,NFFT)/L;
f_2 = Fs/2*linspace(0,1,NFFT/2+1);

subplot(313);
semilogx(f_2,2*abs(yes_2_fft(1:NFFT/2+1)), 'g') 
title('POLYMD - FFT of no cardiac sound', 'FontSize', 20)