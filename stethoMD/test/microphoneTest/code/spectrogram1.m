[s1,~] = audioread('../sound/song1.mp3');
[s2,~] = audioread('../sound/song2.mp3');
[s3,s] = audioread('../sound/YES.m4a');

s1 = s1(1:100000);
s2 = s2(1:100000);
s3 = s3(1:100000);


figure;
spectrogram(s1,128);

figure;
spectrogram(s2,128);

figure;
spectrogram(s3,128);


