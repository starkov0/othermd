function [Frequency, abs_FFT] = time2FFT(Data, SaT)
% 
% This function provides to calculates the Frequency axis and the absolute
% value of the FFT of a certain data vector
%
% [Frequency, abs_FFT] = time2FFT(Data, SaT);
%
% The function takes as INPUT the Data vector and the sampling time SaT in second and
% gives as OUTPUT the Frequency axis and the absolute value of the FFT

L = length(Data); % length
SaF = 1/SaT;

if L/2 ~= floor(L/2); %It is the same to say it is odd
    L = L+1;
end

Frequency = (SaF/L) * (0:((L/2)-1));
Data = Data - mean(Data);
abs_FFT = abs(fft(Data));
abs_FFT = abs_FFT(1:L/2)/max(abs_FFT);