%% compute
[yes,Fs] = audioread('../sound/YES.m4a');

% low pass filter
% order = 2;
% threshold = 0.25;
% [bb,aa] = butter(order,threshold,'low');
% yes = filter(bb,aa,yes);

% high pass filter
% order = 2;
% threshold = 0.0001;
% [bb,aa] = butter(order,threshold,'high');
% yes = filter(bb,aa,yes);

m_yes = max(abs(yes));
yes = yes / m_yes;

a = 2.925*10^5;
b = 2.955 *10^5;
c = b + (b-a);

yes_b = yes;
yes_c = yes;

yes_b(1:a) = nan;
yes_b(b:end) = nan;

yes_c(1:b) = nan;
yes_c(c:end) = nan;

%% display
figure;
subplot(311); hold on
plot(yes);
plot(yes_b, 'r');
plot(yes_c, 'g');
title('POLYMD - Auscultation sound in blue - cardiac sound in red - no cardiac sound in green', 'FontSize', 20)

%
yes_1 = yes(a:b);
L = length(yes_1);
NFFT = 2^nextpow2(L);
yes_1_fft = fft(yes_1,NFFT)/L;
f_1 = Fs/2*linspace(0,1,NFFT/2+1);

subplot(312);
semilogx(f_1,2*abs(yes_1_fft(1:NFFT/2+1)), 'r') 
title('POLYMD - FFT of cardiac sound', 'FontSize', 20)

%
yes_2 = yes(b:c);
L = length(yes_2);
NFFT = 2^nextpow2(L);
yes_2_fft = fft(yes_2,NFFT)/L;
f_2 = Fs/2*linspace(0,1,NFFT/2+1);

subplot(313);
semilogx(f_2,2*abs(yes_2_fft(1:NFFT/2+1)), 'g') 
title('POLYMD - FFT of no cardiac sound', 'FontSize', 20)