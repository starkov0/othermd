%% load data
[yes,Fs] = audioread('../sound/littmann1.wav');

%% segmentation
s_start = [2.2,3.6,6.2,7.9,10.9]*10^4; 
s_end = [2.45,3.9,6.5,8.1,11.3]*10^4;

%% filter
[b,a] = butter(2,0.003,'high');
f_yes = filter(b,a,yes);

m_yes = max(abs(f_yes));
f_yes = f_yes / m_yes;

figure;
subplot(211); hold on
plot(yes);
subplot(212); hold on
plot(f_yes);

% for i=1:size(s_start,2)
%     plot(s_start(i),0,'*r');
%     plot(s_end(i),0,'*r');
% end

audiowrite('../sound/littmann1_filter1.wav', f_yes*30, Fs);

a = 2.85*10^4;
b = 2.95*10^4;

yes_bis = yes;
yes_bis(1:a) = nan;
yes_bis(b:end) = nan;

figure;
subplot(211); hold on
plot(yes);
plot(yes_bis, 'r');
title('Auscultation sound in blue - cardiac B2 sound in red', 'FontSize', 20)

subplot(212); hold on
plot(abs(fft(yes(a:b))), 'r')
title('FFT of the cardiac B2 sound in red', 'FontSize', 20)
