% load data
yes = audioread('../sound/YES.m4a');
no = audioread('../sound/NO.m4a');

% sound1
s_start = 259900;
s_end = 262400;
sound = yes(s_start:s_end);
fft_sound1 = fft(sound);
fft_sound2 = fft(sound);
fft_yes = fft(yes);
s_mean = mean(sound); s_var = var(sound); s_entropy = entropy(sound);

% figure;
% subplot(311); hold on 
% plot(yes);
% plot(s_start, yes(s_start), 'r*', 'LineWidth', 2);
% plot(s_end, yes(s_end), 'r*', 'LineWidth', 2);
% title(['Sound - mean=', num2str(s_mean),' - var=',num2str(s_var),' - entropy=', num2str(s_entropy)], 'FontSize', 20)
% subplot(312);
% hist(sound,100);
% title('Sound Histogram', 'FontSize', 20)
% subplot(313);
% plot(abs(fft_sound));
% title('Sound FFT Amplitude', 'FontSize', 20)

fft_sound1(1:end/2) = 0;
fft_sound2(end/2:end) = 0;
ifft_sound1 = real(ifft(fft_sound1));
ifft_sound2 = real(ifft(fft_sound2));

figure;
subplot(231);
plot(sound);
subplot(232);
plot(ifft_sound1);
subplot(233);
plot(ifft_sound2);
subplot(212); hold on
plot(abs(fft_sound));

% sound 2
s_start = 259900;
s_end = 262400;
sound = yes(s_start:s_end);

fft_sound1 = fft(sound);
fft_sound2 = fft(sound);
fft_sound(100:end-100) = 0;
ifft_sound = real(ifft(fft_sound));

figure;
subplot(221);
plot(sound);
subplot(222);
plot(ifft_sound);
subplot(212); hold on
plot(abs(fft_sound));

