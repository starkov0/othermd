% load data
yes = audioread('../sound/YES.m4a');
no = audioread('../sound/NO.m4a');

% subplot(2,1,1);
% plot(yes);
% subplot(3,1,3);
% plot(no);

%% noise1
n_start = 300000;
n_end = 400000;
noise = no(n_start:n_end);
fft_noise1 = fft(noise);
fft_noise2 = fft(noise);
n_mean = mean(noise); n_var = var(noise); n_entropy = entropy(noise);

% figure;
% subplot(311); hold on
% plot(no);
% plot(n_start, no(n_start), 'r*', 'LineWidth', 2);
% plot(n_end, no(n_end), 'r*', 'LineWidth', 2);
% title(['Noise - mean=', num2str(n_mean),' - var=',num2str(n_var),' - entropy=', num2str(n_entropy)], 'FontSize', 20)
% subplot(312);
% hist(noise,100);
% title('Noise Histogram', 'FontSize', 20)
% subplot(313);
% plot(abs(fft_noise));
% title('Noise FFT Amplitude', 'FontSize', 20)

fft_noise1(end/2:end) = 0;
fft_noise2(1:end/2) = 0;
ifft_noise1 = ifft(fft_noise1);
ifft_noise2 = ifft(fft_noise2);

figure;
subplot(221);
plot(abs(ifft_noise1));
subplot(222);
plot(abs(ifft_noise1));
subplot(212);
plot(noise);
