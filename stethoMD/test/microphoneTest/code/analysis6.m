%% 
[data_1,Fs_1] = audioread('../sound/white noise small.wav');
[data_2,Fs_2] = audioread('../sound/littmann_noise.wav');

order = 2;
threshold = 0.004;
[bb,aa] = butter(order,threshold,'low');
data_1 = filtfilt(bb,aa,data_1);

a_1 = 0.3*1e5;%2.925*10^5;
b_1 = 0.7*1e5;%;2.955 *10^5;
c_1 = b_1 + (b_1-a_1);

a_2 = 2.85*10^4;
b_2 = 2.95*10^4;
c_2 = b_2 + (b_2-a_2);

data_1_ab = data_1;
data_1_bc = data_1;
data_2_ab = data_2;
data_2_bc = data_2;

data_1_ab(1:a_1) = nan;
data_1_ab(b_1:end) = nan;
data_2_ab(1:a_2) = nan;
data_2_ab(b_2:end) = nan;

data_1_bc(1:b_1) = nan;
data_1_bc(c_1:end) = nan;
data_2_bc(1:b_2) = nan;
data_2_bc(c_2:end) = nan;

%% 
figure;
subplot(211); hold on
plot(data_1);
plot(data_1_ab, 'r');
plot(data_1_bc, 'g');
subplot(212); hold on
plot(data_2);
plot(data_2_ab, 'r');
plot(data_2_bc, 'g');

%% 
[Frequency_1, abs_FFT_1] = time2FFT(data_1, 1/Fs_1);
[Frequency_2, abs_FFT_2] = time2FFT(data_2, 1/Fs_2);

figure;
subplot(3,1,1);
semilogx(Frequency_1,abs_FFT_1, 'k');
hold on
semilogx(Frequency_2,abs_FFT_2, 'r');
xlabel('Frequency [Hz]');
ylabel('Normalized Weight of Frequency');

[Frequency_1, abs_FFT_1] = time2FFT(data_1(a_1:b_1), 1/Fs_1);
[Frequency_2, abs_FFT_2] = time2FFT(data_2(a_2:b_2), 1/Fs_2);

subplot(3,1,2);
semilogx(Frequency_1,abs_FFT_1, 'k');
hold on
semilogx(Frequency_2,abs_FFT_2, 'r');
xlabel('Frequency [Hz]');
ylabel('Normalized Weight of Frequency');

[Frequency_1, abs_FFT_1] = time2FFT(data_1(b_1:c_1), 1/Fs_1);
[Frequency_2, abs_FFT_2] = time2FFT(data_2(b_2:c_2), 1/Fs_2);

subplot(3,1,3);
semilogx(Frequency_1,abs_FFT_1, 'k');
hold on
semilogx(Frequency_2,abs_FFT_2, 'r');
xlabel('Frequency [Hz]');
ylabel('Normalized Weight of Frequency');