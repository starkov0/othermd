[data,Fs] = audioread('../sound/littmann1.wav');

a = 2.85*10^4;
b = 2.95*10^4;
c = b + (b-a);

[Frequency, abs_FFT] = time2FFT(data, 1/Fs);

figure;
subplot(3,1,1);
semilogx(Frequency,abs_FFT);
xlabel('Frequency [Hz]');
ylabel('Normalized Weight of Frequency');

[Frequency, abs_FFT] = time2FFT(data(a:b), 1/Fs);
subplot(3,1,2);
semilogx(Frequency,abs_FFT);
xlabel('Frequency [Hz]');
ylabel('Normalized Weight of Frequency');

[Frequency, abs_FFT] = time2FFT(data(b:c), 1/Fs);
subplot(3,1,3);
semilogx(Frequency,abs_FFT);
xlabel('Frequency [Hz]');
ylabel('Normalized Weight of Frequency');