%% load data
[yes,Fs] = audioread('../sound/YES.m4a');

%% segmentation
s_start = [2.2,3.6,6.2,7.9,10.9]*10^4; 
s_end = [2.45,3.9,6.5,8.1,11.3]*10^4;

%% filter
[b,a] = butter(2,0.001,'low');
f_yes = filter(b,a,yes);

m_yes = max(abs(f_yes));
f_yes = f_yes / m_yes;

figure;
subplot(211); hold on
plot(yes);

subplot(212); hold on
plot(f_yes);
% for i=1:size(s_start,2)
%     plot(s_start(i),0,'*r');
%     plot(s_end(i),0,'*r');
% end

audiowrite('../sound/YES_filter1.m4a', f_yes, Fs);