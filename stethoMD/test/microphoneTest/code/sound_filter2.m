%% load data
[yes,Fs] = audioread('../sound/YES.m4a');

%% parameters
order = 2;
% threshold_list = linspace(0.00005,0.001,20);
threshold_list = 0.0001;

%% filter
for threshold=threshold_list

    [b,a] = butter(order,threshold,'low');
    f_yes = filter(b,a,yes);

    %% normalize
    m_yes = max(abs(f_yes));
    f_yes = f_yes / m_yes;

    %% display
%     figure;
%     subplot(211); hold on
%     plot(yes);
%     subplot(212); hold on
%     plot(f_yes);

    %% write
    audiowrite(['../sound/YES_f','_',num2str(order),'_',num2str(threshold),'.m4a'], f_yes, Fs);
end



