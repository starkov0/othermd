//
//  GraphView.h
//  stathoApp
//
//  Created by Pierre Starkov on 16/07/14.
//  Copyright (c) 2014 Pierre Starkov. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AudioContainer;

@interface GraphView : UIView {
    long x0;
    long x1;
    long y0;
    long y1;
    long currentIndex;
    float lineWidth;
    
    UIColor *strokeColor;
    UIColor *fillColor;
    
    AudioContainer *audioContainer;
}

@property long x0;
@property long x1;
@property long y0;
@property long y1;
@property long currentIndex;
@property float lineWidth;

@property (retain) UIColor *strokeColor;
@property (retain) UIColor *fillColor;

@property (retain) AudioContainer *audioContainer;

@end
