//
//  ArrayStatisctics.mm
//  pierre starkov - plug'n'scope - 2014


#import "ArrayStatistics.h"

@implementation ArrayStatistics

+ (NSNumber *)sum:(NSMutableArray*)array {
    NSNumber *sum = [array valueForKeyPath:@"@sum.self"];
    return sum;
}

+ (NSNumber *)mean:(NSMutableArray*)array {
    NSNumber *mean = [array valueForKeyPath:@"@avg.self"];
    return mean;
}

+ (NSNumber *)min:(NSMutableArray*)array {
    NSNumber *min = [array valueForKeyPath:@"@min.self"];
    return min;
}

+ (NSNumber *)max:(NSMutableArray*)array {
    NSNumber *max = [array valueForKeyPath:@"@max.self"];
    return max;
}

+ (NSNumber *)median:(NSMutableArray*)array {
    NSArray *sortedArray = [array sortedArrayUsingSelector:@selector(compare:)];
    NSNumber *median;
    if (sortedArray.count != 1) {
        if (sortedArray.count % 2 == 0) {
            median = @(([[sortedArray objectAtIndex:sortedArray.count / 2] integerValue]) + ([[sortedArray objectAtIndex:sortedArray.count / 2 + 1] integerValue]) / 2);
        }
        else {
            median = @([[sortedArray objectAtIndex:sortedArray.count / 2] integerValue]);
        }
    }
    else {
        median = [sortedArray objectAtIndex:1];
    }
    return median;
}

+ (NSNumber *)standardDeviation:(NSMutableArray*)array {
    double sumOfDifferencesFromMean = 0;
    for (NSNumber *score in array) {
        sumOfDifferencesFromMean += pow(([score doubleValue] - [[self mean:array] doubleValue]), 2);
    }
    NSNumber *standardDeviation = @(sqrt(sumOfDifferencesFromMean / [array count]));
    return standardDeviation;
}

@end
