//
//  GraphView.m
//  stathoApp
//
//  Created by Pierre Starkov on 16/07/14.
//  Copyright (c) 2014 Pierre Starkov. All rights reserved.
//

#import "GraphView.h"
#import "AudioContainer.h"

@implementation GraphView

@synthesize audioContainer;
@synthesize x0;
@synthesize x1;
@synthesize y0;
@synthesize y1;
@synthesize currentIndex;
@synthesize lineWidth;
@synthesize fillColor;
@synthesize strokeColor;

-(void) awakeFromNib {
    
    // alloc & init array to display
    
    // init view cosmetic options
    self.backgroundColor = [UIColor whiteColor];
    [self setStrokeColor:[UIColor blueColor]];
    [self setFillColor:[UIColor blueColor]];
    lineWidth = 1;
    
    // init display options
    [self setX0:0];
    [self setX1:100000];
    [self setY0:-1];
    [self setY1:1];
}



@end
