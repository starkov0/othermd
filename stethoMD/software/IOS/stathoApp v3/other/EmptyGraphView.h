//
//  GraphView.h
//  DynamicGraphView

#import <UIKit/UIKit.h>
#import "GraphView.h"

@interface EmptyGraphView : UIView {
    
    NSMutableArray *pointArray;
    NSMutableArray *positiveDisplayArray;
    NSMutableArray *negativeDisplayArray;
    
    float dxBegin;
    float dxEnd;
    float currentPosition;
    float dy;
    
    int lineWidth;
    
    BOOL fillGraph;
    
    UIColor *strokeColor;
    UIColor *fillColor;
    
    UIColor *zeroLineStrokeColor;
    
    int granularity;
}

@property (retain) NSMutableArray *pointArray;
@property (retain) NSMutableArray *positiveDisplayArray;
@property (retain) NSMutableArray *negativeDisplayArray;
@property float dxBegin;
@property float dxEnd;
@property float dy;
@property float currentPosition;
@property int lineWidth;
@property BOOL fillGraph;
@property (retain) UIColor *strokeColor;
@property (retain) UIColor *fillColor;
@property (retain) UIColor *zeroLineStrokeColor;
@property int granularity;

@end
