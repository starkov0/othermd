//
//  ArrayStatisctics.h
//  pierre starkov - plug'n'scope - 2014


#import <Foundation/Foundation.h>

@interface ArrayStatistics : NSObject

+ (NSNumber *)sum:(NSMutableArray*)array;
+ (NSNumber *)mean:(NSMutableArray*)array;
+ (NSNumber *)min:(NSMutableArray*)array;
+ (NSNumber *)max:(NSMutableArray*)array;
+ (NSNumber *)median:(NSMutableArray*)array;
+ (NSNumber *)standardDeviation:(NSMutableArray*)array;

@end
