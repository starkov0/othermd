//
//  AudioContainer.mm
//  pierre starkov - plug'n'scope - 2014

#import "AudioContainer.h"
#import "AudioController.h"

@interface AudioContainer () {
    
    // audioBuffer
    NSMutableArray *_audioBuffer;
    NSMutableArray *_waitingBuffer;
    NSString *_audioType;
    
    // file variables
    int         _currentFrame;
    float       _totalDuration;
    int         _frameSize;
}
@end

@implementation AudioContainer

//---------------------------------------------------------- INIT

+(AudioContainer*) audioContainer {
    return [[AudioContainer alloc] init];
}

-(id)init {
    self = [super init];
    
    _audioBuffer    = [[NSMutableArray alloc] init];
    _waitingBuffer  = [[NSMutableArray alloc] init];
    _frameSize = 512;
    _audioType = [[NSString alloc] init];
    
    return self;
}

//---------------------------------------------------------- POSITION

-(int) totalFrames {
    return (int)[_audioBuffer count];
}

-(float) totalDuration {
    return _totalDuration;
}

-(int) currentFrame {
    @synchronized (self) {
        return _currentFrame;
    }
}

-(int) frameSize {
    return _frameSize;
}

-(void) setCurrentFrame:(int)currentFrame {
    @synchronized (self) {
        _currentFrame = round(currentFrame / _frameSize) * _frameSize;
        if (_currentFrame < 0) {
            _currentFrame = 0;
        }
        else if (_currentFrame > [_audioBuffer count]) {
            _currentFrame = (int)[_audioBuffer count];
        }
    }
}

//---------------------------------------------------------- AUDIO TYPE

-(NSString*) audioType {
    return _audioType;
}

//---------------------------------------------------------- AUDIO

-(void) startInputAudioWithType:(NSString*)type {
    @synchronized (self) {
        [self _cleanBuffers];
        _audioType = type;
    }
}

-(void) startOutputAudioWithAudioArray:(NSMutableArray*)array
                         totalDuration:(float)totalDuration {
    @synchronized (self) {
        // buffers
        [self _cleanBuffers];
        [_audioBuffer addObjectsFromArray:array];
        _audioType = @"read";
        _currentFrame = 0;
        _totalDuration = totalDuration;
    }
}

-(void) addInputAudio:(float)audio
               withX0:(int)x0
                   x1:(int)x1
            viewWidth:(int)viewWidth {
    @synchronized (self) {
        [_waitingBuffer addObject:@(audio)];
        [self  updateInputBuffersWithX0:x0 x1:x1 viewWidith:viewWidth];
    }
}

-(void) fillOutputAudio:(NSMutableArray*)outputAudio {
    @synchronized (self) {
        if (_currentFrame < [_audioBuffer count]) {
            for (int i=_currentFrame; i<_currentFrame+_frameSize; i++) {
                [outputAudio addObject:[_audioBuffer objectAtIndex:i]];
            }
            _currentFrame += _frameSize;
        }
    }
}

-(void) fillDrawableAudio:(NSMutableArray*)drawableBuffer
                   withX0:(int)x0
                       x1:(int)x1
                viewWidth:(int)viewWidth {
    @synchronized (self) {
        // init first variables
        int k0 = x0;
        int kI = k0;
        int i = 0;
        int dx = (int)round((x1 - x0) / viewWidth);
        int bound = MIN((int)[_audioBuffer count], x1);
        // get total displayable audio
        while( kI<bound ) {
            [drawableBuffer addObject:[_audioBuffer objectAtIndex:kI]];
            kI = round(k0 + i*dx);
            i++;
        }
    }
}

//---------------------------------------------------------- UPDATE BUFFERS

-(void) updateInputBuffersWithX0:(int)x0
                              x1:(int)x1
                      viewWidith:(int)viewWidth {
    @synchronized (self) {
        [self _swapFromAudioToWaitingWithX0:x0 x1:x1 viewWidith:viewWidth];
        [self _swapFromWaitingToAudioWithX0:x0 x1:x1 viewWidith:viewWidth];
        [self _normalizeBuffersWithX0:x0 x1:x1 viewWidith:viewWidth];
    }
}

-(void)_swapFromAudioToWaitingWithX0:(int)x0
                                  x1:(int)x1
                          viewWidith:(int)viewWidth {
    
    int dx = (int)round((x1 - x0) / viewWidth);
    int nbToKeep = (int)(floor([_audioBuffer count] / dx) * dx);
    for (int i=nbToKeep+1; i<[_audioBuffer count]; i++) {
        [_waitingBuffer addObject:[_audioBuffer objectAtIndex:nbToKeep+1]];
        [_audioBuffer removeObjectAtIndex:nbToKeep+1];
    }
}

-(void)_swapFromWaitingToAudioWithX0:(int)x0
                                  x1:(int)x1
                          viewWidith:(int)viewWidth {
    
    int dx = (int)round((x1 - x0) / viewWidth);
    int nbToSwap = (int)(floor([_waitingBuffer count] / dx) * dx);
    for (int i=0; i<nbToSwap; i++) {
        [_audioBuffer addObject:[_waitingBuffer objectAtIndex:0]];
        [_waitingBuffer removeObjectAtIndex:0];
    }
}

-(void)_normalizeBuffersWithX0:(int)x0
                            x1:(int)x1
                    viewWidith:(int)viewWidth {
    
    int dx = (int)round((x1 - x0) / viewWidth);
    while ([_audioBuffer count] > viewWidth*dx) {
        [_audioBuffer removeObjectAtIndex:0];
    }
}


//---------------------------------------------------------- CLEAN BUFFERS

-(void)_cleanBuffers {
    [_audioBuffer removeAllObjects];
    [_waitingBuffer removeAllObjects];
}

//---------------------------------------------------------- DEALLOC

-(void) dealloc {
    
    [self _cleanBuffers];
    [_audioType release];
    [_audioBuffer release];
    [_waitingBuffer release];
    [super dealloc];
}

@end