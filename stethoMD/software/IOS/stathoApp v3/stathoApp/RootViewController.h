//
//  RootViewController.h
//  pierre starkov - plug'n'scope - 2014


#import <UIKit/UIKit.h>
#import "GraphView.h"
#import "AudioController.h"
#import "AudioContainer.h"
#import "TotalView.h"
#import "ViewContainer.h"


///-----------------------------------------------------------
/// @name Classe
///-----------------------------------------------------------

@interface RootViewController : UIViewController

/**
 * graph view
 */
@property (assign, nonatomic) IBOutlet GraphView *graphView;

/**
 * total view
 */
@property (assign, nonatomic) IBOutlet TotalView *totalView;

@property (assign, nonatomic) IBOutlet UISlider *zoomYSlider;

/**
 * file duration
 */
@property (assign, nonatomic) IBOutlet UILabel *timeLabel;
@property (assign, nonatomic) IBOutlet UILabel *currentTaskLabel;

///-----------------------------------------------------------
/// @name Events
///-----------------------------------------------------------

-(void) startUpdateTimeLabel;
-(void) stopUpdateTimeLabel;

/**
 * button listen pressed
 */
- (IBAction)listen;

/**
 * button playPause pressed
 */
- (IBAction)read;

/**
 * button write pressed
 */
- (IBAction)write;

/**
 * zoom X gesture
 */
- (IBAction)graphViewPinchGesture:(UIPinchGestureRecognizer *)sender;

/**
 * translate gesture
 */
- (IBAction)graphViewPanGesture:(UIPanGestureRecognizer *)sender;

/**
 * stop animation + stop listening
 */
- (IBAction)graphViewTapGesture:(UITapGestureRecognizer *)sender;

/**
 * set view index gesture
 */
- (IBAction)graphViewTapTwoGesture:(UITapGestureRecognizer *)sender;

/**
 * translation
 */
- (IBAction)totalViewPanGesture:(UIPanGestureRecognizer *)sender;

/**
 * stop animation + stop listening
 */
- (IBAction)totalViewTapGesture:(UITapGestureRecognizer *)sender;

/**
 * set view index gesture
 */
- (IBAction)totalViewTwoTapGesture:(UITapGestureRecognizer *)sender;

/**
 * zoom Y
 */
- (IBAction)zoomY:(UISlider *)sender;

/**
 * filter
 */
- (IBAction)filter;

/**
 * analyse
 */
- (IBAction)analyse;

@end
