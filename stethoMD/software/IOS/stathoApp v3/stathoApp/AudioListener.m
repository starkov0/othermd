//
//  AudioListener.m
//  inspired from EZAudio
//  pierre starkov - plug'n'scope - 2014

#import "Audio.h"
#import "AudioListener.h"

/// Buses
static const AudioUnitScope audioOutputBus = 0;
static const AudioUnitScope audioInputBus  = 1;

/// Flags
static const UInt32 disableFlag = 0;
static const UInt32 enableFlag  = 1;

@interface AudioListener (){
    /// Internal
    BOOL _isConfigured;
    BOOL _isFetching;
    
    /// Stream Description
    AudioStreamBasicDescription streamFormat;
    
    /// Audio Graph and Listener/Output Units
    AudioUnit _audioListenerUnit;
    
    /// Audio Buffers
    AudioBufferList *_audioListenerBuffer;
    
    /// Device Parameters
    Float64 _deviceSampleRate;
    Float32 _deviceBufferDuration;
    UInt32  _deviceBufferFrameSize;
}
@end

@implementation AudioListener
@synthesize audioListenerDelegate = _audioListenerDelegate;

//---------------------------------------------------------- CALLBACK

static OSStatus listenerCallback(void                          *inRefCon,
                              AudioUnitRenderActionFlags    *ioActionFlags,
                              const AudioTimeStamp          *inTimeStamp,
                              UInt32                        inBusNumber,
                              UInt32                        inNumberFrames,
                              AudioBufferList               *ioData ) {
    
    [Audio freeBufferList:ioData];
    
    // Render audio into buffer
    AudioListener *audioListener = (__bridge AudioListener*)inRefCon;
    OSStatus      result;
    result = AudioUnitRender(audioListener->_audioListenerUnit,
                             ioActionFlags,
                             inTimeStamp,
                             inBusNumber,
                             inNumberFrames,
                             audioListener->_audioListenerBuffer);
    
    // Call delegate class' function
    if( audioListener.audioListenerDelegate ){
        if( [audioListener.audioListenerDelegate respondsToSelector:@selector(audioListener:withBufferList:withBufferSize:)] ){
            [audioListener.audioListenerDelegate audioListener:audioListener
                                                withBufferList:audioListener->_audioListenerBuffer
                                                withBufferSize:inNumberFrames];
        }
    }
    return result;
}

//---------------------------------------------------------- EVENTS

-(void)start {
    if( !_isFetching ){
        // Start fetching Listener
        [Audio checkResult:AudioOutputUnitStart(self->_audioListenerUnit)
                 operation:"AudioListener failed to start fetching audio"];
        _isFetching = YES;
    }
}

-(void)stop {
    // Stop fetching Listener data
    if( _isConfigured ){
        if( _isFetching ){
            [Audio checkResult:AudioOutputUnitStop(self->_audioListenerUnit)
                     operation:"AudioListener failed to stop fetching audio"];
            _isFetching = NO;
        }
    }
}

//---------------------------------------------------------- INIT

-(AudioListener *)initWithAudioListenerDelegate:(id<AudioListenerDelegate>)audioListenerDelegate {
    self = [super init];
    if(self){
        self.audioListenerDelegate = audioListenerDelegate;
        // We're not fetching anything yet
        _isConfigured = NO;
        _isFetching   = NO;
        if( !_isConfigured ){
            // Create the Listener audio graph
            [self _configureListener];
            // We're configured meow
            _isConfigured = YES;
        }
    }
    return self;
}

+(AudioListener *)audioListenerWithDelegate:(id<AudioListenerDelegate>)audioListenerDelegate {
    return [[AudioListener alloc] initWithAudioListenerDelegate:audioListenerDelegate];
}

//---------------------------------------------------------- CONFIG

-(void)_configureListener {
    
    // Create the audio listener unit
    [self _configureAudioListenerUnit];
    
    // Enable Listener Scope
    [self _enableInputScope];

    // Configure device and pull hardware specific buffer duration (default = 0.0232)
    _deviceBufferDuration = [self _configureDeviceBufferDurationWithDefault:0.0232];

    // Configure the stream format with the hardware sample rate
    [self _configureStreamFormatWithSampleRate];
    
    // Create the audio buffer list and pre-malloc the buffers in the list
    [self _configureAudioBufferList];
    
    // Setup Listener callback
    [self _configureListenerCallback];
    
    // Disable buffer allocation (optional - do this if we want to pass in our own)
    [self _disableCallbackBufferAllocation];
    
    // Initialize the audio unit
    [Audio checkResult:AudioUnitInitialize( _audioListenerUnit )
               operation:"Couldn't initialize the Listener unit"];
}

-(void)_configureAudioListenerUnit {
    
    // Create an Listener component description for mic Listener
    AudioComponentDescription audioComponentDescription;
    audioComponentDescription.componentType             = kAudioUnitType_Output;
    audioComponentDescription.componentManufacturer     = kAudioUnitManufacturer_Apple;
    audioComponentDescription.componentFlags            = 0;
    audioComponentDescription.componentFlagsMask        = 0;
    audioComponentDescription.componentSubType          = kAudioUnitSubType_RemoteIO;
    
    // Try and find the component
    AudioComponent AudioComponent = AudioComponentFindNext( NULL , &audioComponentDescription );
    NSAssert(AudioComponent,@"Couldn't get Listener component unit!");

    [Audio checkResult:AudioComponentInstanceNew(AudioComponent,
                                                   &_audioListenerUnit )
               operation:"Couldn't open component for AudioListener Listener unit."];
}

-(void)_enableInputScope {
    [Audio checkResult:AudioUnitSetProperty(_audioListenerUnit,
                                            kAudioOutputUnitProperty_EnableIO,
                                            kAudioUnitScope_Input,
                                            audioInputBus,
                                            &enableFlag,
                                            sizeof(enableFlag))
             operation:"Couldn't enable Listener on I/O unit."];
}

-(Float64)_configureDeviceSampleRateWithDefault:(float)defaultSampleRate {
    Float64 hardwareSampleRate = [[AVAudioSession sharedInstance] sampleRate];
    return hardwareSampleRate;
}

-(Float32)_configureDeviceBufferDurationWithDefault:(float)defaultBufferDuration {
    Float32 bufferDuration = defaultBufferDuration; // Type 1/43 by default
    NSError *err = nil;
    [[AVAudioSession sharedInstance] setPreferredIOBufferDuration:bufferDuration error:&err];
    if (err) {
        NSLog(@"Error setting preferredIOBufferDuration for audio session: %@", err);
    }
    // Buffer Size
    bufferDuration = [[AVAudioSession sharedInstance] IOBufferDuration];
    return bufferDuration;
}

-(void)_configureStreamFormatWithSampleRate {
    
    // Set the stream format
    streamFormat = [Audio monoFloatFormatWithSampleRate:[[AVAudioSession sharedInstance] sampleRate]];
    UInt32 propSize = sizeof(streamFormat);
    
    // Set the stream format for the Listener on the AudioListener's output scope
    [Audio checkResult:AudioUnitSetProperty(_audioListenerUnit,
                                            kAudioUnitProperty_StreamFormat,
                                            kAudioUnitScope_Output,
                                            audioInputBus,
                                            &streamFormat,
                                            propSize)
             operation:"Could not set AudioListener's stream format bus 1"];
}

-(void)_configureAudioBufferList {
    
    UInt32 bufferFrameSize;
    UInt32 sizeOfBufferFrameSize = sizeof(bufferFrameSize);
    [Audio checkResult:AudioUnitGetProperty(_audioListenerUnit,
                                            kAudioUnitProperty_MaximumFramesPerSlice,
                                            kAudioUnitScope_Global,
                                            audioOutputBus,
                                            &bufferFrameSize,
                                            &sizeOfBufferFrameSize)
             operation:"Failed to get buffer frame size"];
    
    UInt32 bufferSizeBytes = bufferFrameSize * streamFormat.mBytesPerFrame;
    UInt32 propSize = offsetof( AudioBufferList, mBuffers[0] ) + ( sizeof( AudioBuffer ) *streamFormat.mChannelsPerFrame );
    _audioListenerBuffer                 = (AudioBufferList*)malloc(propSize);
    _audioListenerBuffer->mNumberBuffers = streamFormat.mChannelsPerFrame;
    for( UInt32 i = 0; i < _audioListenerBuffer->mNumberBuffers; i++ ){
        _audioListenerBuffer->mBuffers[i].mNumberChannels = streamFormat.mChannelsPerFrame;
        _audioListenerBuffer->mBuffers[i].mDataByteSize   = bufferSizeBytes;
        _audioListenerBuffer->mBuffers[i].mData           = malloc(bufferSizeBytes);
    }
}

-(void)_configureListenerCallback {
    AURenderCallbackStruct audioListenerCallbackStruct;
    audioListenerCallbackStruct.inputProc       = listenerCallback;
    audioListenerCallbackStruct.inputProcRefCon = (__bridge void *)self;
    [Audio checkResult:AudioUnitSetProperty(_audioListenerUnit,
                                              kAudioOutputUnitProperty_SetInputCallback,
                                              kAudioUnitScope_Global,
                                              audioInputBus,
                                              &audioListenerCallbackStruct,
                                              sizeof(audioListenerCallbackStruct))
               operation:"Couldn't set Listener callback"];
}

-(void)_disableCallbackBufferAllocation {
    [Audio checkResult:AudioUnitSetProperty(_audioListenerUnit,
                                              kAudioUnitProperty_ShouldAllocateBuffer,
                                              kAudioUnitScope_Output,
                                              audioInputBus,
                                              &disableFlag,
                                              sizeof(disableFlag))
               operation:"Could not disable audio unit allocating its own buffers"];
}

//---------------------------------------------------------- DEALLOC

-(void)dealloc {
    [Audio checkResult:AudioOutputUnitStop(_audioListenerUnit)
               operation:"Failed to uninitialize output unit"];
    [Audio checkResult:AudioUnitUninitialize(_audioListenerUnit)
               operation:"Failed to uninitialize output unit"];
    [Audio checkResult:AudioComponentInstanceDispose(_audioListenerUnit)
               operation:"Failed to uninitialize output unit"];
    [Audio freeBufferList:_audioListenerBuffer];
    [super dealloc];
}

@end
