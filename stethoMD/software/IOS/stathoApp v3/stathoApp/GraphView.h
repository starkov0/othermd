//
//  GraphView.h
//  pierre starkov - plug'n'scope - 2014


#import <UIKit/UIKit.h>

@class AudioContainer;
@class AudioController;
@class ViewContainer;

@interface GraphView : UIView

///-----------------------------------------------------------
/// @name GETTERS
///-----------------------------------------------------------

/**
 * \return view width in int data type
 */
-(int) viewWidth;

///-----------------------------------------------------------
/// @name SETTERS
///-----------------------------------------------------------

/**
 * set controller
 */
-(void) setAudioController:(AudioController*)audioController;

/**
 * set container
 */
-(void) setAudioContainer:(AudioContainer*)audioContainer;

-(void) setViewContainer:(ViewContainer*)viewContainer;

@end
