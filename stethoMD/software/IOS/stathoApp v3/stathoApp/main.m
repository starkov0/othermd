 //
//  main.m
//  stathoApp
//
//  Created by Pierre Starkov on 27/06/14.
//  Copyright (c) 2014 Pierre Starkov. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
