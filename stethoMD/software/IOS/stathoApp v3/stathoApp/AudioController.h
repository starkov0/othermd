//
//  AudioController.h
//  pierre starkov - plug'n'scope - 2014


#import <Foundation/Foundation.h>
#import "TPCircularBuffer.h"
#import "Audio.h"
#import "AudioListener.h"
#import "AudioSpeaker.h"
#import "AudioReader.h"
#import "AudioWriter.h"
#import "AudioContainer.h"
#import "FilterController.h"
#import "AnalysisController.h"

@class TotalView;
@class GraphView;
@class ViewContainer;
@class RootViewController;

///-----------------------------------------------------------
/// @name CLASS
///-----------------------------------------------------------

@interface AudioController : NSObject<AudioListenerDelegate,AudioSpeakerDelegate,AudioReaderDelegate>

///-----------------------------------------------------------
/// @name INITIALIZERS
///-----------------------------------------------------------

/**
 Class init
 */
+(AudioController*) audioController;


///-----------------------------------------------------------
/// @name INIT SETTERS
///-----------------------------------------------------------

/**
 * set audioContainer
 */
-(void) setAudioContainer:(AudioContainer*)audioContainer;

/**
 * set graphview
 */
-(void) setGraphView:(GraphView*)graphView;

/**
 * set totalView
 */
-(void) setTotalView:(TotalView*)totalView;

-(void) setViewContainer:(ViewContainer*)viewContainer;

-(void) setRootViewController:(RootViewController*)rootViewController;

/**
 * set rad file URL
 */
-(void) setReadURL:(NSURL*)readURL;

/**
 * set write file URL
 */
-(void) setWriteURL:(NSURL*)writeURL;

/**
 * set EOF
 */
-(void) setEof:(BOOL)eof;

///-----------------------------------------------------------
/// @name GETTERS BOOL
///-----------------------------------------------------------

/**
 @return bool is listening
 */
-(BOOL) isListening;

/**
 @return bool is playing
 */
-(BOOL) isReading;

/**
 @return bool is writing
 */
-(BOOL) isWriting;

/**
 @return is in reading mode
 */
-(BOOL) isInReadingMode;

/**
 @return is in listening mode
 */
-(BOOL) isInListeningMode;

/**
 @return is in writing mode
 */
-(BOOL) isInWritingMode;

///-----------------------------------------------------------
/// @name SETTERS BOOL
///-----------------------------------------------------------

-(void) setFilter:(BOOL)filter;

-(void) setAnalyse:(BOOL)analyse;

///-----------------------------------------------------------
/// @name EVENTS
///-----------------------------------------------------------

/**
 start getting data from to the microphone
 */
-(void) startListening;

/**
 stop getting data from to the microphone
 */
-(void) stopListening;

/**
 start reading data from a file
 */
-(void) startReading;

/**
 stop reading data from a file
 */
-(void) stopReading;

/**
 start writing data into a file
 */
-(void) startWriting;

/**
 stop writing data into a file
 */
-(void) stopWriting;

@end
