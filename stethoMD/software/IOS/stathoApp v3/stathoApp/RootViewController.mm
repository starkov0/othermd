//
//  RootViewController.mm
//  pierre starkov - plug'n'scope - 2014


#import "RootViewController.h"

@interface RootViewController () {
    // audiocontroller
    AudioController *_audioController;
    
    // view
    GraphView *_graphView;
    TotalView *_totalView;
    ViewContainer *_viewContainer;
    
    // zoom
    float previousFirstPointX;
    float previousSecondPointX;
    
    // translation
    float previousTranslation;
    float translationDiff;
    int translationBuffer;
    int previousTranslationBuffer;
    int translationDirection;
    int translationAnimationVariable;
    NSTimeInterval previousTimeInterval;
    NSTimeInterval timeIntervalDiff;
    int graphViewTotalViewTranslation; // 0 -> none / 1 -> graphView / 2 -> totalView
    NSTimer *_translationAnimationTimer;

    // gesture event
    int previousEvent; // 0 -> none / 1 -> listen / 2 -> write / 3 -> read
    
    // container
    AudioContainer *_audioContainer;
    
    // label
    NSTimeInterval writingStartTime;
    UILabel *_timeLabel;
    NSTimer *_LabelTimeUpdateTimer;
    UILabel *_currentTaskLabel;
    
    
    // slider
    UISlider *_zoomYSlider;
}
@end

@implementation RootViewController
@synthesize graphView = _graphView;
@synthesize totalView = _totalView;
@synthesize zoomYSlider = _zoomYSlider;
@synthesize timeLabel = _timeLabel;
@synthesize currentTaskLabel = _currentTaskLabel;

//---------------------------------------------------------- INIT

- (void)viewDidLoad
{
    [self view];
    [super viewDidLoad];
    
    // init
    _audioContainer     = [AudioContainer audioContainer];
    _audioController    = [AudioController audioController];
    _viewContainer      = [[ViewContainer alloc] init];
    
    // connect classes
    [_audioController setAudioContainer:_audioContainer];
    [_audioController setGraphView:_graphView];
    [_audioController setTotalView:_totalView];
    [_audioController setViewContainer:_viewContainer];
    [_audioController setRootViewController:self];
    
    [_viewContainer setAudioContainer:_audioContainer];
    [_viewContainer setAudioController:_audioController];
    [_viewContainer setGraphView:_graphView];
    [_viewContainer setZoomMaxLimitX:500000 zoomMinLimitX:[_graphView viewWidth]*2];
    
    [_graphView setAudioContainer:_audioContainer];
    [_graphView setAudioController:_audioController];
    [_graphView setViewContainer:_viewContainer];
    
    [_totalView setAudioContainer:_audioContainer];
    [_totalView setAudioController:_audioController];
    [_totalView setViewContainer:_viewContainer];
    
    // set file URL
    [_audioController setReadURL:[self filePath:@"audioTest.m4a"]];
    [_audioController setWriteURL:[self filePath:@"audioTest.m4a"]];
    
    // gesture event
    previousEvent = 0;
    graphViewTotalViewTranslation = 0;
    
    // zoomYSlider
    [_zoomYSlider setTranslatesAutoresizingMaskIntoConstraints:YES];
    [_zoomYSlider removeConstraints:_zoomYSlider.constraints];
    CGAffineTransform trans = CGAffineTransformMakeRotation(M_PI_2);
    _zoomYSlider.transform = trans;
    
    // label
    _timeLabel.text = @"...";
    _currentTaskLabel.text = @"No Current Task";
}


//---------------------------------------------------------- FILE PATH

-(NSURL*)filePath:(NSString*)fileName {
    // basepath
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    
    return [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/%@", basePath, fileName]];
}

//---------------------------------------------------------- LABELS

-(void) startUpdateTimeLabel {
    if ([_audioController isInReadingMode] || [_audioController isInWritingMode]) {
        if(_LabelTimeUpdateTimer == nil) {
            _LabelTimeUpdateTimer = [NSTimer scheduledTimerWithTimeInterval:0.04
                                                               target:self
                                                             selector:@selector(updateTimeLabel:)
                                                             userInfo:nil
                                                              repeats:YES];
        }
    }
    else {
        _timeLabel.text = @"...";
    }
}

-(void) stopUpdateTimeLabel {
    if(_LabelTimeUpdateTimer != nil) {
        
        // animation
        [_LabelTimeUpdateTimer invalidate];
        _LabelTimeUpdateTimer = nil;
    }
}

-(void) updateTimeLabel:(NSTimer*)timer {
    if ([_audioController isInReadingMode]) {
        // duration number
        float totalDuration   = [_audioContainer totalDuration];
        float currentDuration = totalDuration * [_audioContainer currentFrame] / [_audioContainer totalFrames];
        
        int totalDurationSeconds = floor(totalDuration);
        int totalDurationMiliseconds = round(totalDuration*100 - totalDurationSeconds*100);
        int currentDurationSeconds = floor(currentDuration);
        int currentDurationMiliseconds = round(currentDuration*100 - currentDurationSeconds*100);
        
        NSLog(@"lala: %f, %f, %d, %d, %d, %d",totalDuration,currentDuration,totalDurationSeconds,totalDurationMiliseconds, currentDurationSeconds, currentDurationMiliseconds);
        
        // duration string
        NSString *totalSeconds, *totalMiliseconds, *currentSeconds, *currentMiliseconds;
        
        if (totalDurationSeconds < 10) {
            totalSeconds = [NSString stringWithFormat:@"0%d",totalDurationSeconds];
        } else {
            totalSeconds = [NSString stringWithFormat:@"%d",totalDurationSeconds];
        }
        
        if (totalDurationMiliseconds < 10) {
            totalMiliseconds = [NSString stringWithFormat:@"0%d",totalDurationMiliseconds];
        } else {
            totalMiliseconds = [NSString stringWithFormat:@"%d",totalDurationMiliseconds];
        }
        
        if (currentDurationSeconds < 10) {
            currentSeconds = [NSString stringWithFormat:@"0%d",currentDurationSeconds];
        } else {
            currentSeconds = [NSString stringWithFormat:@"%d",currentDurationSeconds];
        }
        
        if (currentDurationMiliseconds < 10) {
            currentMiliseconds = [NSString stringWithFormat:@"0%d",currentDurationMiliseconds];
        } else {
            currentMiliseconds = [NSString stringWithFormat:@"%d",currentDurationMiliseconds];
        }
        
        // set label
        _timeLabel.text = [NSString stringWithFormat:@"%@:%@ / %@:%@", currentSeconds, currentMiliseconds, totalSeconds, totalMiliseconds];
    }
    else if ([_audioController isInWritingMode]) {
        // time number
        NSTimeInterval currentTime = [[NSDate date] timeIntervalSince1970] - writingStartTime;
        int currentSeconds = floor(currentTime);
        int currentMiliseconds = round(currentTime*100 - currentSeconds*100);
        
        // time string
        NSString *seconds, *miliseconds;
        
        if (currentSeconds < 10) {
            seconds = [NSString stringWithFormat:@"0%d",currentSeconds];
        } else {
            seconds = [NSString stringWithFormat:@"%d",currentSeconds];
        }
        
        if (currentMiliseconds < 10) {
            miliseconds = [NSString stringWithFormat:@"0%d",currentMiliseconds];
        } else {
            miliseconds = [NSString stringWithFormat:@"%d",currentMiliseconds];
        }
        
        // set label
        _timeLabel.text = [NSString stringWithFormat:@"%@:%@", seconds, miliseconds];
    }
}

//---------------------------------------------------------- TRANSLATION ANIMATION

-(void)_startTranslationAnimation {
    if(_translationAnimationTimer == nil) {
        _translationAnimationTimer = [NSTimer scheduledTimerWithTimeInterval:0.04
                                                           target:self
                                                         selector:@selector(animateTranslation:)
                                                         userInfo:nil
                                                          repeats:YES];
    }
}

-(void)_stopTranslationAnimation {
    if(_translationAnimationTimer != nil) {
        
        // animation
        [_translationAnimationTimer invalidate];
        _translationAnimationTimer = nil;
        
        // previous event
        if (previousEvent == 3) {
            [self read];
            previousEvent = 0;
        }
    }
}

-(void) animateTranslation:(NSTimer *)timer {
    @synchronized(self) {
        
        float invtimeIntervalDiff = 1.0/timeIntervalDiff;

        if ((invtimeIntervalDiff < 0) ||
            ((translationDirection == 1) && (previousTranslationBuffer < 0)) ||
            ((translationDirection == -1) && (previousTranslationBuffer > 0)) ||
            ([_audioContainer currentFrame] <= 0) ||
            ([_audioContainer currentFrame] >= [_audioContainer totalFrames])) {
            [self _stopTranslationAnimation];
        }
        else {
            [_viewContainer translateCurrentFrameWithFrame:previousTranslationBuffer];
            [_graphView setNeedsDisplay];
            [_totalView setNeedsDisplay];
        }
        
        // translation
        if (translationAnimationVariable < 0) {
            if (translationDirection == 1) {
                previousTranslationBuffer -= [_audioContainer frameSize];
            } else {
                previousTranslationBuffer += [_audioContainer frameSize];
            }
            translationAnimationVariable = 2;
        } else {
            translationAnimationVariable--;
        }
        // time
        invtimeIntervalDiff -= 1;
        timeIntervalDiff = 1.0/invtimeIntervalDiff;
    }
}

//---------------------------------------------------------- EVENTS

- (IBAction)listen {
    @synchronized(self) {
        if (![_audioController isListening]) {
            [_viewContainer translationX0X1CurrentFrameReset];
            [_audioController startListening];
            _currentTaskLabel.text = @"Listening";
        }
        else {
            [_audioController stopListening];
            _currentTaskLabel.text = @"Stoped Listening";
        }
    }
}

- (IBAction)read {
    @synchronized(self) {
        if (![_audioController isReading]) {
            if (![_audioController isInReadingMode]) {
                [_viewContainer translationX0X1CurrentFrameReset];
            }
            [_audioController startReading];
            _currentTaskLabel.text = @"Reading";
        }
        else {
            [_audioController stopReading];
            _currentTaskLabel.text = @"Stopped Reading";
        }
    }
}

- (IBAction)write {
    @synchronized(self) {
        if (![_audioController isWriting]) {
            [_viewContainer translationX0X1CurrentFrameReset];
            [_audioController startWriting];
            writingStartTime = [[NSDate date] timeIntervalSince1970];
            _currentTaskLabel.text = @"Writing";
        }
        else {
            [_audioController stopWriting];
            _currentTaskLabel.text = @"Stopped Writing";
        }
    }
}

//---------------------------------------------------------- GRAPH VIEW GESTURE

// zoom
- (IBAction)graphViewPinchGesture:(UIPinchGestureRecognizer *)sender {
    @synchronized(self) {
        if ([_audioController isInListeningMode] || [_audioController isInReadingMode] || [_audioController isInWritingMode]) {
            if ([sender numberOfTouches] == 2) {
                if ([sender state] == UIGestureRecognizerStateBegan) {
                    
                    // translation
                    [self _stopTranslationAnimation];
                    
                    // previous event
                    if ([_audioController isListening]) {
                        previousEvent = 1;
                        [self listen];
                    }
                    else if ([_audioController isWriting]) {
                        previousEvent = 2;
                        [self write];
                    }
                    else if ([_audioController isReading]) {
                        previousEvent = 3;
                        [self read];
                    }
                    
                    // points
                    if([sender locationOfTouch:0 inView:sender.view].x < [sender locationOfTouch:1 inView:sender.view].x) {
                        previousFirstPointX = [sender locationOfTouch:0 inView:sender.view].x;
                        previousSecondPointX = [sender locationOfTouch:1 inView:sender.view].x;
                    } else {
                        previousFirstPointX = [sender locationOfTouch:1 inView:sender.view].x;
                        previousSecondPointX = [sender locationOfTouch:0 inView:sender.view].x;
                    }
                }
                else if ([sender state] == UIGestureRecognizerStateChanged) {

                    // points
                    float diffFirstPointX, diffSecondPointX;
                    if([sender locationOfTouch:0 inView:sender.view].x < [sender locationOfTouch:1 inView:sender.view].x) {
                        diffFirstPointX = previousFirstPointX - [sender locationOfTouch:0 inView:sender.view].x;
                        diffSecondPointX = previousSecondPointX - [sender locationOfTouch:1 inView:sender.view].x;
                        previousFirstPointX = [sender locationOfTouch:0 inView:sender.view].x;
                        previousSecondPointX = [sender locationOfTouch:1 inView:sender.view].x;
                    } else {
                        diffFirstPointX = previousFirstPointX - [sender locationOfTouch:1 inView:sender.view].x;
                        diffSecondPointX = previousSecondPointX - [sender locationOfTouch:0 inView:sender.view].x;
                        previousFirstPointX = [sender locationOfTouch:1 inView:sender.view].x;
                        previousSecondPointX = [sender locationOfTouch:0 inView:sender.view].x;
                    }
                    
                    // zoom
                    [_viewContainer zoomX:diffSecondPointX - diffFirstPointX];
                    
                    // draw
                    [_graphView setNeedsDisplay];
                    [_totalView setNeedsDisplay];
                }
            }
            if ([sender state] == UIGestureRecognizerStateEnded) {
                
                // previous event
                if (previousEvent == 1) {
                    [self listen];
                    previousEvent = 0;
                }
                else if (previousEvent == 2) {
                    [self write];
                    previousEvent = 0;
                }
                else if (previousEvent == 3) {
                    [self read];
                    previousEvent = 0;
                }
            }
        }
    }
}

// translate
// 1 -> graphView / 2 -> totalview
- (IBAction)graphViewPanGesture:(UIPanGestureRecognizer *)sender {
    @synchronized(self) {
        if ([_audioController isInReadingMode]) {
            if ([sender state] == UIGestureRecognizerStateBegan) {
                
                if (graphViewTotalViewTranslation == 0) {
                    
                    // view
                    graphViewTotalViewTranslation = 1;
                    
                    // previous event
                    if ([_audioController isReading]) {
                        previousEvent = 3;
                        [self read];
                    }

                    // translation
                    [self _stopTranslationAnimation];
                    
                    // translation
                    previousTranslation = [sender locationInView:sender.view].x;
                    translationBuffer = 0;
                    
                    // time
                    previousTimeInterval = [[NSDate date] timeIntervalSince1970];
                }
            }
            else if ([sender state] == UIGestureRecognizerStateChanged) {
               
                if (graphViewTotalViewTranslation == 1) {
                    
                    // translation
                    translationDiff = [sender locationInView:sender.view].x - previousTranslation;
                    previousTranslation = [sender locationInView:sender.view].x;
                    translationBuffer += translationDiff * [_viewContainer dx];
                    previousTranslationBuffer = translationBuffer;
                    
                    // time
                    timeIntervalDiff = [[NSDate date] timeIntervalSince1970] - previousTimeInterval;
                    previousTimeInterval = [[NSDate date] timeIntervalSince1970];
                    
                    if (abs(translationBuffer) >= [_audioContainer frameSize]) {
                        
                        // translate
                        int translationValue = (int)round(translationBuffer / [_audioContainer frameSize]) * [_audioContainer frameSize];
                        translationBuffer -= translationValue;
                        [_viewContainer translateCurrentFrameWithFrame:translationValue];
                        
                        // draw
                        [_graphView setNeedsDisplay];
                        [_totalView setNeedsDisplay];
                    }
                }
            }
            else if ([sender state] == UIGestureRecognizerStateEnded) {
                
                if (graphViewTotalViewTranslation == 1) {

                    // translation
                    previousTranslationBuffer = (int)round(previousTranslationBuffer / [_audioContainer frameSize]) * [_audioContainer frameSize];
                    if (previousTranslationBuffer > 0) {
                        translationDirection = 1;
                    } else {
                        translationDirection = -1;
                    }

                    // time
                    timeIntervalDiff = [[NSDate date] timeIntervalSince1970] - previousTimeInterval;
                    translationAnimationVariable = 2;
                    
                    // animate
                    [self _startTranslationAnimation];
                    
                    // view
                    graphViewTotalViewTranslation = 0;
                }
            }
        }
    }
}

// stop translation animation
- (IBAction)graphViewTapGesture:(UITapGestureRecognizer *)sender {
    @synchronized(self) {
        [self _stopTranslationAnimation];
    }
}

// set viewPositon
- (IBAction)graphViewTapTwoGesture:(UITapGestureRecognizer *)sender {
    @synchronized(self) {
        // event
        if ([_audioController isInListeningMode]) {
            [self listen];
        }
        else if ([_audioController isInWritingMode]) {
            [self write];
        }
        else if ([_audioController isInReadingMode]) {
            [self read];
        }
        // draw
        [_graphView setNeedsDisplay];
        [_totalView setNeedsDisplay];
    }
}

//---------------------------------------------------------- TOTAL VIEW GESTURE

- (IBAction)totalViewPanGesture:(UIPanGestureRecognizer *)sender {
    @synchronized(self) {
        if ([_audioController isInReadingMode]) {
            if ([sender state] == UIGestureRecognizerStateBegan) {
                
                if (graphViewTotalViewTranslation == 0) {

                    // view
                    graphViewTotalViewTranslation = 2;
                    
                    // previous event
                    if ([_audioController isReading]) {
                        previousEvent = 3;
                        [self read];
                    }
                    
                    // translation
                    [self _stopTranslationAnimation];
                    
                    // translation
                    previousTranslation = [sender locationInView:sender.view].x;
                    translationBuffer = 0;
                }
            }
            else if ([sender state] == UIGestureRecognizerStateChanged) {
                
                if (graphViewTotalViewTranslation == 2) {

                    // translation
                    translationDiff = [sender locationInView:sender.view].x - previousTranslation;
                    previousTranslation = [sender locationInView:sender.view].x;
                    translationBuffer += translationDiff * [_viewContainer totalViewDx];
                    previousTranslationBuffer = translationBuffer;
                    
                    if (abs(translationBuffer) >= [_audioContainer frameSize]) {
                        
                        // translate
                        int translationValue = (int)round(translationBuffer / [_audioContainer frameSize]) * [_audioContainer frameSize];
                        translationBuffer -= translationValue;
                        [_viewContainer translateCurrentFrameWithFrame:translationValue];
                        
                        // draw
                        [_graphView setNeedsDisplay];
                        [_totalView setNeedsDisplay];
                    }
                }
            }
            else if ([sender state] == UIGestureRecognizerStateEnded) {
                
                if (graphViewTotalViewTranslation == 2) {

                    // previous event
                    if (previousEvent == 3) {
                        [self read];
                        previousEvent = 0;
                    }
                    
                    // view
                    graphViewTotalViewTranslation = 0;
                }
            }
        }
    }
}

- (IBAction)totalViewTapGesture:(UITapGestureRecognizer *)sender {
    @synchronized(self) {
    [self _stopTranslationAnimation];
    }
}

- (IBAction)totalViewTwoTapGesture:(UITapGestureRecognizer *)sender {
    @synchronized(self) {
        // event
        if ([_audioController isInListeningMode]) {
            [self listen];
        }
        else if ([_audioController isInWritingMode]) {
            [self write];
        }
        else if ([_audioController isInReadingMode]) {
            [self read];
        }
        // draw
        [_graphView setNeedsDisplay];
        [_totalView setNeedsDisplay];
    }
}

- (IBAction)zoomY:(UISlider *)sender {
    // value
    float value = round([sender value] * 20) / 20;
    NSLog(@"lala: %f",value);
    [sender setValue:value animated:YES];
    [_viewContainer setZoomY:value];
    [_graphView setNeedsDisplay];
    [_totalView setNeedsDisplay];
}

//---------------------------------------------------------- FILTER - ANALYSE

- (IBAction)filter {
    // duration number
    float totalDuration   = [_audioContainer totalDuration];
    float currentDuration = totalDuration * [_audioContainer currentFrame] / [_audioContainer totalFrames];
    
    int totalDurationSeconds = floor(totalDuration);
    int totalDurationMiliseconds = round(totalDuration*100 - totalDurationSeconds*100);
    int currentDurationSeconds = floor(currentDuration);
    int currentDurationMiliseconds = round(currentDuration*100 - currentDurationSeconds*100);
    
    // duration string
    NSString *totalSeconds, *totalMiliseconds, *currentSeconds, *currentMiliseconds;
    
    if (totalDurationSeconds < 10) {
        totalSeconds = [NSString stringWithFormat:@"0%d",totalDurationSeconds];
    } else {
        totalSeconds = [NSString stringWithFormat:@"%d",totalDurationSeconds];
    }
    
    if (totalDurationMiliseconds < 10) {
        totalMiliseconds = [NSString stringWithFormat:@"0%d",totalDurationMiliseconds];
    } else {
        totalMiliseconds = [NSString stringWithFormat:@"%d",totalDurationMiliseconds];
    }
    
    if (currentDurationSeconds < 10) {
        currentSeconds = [NSString stringWithFormat:@"0%d",currentDurationSeconds];
    } else {
        currentSeconds = [NSString stringWithFormat:@"%d",currentDurationSeconds];
    }
    
    if (currentDurationMiliseconds < 10) {
        currentMiliseconds = [NSString stringWithFormat:@"0%d",currentDurationMiliseconds];
    } else {
        currentMiliseconds = [NSString stringWithFormat:@"%d",currentDurationMiliseconds];
    }
    
    // set label
    _timeLabel.text = [NSString stringWithFormat:@"%@:%@ / %@:%@", currentSeconds, currentMiliseconds, totalSeconds, totalMiliseconds];
}

- (IBAction)analyse {
    // time number
    NSTimeInterval currentTime = [[NSDate date] timeIntervalSince1970] - writingStartTime;
    int currentSeconds = floor(currentTime);
    int currentMiliseconds = round(currentTime*100 - currentSeconds*100);
    
    // time string
    NSString *seconds, *miliseconds;
    
    if (currentSeconds < 10) {
        seconds = [NSString stringWithFormat:@"0%d",currentSeconds];
    } else {
        seconds = [NSString stringWithFormat:@"%d",currentSeconds];
    }
    
    if (currentMiliseconds < 10) {
        miliseconds = [NSString stringWithFormat:@"0%d",currentMiliseconds];
    } else {
        miliseconds = [NSString stringWithFormat:@"%d",currentMiliseconds];
    }
    
    // set label
    _timeLabel.text = [NSString stringWithFormat:@"%@:%@", seconds, miliseconds];
}

//---------------------------------------------------------- I DONT KNOW

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [_viewContainer release];
    [super dealloc];
}

@end
