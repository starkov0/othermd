//
//  AudioReader.m
//  inspired from EZAudio
//  pierre starkov - plug'n'scope - 2014

#import "AudioReader.h"
#import "Audio.h"

/// Buses
static const AudioUnitScope audioOutputBus = 0;
static const AudioUnitScope audioInputBus  = 1;

/// Flags
static const UInt32 disableFlag = 0;
static const UInt32 enableFlag  = 1;

@interface AudioReader (){
    
    // Reading from the audio file
    ExtAudioFileRef             _audioFile;
    AudioStreamBasicDescription _clientFormat;
    AudioStreamBasicDescription _fileFormat;
    SInt64                      _frameIndex;
    CFURLRef                    _sourceURL;
    Float32                     _totalDuration;
    SInt64                      _totalFrames;
    
    /// Audio Input/Output Unit
    AudioUnit _audioReaderUnit;
    
    // Internal Booleans
    BOOL _isReading;
    BOOL _isConfigured;
    BOOL _AudioReaderOn;
    
}
@end

@implementation AudioReader
@synthesize audioReaderDelegate = _audioReaderDelegate;

//---------------------------------------------------------- TOTAL AUDIO DATA

-(void) fillTotalAudioData:(NSMutableArray*)totalAudioArray {
    @synchronized (self) {
        // init variables
        UInt32 inNumberFrames = 64;
        AudioBufferList *bufferList = [Audio audioBufferListWithNumberOfFrames:64
                                                              numberOfChannels:_clientFormat.mChannelsPerFrame
                                                                   interleaved:YES];
        
        // Read in the specified number of frames -> first iteration
        [Audio checkResult:ExtAudioFileRead(_audioFile,
                                            &inNumberFrames,
                                            bufferList)
                 operation:"Failed to read audio data from audio file"];
        
        while (inNumberFrames != 0) {
            
            // add audio data to an array
            for(int i=0; i<inNumberFrames; i++){
                [totalAudioArray addObject:@(((float*)bufferList[0].mBuffers[0].mData)[i])];
            }
            
            // Read in the specified number of frames -> other iteration
            [Audio checkResult:ExtAudioFileRead(_audioFile,
                                                &inNumberFrames,
                                                bufferList)
                     operation:"Failed to read audio data from audio file"];
        }
        
        // free buffer list
        [Audio freeBufferList:bufferList];
    }
}

//---------------------------------------------------------- INIT

-(AudioReader*)initWithURL:(NSURL*)url andDelegate:(id<AudioReaderDelegate>)delegate {
    @synchronized (self) {
        self = [super init];
        if(self){
            _isConfigured = NO;
            _isReading = NO;
            _sourceURL = (__bridge CFURLRef)url;
            if (!_isConfigured) {
                [self _configureAudioReader];
                _isConfigured = YES;
            }
            self.audioReaderDelegate = delegate;
        }
        return self;
    }
}

+(AudioReader *)audioReaderWithURL:(NSURL *)url andDelegate:(id<AudioReaderDelegate>)delegate {
    return [[AudioReader alloc] initWithURL:url andDelegate:delegate];
}

//---------------------------------------------------------- CONFIG

-(void)_configureAudioReader {
    // open file
    [self _openFile];
    
    // get file description
    [self _getTotalFramesDuration];
    
    // set file format
    [self _setFileFormat];
    
    // create audioReaderUnit
    [self _configureAudioReaderUnit];
    
    // input
    [self _enableInputScope];
    
    // output
    [self _disableOutputScope];
    
    // Initialize the audio unit
    [Audio checkResult:AudioUnitInitialize( _audioReaderUnit )
             operation:"Couldn't initialize the input unit"];
}

-(void)_openFile {
    // Source URL should not be nil
    NSAssert(_sourceURL,@"Source URL was not specified correctly.");
    
    // Try to open the file for reading
    [Audio checkResult:ExtAudioFileOpenURL(_sourceURL,&_audioFile)
             operation:"Failed to open audio file for reading"];
}

-(void)_getTotalFramesDuration {
    // Try pulling the stream description
    UInt32 size = sizeof(_fileFormat);
    [Audio checkResult:ExtAudioFileGetProperty(_audioFile,kExtAudioFileProperty_FileDataFormat, &size, &_fileFormat)
             operation:"Failed to get audio stream basic description of input file"];
    
    // Try pulling the total frame size
    size = sizeof(_totalFrames);
    [Audio checkResult:ExtAudioFileGetProperty(_audioFile,kExtAudioFileProperty_FileLengthFrames, &size, &_totalFrames)
             operation:"Failed to get total frames of input file"];
    _totalFrames = MAX(1, _totalFrames);
    
    // Total duration
    _totalDuration = _totalFrames / _fileFormat.mSampleRate;
}

-(void)_setFileFormat {
    _clientFormat = [Audio monoFloatFormatWithSampleRate:_fileFormat.mSampleRate];
    
    [Audio checkResult:ExtAudioFileSetProperty(_audioFile,
                                               kExtAudioFileProperty_ClientDataFormat,
                                               sizeof (AudioStreamBasicDescription),
                                               &_clientFormat)
             operation:"Couldn't set client data format on input ext file"];
}

-(void)_configureAudioReaderUnit {
    // Create an input component description for mic input
    AudioComponentDescription audioComponentDescription;
    audioComponentDescription.componentType             = kAudioUnitType_Output;
    audioComponentDescription.componentManufacturer     = kAudioUnitManufacturer_Apple;
    audioComponentDescription.componentFlags            = 0;
    audioComponentDescription.componentFlagsMask        = 0;
    audioComponentDescription.componentSubType          = kAudioUnitSubType_RemoteIO;

    // Try and find the component
    AudioComponent audioComponent = AudioComponentFindNext( NULL , &audioComponentDescription );
    NSAssert(audioComponent,@"Couldn't get input component unit!");
    
    // create Unit
    [Audio checkResult:AudioComponentInstanceNew(audioComponent,
                                                 &_audioReaderUnit )
             operation:"Couldn't open component for microphone input unit."];
}

-(void)_disableOutputScope {
    [Audio checkResult:AudioUnitSetProperty(_audioReaderUnit,
                                            kAudioOutputUnitProperty_EnableIO,
                                            kAudioUnitScope_Output,
                                            audioOutputBus,
                                            &disableFlag,
                                            sizeof(disableFlag))
             operation:"Couldn't disable output on I/O unit."];
}

-(void)_enableInputScope {
    [Audio checkResult:AudioUnitSetProperty(_audioReaderUnit,
                                            kAudioOutputUnitProperty_EnableIO,
                                            kAudioUnitScope_Input,
                                            audioInputBus,
                                            &enableFlag,
                                            sizeof(enableFlag))
             operation:"Couldn't enable input on I/O unit."];
}

//---------------------------------------------------------- GETTERS-SETTERS

-(SInt64)frameIndex {
    return _frameIndex;
}

-(float)totalDuration {
    return (float)_totalDuration;
}

-(int)totalFrames {
    return (int)_totalFrames;
}

-(NSURL *)url {
    return (__bridge NSURL*)_sourceURL;
}

//---------------------------------------------------------- DEALLOC

-(void)dealloc {
    @synchronized (self) {
        // close file
        if( _audioFile ){
            [Audio checkResult:ExtAudioFileDispose(_audioFile)
                     operation:"Failed to dispose of audio file"];
        }

        // disable audioUnit
        [Audio checkResult:AudioOutputUnitStop(_audioReaderUnit)
                 operation:"Failed to stop output unit"];
        [Audio checkResult:AudioUnitUninitialize(_audioReaderUnit)
                 operation:"Failed to uninitialize output unit"];
        [Audio checkResult:AudioComponentInstanceDispose(_audioReaderUnit)
                 operation:"Failed to dispose output unit"];
        
        [super dealloc];
    }
}

@end
