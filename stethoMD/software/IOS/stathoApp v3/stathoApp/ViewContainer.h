//
//  ViewContainer.h
//  stathoApp
//
//  Created by Pierre Starkov on 10/09/14.
//  Copyright (c) 2014 Pierre Starkov. All rights reserved.
//

#import <Foundation/Foundation.h>

@class AudioController;
@class AudioContainer;
@class GraphView;

@interface ViewContainer : NSObject

///-----------------------------------------------------------
/// @name GETTERS
///-----------------------------------------------------------

-(int) x0;
-(int) x1;
-(int) dx;
-(int) totalViewDx;
-(UIColor*) colorWithName:(NSString*)name;
-(float) zoomY;

///-----------------------------------------------------------
/// @name SETTERS
///-----------------------------------------------------------

-(void) setAudioController:(AudioController*)audioController;

-(void) setAudioContainer:(AudioContainer*)audioContainer;

-(void) setGraphView:(GraphView*)graphView;

-(void) setZoomMaxLimitX:(int)zoomMaxLimitX
           zoomMinLimitX:(int)zoomMinLimitX;

///-----------------------------------------------------------
/// @name EVENTS
///-----------------------------------------------------------

-(void) translationX0X1CurrentFrameReset;

-(void) translateCurrentFrameWithFrame:(int)translationValue;
-(void) translateCurrentFrameWithPosition:(int)translationValue;

-(void) translateX0X1WithFrames:(int)translationValue;
-(void) translateX0X1WithPosition:(int)translationValue;

-(void) translateX0X1CurrentFrameWithFrames:(int)translationValue;
-(void) translateX0X1CurrentFrameWithPosition:(int)translationValue;

-(void) zoomXReset;

-(void) zoomYReset;

-(void) zoomX:(float)zoomX;

-(void) setZoomY:(float)zoomY;

@end

