//
//  AudioWriter.h
//  inspired from EZAudio
//  pierre starkov - plug'n'scope - 2014

#import <Foundation/Foundation.h>
#import <AudioToolbox/AudioToolbox.h>

///-----------------------------------------------------------
/// @name Class
///-----------------------------------------------------------

/**
 The AudioWriter provides a flexible way to create an audio file and append raw audio data to it. The AudioWriter will convert the incoming audio on the fly to the destination format so no conversion is needed between this and any other component. Right now the only supported output format is 'caf'. Each output file should have its own AudioWriter instance (think 1 AudioWriter = 1 audio file).
 */
@interface AudioWriter : NSObject

///-----------------------------------------------------------
/// @name Initializers
///-----------------------------------------------------------

/**
 Creates a new instance of an AudioWriter using a destination file path URL and the source format of the incoming audio.
 @param url                 An NSURL specifying the file path location of where the audio file should be written to.
 @return The newly created AudioWriter instance.
 */
-(AudioWriter*)initWithDestinationURL:(NSURL*)url;

/**
 Class method to create a new instance of an AudioWriter using a destination file path URL and the source format of the incoming audio.
 @param url                 An NSURL specifying the file path location of where the audio file should be written to.
 @return The newly created AudioWriter instance.
 */
+(AudioWriter*)audioWriterWithDestinationURL:(NSURL*)url;

///-----------------------------------------------------------
/// @name Getting The Recorder's Properties
///-----------------------------------------------------------
/**
 Provides the file path that's currently being used by the recorder.
 @return  The NSURL representing the file path of the audio file path being used for recording.
 */
-(NSURL*)url;

///-----------------------------------------------------------
/// @name Events
///-----------------------------------------------------------

/**
 Appends audio data to the tail of the output file from an AudioBufferList.
 @param bufferList The AudioBufferList holding the audio data to append
 @param bufferSize The size of each of the buffers in the buffer list.
 */
-(void)appendDataFromBufferList:(AudioBufferList*)bufferList
                 withBufferSize:(UInt32)bufferSize;

@end