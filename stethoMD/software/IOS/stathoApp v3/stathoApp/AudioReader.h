//
//  AudioReader.h
//  inspired from EZAudio
//  pierre starkov - plug'n'scope - 2014

#import <Foundation/Foundation.h>
#import <AudioToolbox/AudioToolbox.h>

@class Audio;
@class AudioReader;

///-----------------------------------------------------------
/// @name Delagate
///-----------------------------------------------------------

/**
 The AudioReaderDelegate provides event callbacks for the AudioReader object. These type of events are triggered by reads and seeks on the file and gives feedback such as the audio data read as a float array for visualizations and the new seek position for UI updating.
 */
@protocol AudioReaderDelegate <NSObject>

@end

///-----------------------------------------------------------
/// @name Class
///-----------------------------------------------------------

/**
 AudioReader's interactions included reading audio data, seeking within an audio file and getting information about the file. The AudioReaderDelegate provides event callbacks for when reads, seeks, and various updates happen within the audio file to allow the caller to interact with the action in meaningful ways. Common use cases here could be to read the audio file's data as AudioBufferList structures for output
 */
@interface AudioReader : NSObject

/**
 A AudioReaderDelegate for the audio file that is used to return events such as new seek positions within the file and the read audio data as a float array.
 */
@property (nonatomic,assign) id<AudioReaderDelegate> audioReaderDelegate;

///-----------------------------------------------------------
/// @name Initializers
///-----------------------------------------------------------

/**
 Creates a new instance of the AudioReader using a file path URL and allows specifying an AudioReaderDelegate.
 @param url      The file path reference of the audio file as an NSURL.
 @param delegate The audio file delegate that receives events specified by the AudioReaderDelegate protocol
 @return The newly created AudioReader instance.
 */
-(AudioReader*)initWithURL:(NSURL*)url
               andDelegate:(id<AudioReaderDelegate>)delegate;

/**
 Class method that creates a new instance of the AudioReader using a file path URL and allows specifying an AudioReaderDelegate.
 @param url      The file path reference of the audio file as an NSURL.
 @param delegate The audio file delegate that receives events specified by the AudioReaderDelegate protocol
 @return The newly created AudioReader instance.
 */
+(AudioReader*)audioReaderWithURL:(NSURL*)url
                    andDelegate:(id<AudioReaderDelegate>)delegate;

///-----------------------------------------------------------
/// @name TOTAL AUDIO DATA
///-----------------------------------------------------------

/**
 Read the the whole file
 fill an array containing the file's audio data
 */
-(void) fillTotalAudioData:(NSMutableArray*)totalAudioArray;

///-----------------------------------------------------------
/// @name Getting Information About The Audio File
///-----------------------------------------------------------

/**
 Provides the frame index (a.k.a the seek positon) within the audio file as an integer. This can be helpful when seeking through the audio file.
 @return The current frame index within the audio file as a SInt64.
 */
-(SInt64)frameIndex;

/**
 Provides the total duration of the audio file in seconds.
 @return The total duration of the audio file as a Float32.
 */
-(float)totalDuration;

/**
 Provides the total frame count of the audio file.
 @return The total number of frames in the audio file as a SInt64.
 */
-(int)totalFrames;

/**
 Provides the NSURL for the audio file.
 @return An NSURL representing the path of the AudioReader instance.
 */
-(NSURL*)url;

@end
