//
//  AudioSpeaker.m
//  inspired from EZAudio
//  pierre starkov - plug'n'scope - 2014

#import "Audio.h"
#import "AudioSpeaker.h"

/// Buses
static const AudioUnitElement audioOutputBus = 0;
//static const AudioUnitScope audioInputBus  = 1;

/// Flags
//static const UInt32 disableFlag = 0;
static const UInt32 enableFlag  = 1;

@interface AudioSpeaker (){
    BOOL                        _customASBD;
    BOOL                        _isPlaying;
    AudioStreamBasicDescription _audioSpeakerASBD;
    AudioUnit                   _audioSpeakerUnit;
}
@end

@implementation AudioSpeaker
@synthesize audioSpeakerDelegate = _audioSpeakerDelegate;

//---------------------------------------------------------- CALLBACK

static OSStatus SpeakerRenderCallback(void                        *inRefCon,
                                     AudioUnitRenderActionFlags  *ioActionFlags,
                                     const AudioTimeStamp        *inTimeStamp,
                                     UInt32                      inBusNumber,
                                     UInt32                      inNumberFrames,
                                     AudioBufferList             *ioData){
    
    AudioSpeaker *Speaker = (__bridge AudioSpeaker*)inRefCon;
    
    if( [Speaker.audioSpeakerDelegate respondsToSelector:@selector(audioSpeakerShouldUseCircularBuffer:)] ){
        
    TPCircularBuffer *circularBuffer = [Speaker.audioSpeakerDelegate audioSpeakerShouldUseCircularBuffer:Speaker];
        if( !circularBuffer ){
        
            SInt32 *data  = (SInt32*)ioData->mBuffers[0].mData;
            for(int i = 0; i < inNumberFrames; i++ ){
                data[ i ] = 0.0f;
            }
            return noErr;
    };

    // Get the desired amount of bytes to copy
    int32_t bytesToCopy = ioData->mBuffers[0].mDataByteSize;
    SInt32 *data  = (SInt32*)ioData->mBuffers[0].mData;

    // Get the available bytes in the circular buffer
    int32_t availableBytes;
    SInt32 *buffer = TPCircularBufferTail(circularBuffer, &availableBytes);

    // Ideally we'd have all the bytes to be copied, but compare it against the available bytes (get min)
    int32_t amount = MIN(bytesToCopy,availableBytes);
    memcpy( data, buffer, amount );

    // Consume those bytes ( this will internally push the head of the circular buffer )
    TPCircularBufferConsume(circularBuffer, amount);
    
    }
    return noErr;
}

//---------------------------------------------------------- EVENTS

-(void)start {
    if( !_isPlaying ){
        [Audio checkResult:AudioOutputUnitStart(_audioSpeakerUnit)
                 operation:"Failed to start Speaker unit"];
        _isPlaying = YES;
    }
}

-(void)stop {
    if( _isPlaying ){
        [Audio checkResult:AudioOutputUnitStop(_audioSpeakerUnit)
                 operation:"Failed to stop Speaker unit"];
        _isPlaying = NO;
    }
}

//---------------------------------------------------------- INIT

-(id)initWithDelegate:(id<AudioSpeakerDelegate>)delegate {
  self = [super init];
  if(self){
    _audioSpeakerDelegate = delegate;
    [self _configureSpeaker];
  }
  return self;
}

+(AudioSpeaker*)audioSpeakerWithDelegate:(id<AudioSpeakerDelegate>)delegate {
  return [[AudioSpeaker alloc] initWithDelegate:delegate];
}

//---------------------------------------------------------- CONFIG

-(void)_configureSpeaker {
  
    // configure unit
    [self _configureAudioSpeakerUnit];
    
    // Set the format for Speaker
    [self _configureStreamFormat];

    // configure callback
    [self _configureSpeakerCallback];
    
    // Init Unit
    [Audio checkResult:AudioUnitInitialize(_audioSpeakerUnit)
             operation:"Couldn't initialize Speaker unit"];
}

-(void)_configureAudioSpeakerUnit {
    
    // create description
    AudioComponentDescription Speakercd;
    Speakercd.componentFlags        = 0;
    Speakercd.componentFlagsMask    = 0;
    Speakercd.componentManufacturer = kAudioUnitManufacturer_Apple;
    Speakercd.componentSubType      = kAudioUnitSubType_RemoteIO;
    Speakercd.componentType         = kAudioUnitType_Output;
    
    // create component
    AudioComponent comp = AudioComponentFindNext(NULL,&Speakercd);
    [Audio checkResult:AudioComponentInstanceNew(comp,&_audioSpeakerUnit)
             operation:"Failed to get Speaker unit"];
    
    // Setup the Speaker unit for playback
    [Audio checkResult:AudioUnitSetProperty(_audioSpeakerUnit,
                                            kAudioOutputUnitProperty_EnableIO,
                                            kAudioUnitScope_Output,
                                            audioOutputBus,
                                            &enableFlag,
                                            sizeof(enableFlag))
             operation:"Failed to enable Speaker unit"];
}

-(void)_configureStreamFormat {
    // set sample rate
    _audioSpeakerASBD = [Audio monoFloatFormatWithSampleRate:[[AVAudioSession sharedInstance] sampleRate]];
    
    // Set the format for Speaker
    [Audio checkResult:AudioUnitSetProperty(_audioSpeakerUnit,
                                            kAudioUnitProperty_StreamFormat,
                                            kAudioUnitScope_Input,
                                            audioOutputBus,
                                            &_audioSpeakerASBD,
                                            sizeof(_audioSpeakerASBD))
             operation:"Couldn't set the ASBD for input scope/bos 0"];
}

-(void)_configureSpeakerCallback {
    AURenderCallbackStruct input;
    input.inputProc = SpeakerRenderCallback;
    input.inputProcRefCon = (__bridge void *)self;
    [Audio checkResult:AudioUnitSetProperty(_audioSpeakerUnit,
                                            kAudioUnitProperty_SetRenderCallback,
                                            kAudioUnitScope_Input,
                                            audioOutputBus,
                                            &input,
                                            sizeof(input))
             operation:"Failed to set the render callback on the Speaker unit"];
}

//---------------------------------------------------------- DEALLOC

-(void)dealloc {
    [Audio checkResult:AudioOutputUnitStop(_audioSpeakerUnit)
             operation:"Failed to uninitialize Speaker unit"];
    [Audio checkResult:AudioUnitUninitialize(_audioSpeakerUnit)
             operation:"Failed to uninitialize Speaker unit"];
    [Audio checkResult:AudioComponentInstanceDispose(_audioSpeakerUnit)
             operation:"Failed to uninitialize Speaker unit"];
    [super dealloc];
}

@end
