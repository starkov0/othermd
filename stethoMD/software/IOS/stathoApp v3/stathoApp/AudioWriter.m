//
//  AudioWriter.m
//  inspired from EZAudio
//  pierre starkov - plug'n'scope - 2014

#import "AudioWriter.h"

#import "Audio.h"

@interface AudioWriter (){
    ExtAudioFileRef             _destinationFile;
    AudioFileTypeID             _destinationFileTypeID;
    CFURLRef                    _destinationFileURL;
    AudioStreamBasicDescription _destinationFormat;
    AudioStreamBasicDescription _sourceFormat;
}

@end

@implementation AudioWriter

//---------------------------------------------------------- INIT

-(AudioWriter*)initWithDestinationURL:(NSURL*)url {
    @synchronized (self) {
        self = [super init];
        if( self )
        {
            _destinationFile        = NULL;
            _destinationFileURL     = (__bridge CFURLRef)url;
            _sourceFormat           = [Audio monoFloatFormatWithSampleRate:44100.0];
            _destinationFormat      = [Audio M4AFormatWithNumberOfChannels:_sourceFormat.mChannelsPerFrame
                                                                sampleRate:_sourceFormat.mSampleRate];
            _destinationFileTypeID  = kAudioFileM4AType;

            // configure Writer
            [self _configureWriter];

        }
        return self;
    };
}

+(AudioWriter*)audioWriterWithDestinationURL:(NSURL*)url {
    return [[AudioWriter alloc] initWithDestinationURL:url];
}

//---------------------------------------------------------- CONFIG

-(void)_configureWriter {
    // Finish filling out the destination format description
    UInt32 propSize = sizeof(_destinationFormat);
    [Audio checkResult:AudioFormatGetProperty(kAudioFormatProperty_FormatInfo,
                                                0,
                                                NULL,
                                                &propSize,
                                                &_destinationFormat)
               operation:"Failed to fill out rest of destination format"];
    
    // Create the audio file
    [Audio checkResult:ExtAudioFileCreateWithURL(_destinationFileURL,
                                                   _destinationFileTypeID,
                                                   &_destinationFormat,
                                                   NULL,
                                                   kAudioFileFlags_EraseFile,
                                                   &_destinationFile)
               operation:"Failed to create audio file"];
    
    // Set the client format (which should be equal to the source format)
    [Audio checkResult:ExtAudioFileSetProperty(_destinationFile,
                                                 kExtAudioFileProperty_ClientDataFormat,
                                                 sizeof(_sourceFormat),
                                                 &_sourceFormat)
               operation:"Failed to set client format on recorded audio file"];
    
}

//---------------------------------------------------------- EVENT

-(void)appendDataFromBufferList:(AudioBufferList *)bufferList
                 withBufferSize:(UInt32)bufferSize {
    @synchronized (self) {
        if( _destinationFile != NULL )
        {
            [Audio checkResult:ExtAudioFileWrite(_destinationFile,
                                                        bufferSize,
                                                        bufferList)
                       operation:"Failed to write audio data to recorded audio file"];
        }
    }
}

//---------------------------------------------------------- GETTER

-(NSURL *)url {
    return (__bridge NSURL*)_destinationFileURL;
}

//---------------------------------------------------------- DEALLOC

-(void)dealloc {
    @synchronized (self) {
        if( _destinationFile ) {
            // Dispose of the audio file reference
            [Audio checkResult:ExtAudioFileDispose(_destinationFile)
                     operation:"Failed to close audio file"];
            
            // Null out the file reference
            _destinationFile = NULL;
        }
    }
    [super dealloc];
}

@end