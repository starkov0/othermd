//
//  Audio.h
//  inspired from EZAudio
//  pierre starkov - plug'n'scope - 2014

#import <Foundation/Foundation.h>
#import <AudioToolbox/AudioToolbox.h>
#import "TPCircularBuffer.h"

/**
 EZAudio is a simple, intuitive framework for iOS and OSX. The goal of EZAudio was to provide a modular, cross-platform framework to simplify performing everyday audio operations like getting microphone input, creating audio waveforms, recording/playing audio files, etc. The visualization tools like the EZAudioPlot and EZAudioPlotGL were created to plug right into the framework's various components and provide highly optimized drawing routines that work in harmony with audio callback loops. All components retain the same namespace whether you're on an iOS device or a Mac computer so an EZAudioPlot understands it will subclass an UIView on an iOS device or an NSView on a Mac.
 
 Class methods for EZAudio are provided as utility methods used throughout the other modules within the framework. For instance, these methods help make sense of error codes (checkResult:operation:), map values betwen coordinate systems (MAP:leftMin:leftMax:rightMin:rightMax:), calculate root mean squared values for buffers (RMS:length:), etc.
 */
@interface Audio : NSObject

///-----------------------------------------------------------
/// @name AudioBufferList Utility
///-----------------------------------------------------------

/**
 Allocates an AudioBufferList structure. Make sure to call freeBufferList when done using AudioBufferList or it will leak.
 @param frames The number of frames that will be stored within each audio buffer
 @param channels The number of channels (e.g. 2 for stereo, 1 for mono, etc.)
 @param interleaved Whether the samples will be interleaved (if not it will be assumed to be non-interleaved and each channel will have an AudioBuffer allocated)
 @return An AudioBufferList struct that has been allocated in memory
 */
+(AudioBufferList *)audioBufferListWithNumberOfFrames:(UInt32)frames
                                     numberOfChannels:(UInt32)channels
                                          interleaved:(BOOL)interleaved;

/**
 Deallocates an AudioBufferList structure from memory.
 @param bufferList A pointer to the buffer list you would like to free
 */
+(void)freeBufferList:(AudioBufferList*)bufferList;

///-----------------------------------------------------------
/// @name Creating An AudioStreamBasicDescription
///-----------------------------------------------------------

/**
 
 @param channels   The desired number of channels
 @param sampleRate The desired sample rate
 @return A new AudioStreamBasicDescription with the specified format.
 */
+(AudioStreamBasicDescription)AIFFFormatWithNumberOfChannels:(UInt32)channels
                                                  sampleRate:(float)sampleRate;

/**
 
 @param sampleRate The desired sample rate
 @return A new AudioStreamBasicDescription with the specified format.
 */
+(AudioStreamBasicDescription)iLBCFormatWithSampleRate:(float)sampleRate;

/**
 
 @param channels   The desired number of channels
 @param sampleRate The desired sample rate
 @return A new AudioStreamBasicDescription with the specified format.
 */
+(AudioStreamBasicDescription)M4AFormatWithNumberOfChannels:(UInt32)channels
                                                 sampleRate:(float)sampleRate;

/**
 
 @param sampleRate The desired sample rate
 @return A new AudioStreamBasicDescription with the specified format.
 */
+(AudioStreamBasicDescription)monoFloatFormatWithSampleRate:(float)sampleRate;

/**
 
 @param sampleRate The desired sample rate
 @return A new AudioStreamBasicDescription with the specified format.
 */
+(AudioStreamBasicDescription)monoCanonicalFormatWithSampleRate:(float)sampleRate;

/**
 
 @param sampleRate The desired sample rate
 @return A new AudioStreamBasicDescription with the specified format.
 */
+(AudioStreamBasicDescription)monoPlugNScopeFormatWithSampleRate:(float)sampleRate;


/**
 
 @param sampleRate The desired sample rate
 @return A new AudioStreamBasicDescription with the specified format.
 */
+(AudioStreamBasicDescription)stereoCanonicalNonInterleavedFormatWithSampleRate:(float)sampleRate;

/**
 
 @param sampleRate The desired sample rate
 @return A new AudioStreamBasicDescription with the specified format.
 */
+(AudioStreamBasicDescription)stereoFloatInterleavedFormatWithSampleRate:(float)sampleRate;

/**
 
 @param sampleRate The desired sample rate
 @return A new AudioStreamBasicDescription with the specified format.
 */
+(AudioStreamBasicDescription)stereoFloatNonInterleavedFormatWithSampleRate:(float)sameRate;

///-----------------------------------------------------------
/// @name AudioStreamBasicDescription Utilities
///-----------------------------------------------------------

/**
 Nicely logs out the contents of an AudioStreamBasicDescription struct
 @param 	asbd 	The AudioStreamBasicDescription struct with content to print out
 */
+(void)printASBD:(AudioStreamBasicDescription)asbd;

///-----------------------------------------------------------
/// @name OSStatus Utility
///-----------------------------------------------------------

/**
 Basic check result function useful for checking each step of the audio setup process
 @param result    The OSStatus representing the result of an operation
 @param operation A string (const char, not NSString) describing the operation taking place (will print if fails)
 */
+(void)checkResult:(OSStatus)result
         operation:(const char*)operation;

///-----------------------------------------------------------
/// @name TPCircularBuffer Utility
///-----------------------------------------------------------

/**
 Appends the data from the audio buffer list to the circular buffer
 @param circularBuffer  Pointer to the instance of the TPCircularBuffer to add the audio data to
 @param audioBufferList Pointer to the instance of the AudioBufferList with the audio data
 @param bufferSize      Size of the audioBufferList
 */
+(void)appendDataToCircularBuffer:(TPCircularBuffer*)circularBuffer
              fromAudioBufferList:(AudioBufferList*)audioBufferList;

/**
 Initializes the circular buffer (just a wrapper around the C method)
 *  @param circularBuffer Pointer to an instance of the TPCircularBuffer
 *  @param size           The length of the TPCircularBuffer (usually 1024)
 */
+(void)circularBuffer:(TPCircularBuffer*)circularBuffer
             withSize:(int)size;

/**
 Frees a circular buffer
 @param circularBuffer Pointer to the circular buffer to clear
 */
+(void)freeCircularBuffer:(TPCircularBuffer*)circularBuffer;

@end
