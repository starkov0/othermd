//
//  AudioContainer.h
//  pierre starkov - plug'n'scope - 2014

#import <Foundation/Foundation.h>

@class AudioController;
@class ViewContainer;

///-----------------------------------------------------------
/// @name Class
///-----------------------------------------------------------

@interface AudioContainer : NSObject

///-----------------------------------------------------------
/// @name Initializers
///-----------------------------------------------------------

/**
 * returns an audio container
 */
+(AudioContainer*) audioContainer;

///-----------------------------------------------------------
/// @name GETTERS
///-----------------------------------------------------------

/**
 * fills drawableBuffer into the given arrays
 * buffers are of the same size
 * for bufferTypes :
 * indicates, for each variable, wether it comes from the microphone, micrphone + writeInFile, file
 * 0: listener
 * 1: writer
 * 2: reader
 */
-(void) fillDrawableAudio:(NSMutableArray*)drawableBuffer
                   withX0:(int)x0
                       x1:(int)x1
                viewWidth:(int)viewWidth;

/**
 * file's total frames -> number of data per file
 */
-(int) totalFrames;

/**
 * file's total duration
 */
-(float) totalDuration;

-(int) frameSize;

/**
 * frame position
 */
-(int) currentFrame;

-(NSString*) audioType;

///-----------------------------------------------------------
/// @name SETTERS
///-----------------------------------------------------------

/**
 * frame position is the reading position
 */
-(void) setCurrentFrame:(int)currentFrame;

///-----------------------------------------------------------
/// @name START AUDIO MODE
///-----------------------------------------------------------

/**
 * removes all the data that is not drawn
 */
-(void) startInputAudioWithType:(NSString*)type;

/**
 * removes all the data from audioBuffer
 * fill the audioBuffer with array's data
 */
-(void) startOutputAudioWithAudioArray:(NSMutableArray*)array
                         totalDuration:(float)totalDuration;

///-----------------------------------------------------------
/// @name ADD AUDIO DATA
///-----------------------------------------------------------

/**
 * add input points
 * it will use audioBuffer and waitingBuffer in order to limit audiobuffer's size 
 */
-(void) addInputAudio:(float)audio
               withX0:(int)x0
                   x1:(int)x1
            viewWidth:(int)viewWidth;

/**
 * fill outputAudio with data from audioBuffer
 * update currentFrame + frameIndex + sets eof
 */
-(void) fillOutputAudio:(NSMutableArray*)outputAudio;

///-----------------------------------------------------------
/// @name EVENTS
///-----------------------------------------------------------

/**
 * update audio buffers
 */
-(void) updateInputBuffersWithX0:(int)x0
                              x1:(int)x1
                      viewWidith:(int)viewWidth;

@end
