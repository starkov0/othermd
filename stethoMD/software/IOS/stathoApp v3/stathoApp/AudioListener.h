//
//  AudioListener.h
//  inspired from EZAudio
//  pierre starkov - plug'n'scope - 2014

#import  <Foundation/Foundation.h>
#import  <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVFoundation.h>
#import  "TargetConditionals.h"

@class Audio;
@class AudioListener;

///-----------------------------------------------------------
/// @name Delegate
///-----------------------------------------------------------

/**
 The delegate for the  AudioListener provides a receiver for the incoming audio data events.
 */
@protocol AudioListenerDelegate <NSObject>

@optional
/**
 Returns back the buffer list containing the audio received. This occurs on the background thread so any drawing code must explicity perform its functions on the main thread.
 @param AudioListener       The instance of the  AudioListener that triggered the event.
 @param bufferList       The AudioBufferList holding the audio data.
 @param bufferSize       The size of each of the buffers of the AudioBufferList.
 @param numberOfChannels The number of channels for the incoming audio.
 @warning This function executes on a background thread to avoid blocking any audio operations. If operations should be performed on any other thread (like the main thread) it should be performed within a dispatch block like so: dispatch_async(dispatch_get_main_queue(), ^{ ...Your Code... })
 */
-(void)    audioListener:(AudioListener*)audioListener
        withBufferList:(AudioBufferList*)bufferList
          withBufferSize:(UInt32)bufferSize;
@end

///-----------------------------------------------------------
/// @name Classe
///-----------------------------------------------------------

/**
 The  AudioListener provides a component to get audio data from the default device AudioListener. On OSX this is the default selected Listener device in the system preferences while on iOS this defaults to use the default RemoteIO audio unit. The AudioListener data is converted to a float buffer array and returned back to the caller via the  AudioListenerDelegate protocol.
 */
@interface AudioListener : NSObject

/**
 The  AudioListenerDelegate for which to handle the AudioListener callbacks
 */
@property (nonatomic,assign) id<AudioListenerDelegate> audioListenerDelegate;

///-----------------------------------------------------------
/// @name Initializers
///-----------------------------------------------------------

/**
 Creates an instance of the  AudioListener with a delegate to respond to the audioReceived callback. This will not start fetching the audio until startFetchingAudio has been called. Use initWithAudioListenerDelegate:startsImmediately: to instantiate this class and immediately start fetching audio data.
 @param 	AudioListenerDelegate 	A  AudioListenerDelegate delegate that will receive the audioReceived callback.
 @return	An instance of the  AudioListener class. This should be strongly retained.
 */
-(AudioListener*)initWithAudioListenerDelegate:(id< AudioListenerDelegate>)audioListenerDelegate;

/**
 Creates an instance of the  AudioListener with a delegate to respond to the audioReceived callback. This will not start fetching the audio until startFetchingAudio has been called. Use AudioListenerWithDelegate:startsImmediately: to instantiate this class and immediately start fetching audio data.
 @param 	AudioListenerDelegate 	A  AudioListenerDelegate delegate that will receive the audioReceived callback.
 @return	An instance of the  AudioListener class. This should be declared as a strong property!
 */
+(AudioListener*)audioListenerWithDelegate:(id< AudioListenerDelegate>)audioListenerDelegate;

///-----------------------------------------------------------
/// @name Events
///-----------------------------------------------------------

/**
 Starts fetching audio from the default AudioListener. Will notify delegate with audioReceived callback.
 */
-(void)start;

/**
 Stops fetching audio. Will stop notifying the delegate's audioReceived callback.
 */
-(void)stop;

@end
