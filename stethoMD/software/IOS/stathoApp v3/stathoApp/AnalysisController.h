//
//  AnalysisController.h
//  stathoApp
//
//  Created by Pierre Starkov on 05/09/14.
//  Copyright (c) 2014 Pierre Starkov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AnalysisController : NSObject

-(void) analyse:(NSMutableArray*)audio analysis:(NSMutableArray*)analysis;

@end
