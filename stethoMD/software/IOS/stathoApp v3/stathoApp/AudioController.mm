//
//  AudioController.mm
//  pierre starkov - plug'n'scope - 2014

#import "AudioController.h"
#import "GraphView.h"
#import "TotalView.h"
#import "ViewContainer.h"
#import "RootViewController.h"

@interface AudioController () {
    
    // A circular buffer used by the speaker
    TPCircularBuffer circularBuffer;

    // Audio Core
    AudioListener *_audioListener;
    AudioSpeaker *_audioSpeaker;
    AudioReader *_audioReader;
    AudioWriter *_audioWriter;
    
    // connections
    AudioContainer *_audioContainer;
    GraphView *_graphView;
    TotalView *_totalView;
    AnalysisController *_analysisController;
    FilterController *_filterController;
    ViewContainer *_viewContainer;
    RootViewController *_rootViewController;

    // reading variables
    NSTimer *_readerTimer;
    AudioBufferList *_audioList;
    NSMutableArray *_audioArray;

    // State booleans
    BOOL _isListening;
    BOOL _isReading;
    BOOL _isWriting;
    BOOL _isInReadingMode;
    BOOL _isInListeningMode;
    BOOL _isInWritingMode;
    BOOL _eof;
    BOOL _filter;
    BOOL _analyze;
    
    // audio Files
    NSURL *_readURL;
    NSURL *_writeURL;
    
    // queue
    dispatch_queue_t speakerQueue;
    dispatch_queue_t listenerQueue;
    dispatch_queue_t readerQueue;
    dispatch_queue_t writerQueue;
    dispatch_queue_t containerQueue;
    dispatch_queue_t circularBufferQueue;
}
@end

@implementation AudioController

//---------------------------------------------------------- INIT

-(id) init {
    self = [super init];
    
    [self _initAudioList];
    [self _initAudioArray];
    [self _initCircularBuffer];
    [self _initSpeaker];
    [self _initListener];
    
    [self setEof:YES];
    
    // queue creation
    speakerQueue = dispatch_queue_create("speakerQueue", NULL);
    listenerQueue = dispatch_queue_create("listenerQueue", NULL);
    readerQueue = dispatch_queue_create("readerQueue", NULL);
    writerQueue = dispatch_queue_create("writerQueue", NULL);
    containerQueue = dispatch_queue_create("containerQueue", NULL);
    circularBufferQueue = dispatch_queue_create("circularBufferQueue", NULL);

    return self;
}

+(AudioController*) audioController {
    return [[AudioController alloc] init];
}

//---------------------------------------------------------- CONNECTIONS

-(void) setAudioContainer:(AudioContainer*)audioContainer {
    _audioContainer = audioContainer;
}

-(void) setGraphView:(GraphView*)graphView {
    _graphView = graphView;
}

-(void) setTotalView:(TotalView*)totalView {
    _totalView = totalView;
}

-(void) setViewContainer:(ViewContainer*)viewContainer {
    _viewContainer = viewContainer;
}

-(void) setRootViewController:(RootViewController*)rootViewController {
    _rootViewController = rootViewController;
}

//---------------------------------------------------------- URL

-(void) setReadURL:(NSURL*)readURL {
    if (_readURL != nil) {
        [_readURL release]; _readURL = nil;
    }
    _readURL = [readURL copy];
}

-(void) setWriteURL:(NSURL*)writeURL {
    if (_writeURL != nil) {
        [_writeURL release]; _writeURL = nil;
    }
    _writeURL = [writeURL copy];
}

//---------------------------------------------------------- SETTERS BOOL

-(void) setIsListening:(BOOL)isListening {
    @synchronized (self) {
        _isListening = isListening;
    }
}
-(void) setIsReading:(BOOL)isReading {
    @synchronized (self) {
        _isReading = isReading;
    }
}
-(void) setIsWriting:(BOOL)isWriting {
    @synchronized (self) {
        _isWriting = isWriting;
    }
}
-(void) setIsInReadingMode:(BOOL)isInReadingMode {
    @synchronized (self) {
        _isInReadingMode = isInReadingMode;
    }
}
-(void) setIsInListeningMode:(BOOL)isInListeningMode {
    @synchronized (self) {
        _isInListeningMode = isInListeningMode;
    }
}
-(void) setIsInWritingMode:(BOOL)isInWritingMode {
    @synchronized (self) {
        _isInWritingMode = isInWritingMode;
    }
}
-(void) setEof:(BOOL)eof {
    @synchronized (self) {
        _eof = eof;
    }
}
-(void) setFilter:(BOOL)filter {
    @synchronized (self) {
        _filter = filter;
    }
}
-(void) setAnalyse:(BOOL)analyse {
    @synchronized (self) {
        _analyze = analyse;
    }
}

//---------------------------------------------------------- GETTERS BOOL

-(BOOL) isListening {
    @synchronized (self) {
        return _isListening;
    }
}
-(BOOL) isReading {
    @synchronized (self) {
        return _isReading;
    }
}
-(BOOL) isInReadingMode {
    @synchronized (self) {
        return _isInReadingMode;
    }
}
-(BOOL) isInListeningMode {
    @synchronized (self) {
        return _isInListeningMode;
    }
}
-(BOOL) isInWritingMode {
    @synchronized (self) {
        return _isInWritingMode;
    }
}
-(BOOL) isWriting {
    @synchronized (self) {
        return _isWriting;
    }
}
-(BOOL) eof {
    @synchronized (self) {
        return _eof;
    }
}
-(BOOL) filter {
    @synchronized (self) {
        return _filter;
    }
}
-(BOOL) analyze {
    @synchronized (self) {
        return _analyze;
    }
}

//---------------------------------------------------------- AUDIO CLASSES INIT

-(void)_initSpeaker {
    if (_audioSpeaker == nil) {
        _audioSpeaker = [AudioSpeaker audioSpeakerWithDelegate:self];
    }
}
-(void)_initListener {
    if (_audioListener == nil) {
        _audioListener = [AudioListener audioListenerWithDelegate:self];
    }
}
-(void)_initReader {
    if (_audioReader == nil) {
        _audioReader = [AudioReader audioReaderWithURL:_readURL andDelegate:self];
    }
}
-(void)_initReaderTimer {
    if (_readerTimer == nil) {
        _readerTimer = [NSTimer scheduledTimerWithTimeInterval:0.0116 target:self selector:@selector(_readAudio:) userInfo:nil repeats:YES];
    }
}
-(void)_initWriter {
    if (_audioWriter == nil) {
        _audioWriter = [AudioWriter audioWriterWithDestinationURL:_writeURL];
    }
}
-(void)_initCircularBuffer {
    [Audio circularBuffer:&circularBuffer withSize:1024];
}
-(void)_initFilterController {
    if (_filterController == nil) {
        _filterController = [[FilterController alloc] init];
    }
}
-(void)_initAnalysisController {
    if (_analysisController == nil) {
        _analysisController = [[AnalysisController alloc] init];
    }
}
-(void)_initAudioList {
    if (_audioList == nil) {
        _audioList = [Audio audioBufferListWithNumberOfFrames:64 numberOfChannels:1 interleaved:YES];
    }
}
-(void)_initAudioArray {
    if (_audioArray == nil) {
        _audioArray = [[NSMutableArray alloc] init];
    }
}

//---------------------------------------------------------- AUDIO CLASSES REMOVE

-(void)_removeSpeaker {
    [_audioSpeaker stop];
    if (_audioSpeaker != nil) {
        [_audioSpeaker release];
        _audioSpeaker = nil;
    }
}
-(void)_removeListener {
    if (_audioListener != nil) {
        [_audioListener release];
        _audioListener = nil;
    }
}
-(void)_removeReader {
    if (_audioReader != nil) {
        [_audioReader release];
        _audioReader = nil;
    }
}
-(void)_removeReaderTimer {
    if (_readerTimer != nil) {
        [_readerTimer invalidate];
        _readerTimer = nil;
    }
}
-(void)_removeWriter {
    if (_audioWriter != nil) {
        [_audioWriter release];
        _audioWriter = nil;
    }
}
-(void)_removeCircularBuffer {
    [Audio freeCircularBuffer:&circularBuffer];
}
-(void)_removeFilterController {
    if (_filterController != nil) {
        [_filterController release];
        _filterController = nil;
    }
}
-(void)_removeAnalysisController {
    if (_analysisController != nil) {
        [_analysisController release];
        _analysisController = nil;
    }
}
-(void)_removeAudioList {
    if (_audioList != nil) {
        [Audio freeBufferList:_audioList];
        _audioList = nil;
    }
}
-(void)_removeAudioArray {
    if (_audioArray != nil) {
        [_audioArray removeAllObjects];
        [_audioArray release];
        _audioArray = nil;
    }
}

//---------------------------------------------------------- SET CONTAINER

-(void)_startContainerInputModeWithType:(NSString*)type {
    if (![self isInListeningMode]) {
        [_audioContainer startInputAudioWithType:type];
    }
}

-(void)_startContainerOutputMode {
    if ([self eof]) {
        NSMutableArray* totalAudioData = [[NSMutableArray alloc] init];
        [self _initReader];
        [_audioReader fillTotalAudioData:totalAudioData];
        [_audioContainer startOutputAudioWithAudioArray:totalAudioData
                                          totalDuration:[_audioReader totalDuration]];
        [self _removeReader];
        [totalAudioData removeAllObjects];
        [totalAudioData release];
        totalAudioData = nil;
        [self setEof:NO];
    }
}

//---------------------------------------------------------- SUB EVENTS

-(void)_initListening {
    [self _startContainerInputModeWithType:@"listen"];
    [_audioListener start];
    [self setEof:YES];
    [self setIsInWritingMode:NO];
    [self setIsWriting:NO];
    [self setIsInListeningMode:YES];
    [self setIsListening:YES];
    [self setIsInReadingMode:NO];
    [self setIsReading:NO];
}

-(void)_removeListening {
    [_audioListener stop];
    [self setIsListening:NO];
}

-(void)_initWriting {
    [self _startContainerInputModeWithType:@"write"];
    [_audioListener start];
    [self _initWriter];
    [self setEof:YES];
    [self setIsInWritingMode:YES];
    [self setIsWriting:YES];
    [self setIsInListeningMode:NO];
    [self setIsListening:NO];
    [self setIsInReadingMode:NO];
    [self setIsReading:NO];}

-(void)_removeWriting {
    [_audioListener stop];
    [self _removeWriter];
    [self setIsWriting:NO];
}

-(void)_initReading {
    [self _startContainerOutputMode];
    [self _initReaderTimer];
    [self setIsInWritingMode:NO];
    [self setIsWriting:NO];
    [self setIsInListeningMode:NO];
    [self setIsListening:NO];
    [self setIsInReadingMode:YES];
    [self setIsReading:YES];
}

-(void)_removeReading {
    [self _removeReaderTimer];
    [self setIsReading:NO];
}

-(void)_initSpeaking {
    [_audioSpeaker start];
}

-(void)_removeSpeaking {
    [_audioSpeaker stop];
}

//---------------------------------------------------------- EVENTS

-(void) startListening {
    [self _removeSpeaking];
    [self _removeWriting];
    [self _removeReading];
    [self _initListening];
    [self _initSpeaking];
}

-(void) stopListening {
    [self _removeSpeaking];
    [self _removeListening];
}

-(void) startReading {
    [self _removeSpeaking];
    [self _removeListening];
    [self _removeWriting];
    [self _initReading];
    [self _initSpeaking];
    [_rootViewController startUpdateTimeLabel];
}

-(void) stopReading {
    [self _removeSpeaking];
    [self _removeReading];
}

-(void) startWriting {
    [self _removeSpeaking];
    [self _removeListening];
    [self _removeReading];
    [self _initWriting];
    [self _initSpeaking];
    [_rootViewController startUpdateTimeLabel];
}

-(void) stopWriting {
    [self _removeSpeaking];
    [self _removeWriting];
    [_rootViewController stopUpdateTimeLabel];
}

//---------------------------------------------------------- LISTENER FUNCTIONS

-(void)     audioListener:(AudioListener *)audioListener
           withBufferList:(AudioBufferList *)bufferList
           withBufferSize:(UInt32)bufferSize {
    @synchronized (self) {
        
        if ([self isWriting]) {
            // file
            dispatch_async(writerQueue, ^{
                [_audioWriter appendDataFromBufferList:bufferList withBufferSize:bufferSize];
            });
            // container
            dispatch_async(containerQueue, ^{
                for (int i=0; i<bufferSize; i++) {
                    [_audioContainer addInputAudio:((float*)bufferList->mBuffers[0].mData)[i]
                                            withX0:[_viewContainer x0]
                                                x1:[_viewContainer x1]
                                         viewWidth:[_graphView viewWidth]];
                }
                // circilar buffer -> speak audio
                [self _appendDataToCircularBufferFromBufferList:bufferList];
            });
        }
        else if ([self isListening]) {
            // container
            dispatch_async(containerQueue, ^{
                for (int i=0; i<bufferSize; i++) {
                    [_audioContainer addInputAudio:((float*)bufferList->mBuffers[0].mData)[i]
                                            withX0:[_viewContainer x0]
                                                x1:[_viewContainer x1]
                                         viewWidth:[_graphView viewWidth]];
                }
                // circilar buffer -> speak audio
                [self _appendDataToCircularBufferFromBufferList:bufferList];
            });
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            [_graphView setNeedsDisplay];
            [_totalView setNeedsDisplay];
        });
    }
}

//---------------------------------------------------------- READER FUNCTIONS

-(void)_readAudio:(NSTimer *)timer {
    @synchronized (self) {
        dispatch_async(readerQueue, ^{
        // init array
        NSMutableArray* audio = [[NSMutableArray alloc] init];
        
        // get audio
        [_audioContainer fillOutputAudio:audio];
        
        // use audio data
        if ([audio count] > 0) {
            for (int i=0; i<[audio count]; i++) {
                ((float*)_audioList[0].mBuffers[0].mData)[i] = [[audio objectAtIndex:i] floatValue];
            }
            
            [self _appendDataToCircularBufferFromBufferList:_audioList];
        } else {
            // stop reading
            [self setEof:YES];
            [self stopReading];
        }
        
        // remove array
        [audio removeAllObjects];
        [audio release];
        audio = nil;
    });
    [_graphView setNeedsDisplay];
    [_totalView setNeedsDisplay];
    }
}

//---------------------------------------------------------- AUDIOLISTENER CALLBACK

-(TPCircularBuffer *)audioSpeakerShouldUseCircularBuffer:(AudioSpeaker *)Speaker {
    return &circularBuffer;
}

-(void)_appendDataToCircularBufferFromBufferList:(AudioBufferList *)bufferList {
    dispatch_async(listenerQueue, ^{
        [Audio appendDataToCircularBuffer:&circularBuffer
                      fromAudioBufferList:bufferList];
    });
}

//---------------------------------------------------------- DEALLOC

-(void) dealloc {
    // remove classes
    [self _removeCircularBuffer];
    [self _removeListener];
    [self _removeReader];
    [self _removeSpeaker];
    [self _removeReaderTimer];
    [self _removeAudioList];
    [self _removeAudioArray];
    [self _removeFilterController];
    [self _removeAnalysisController];
    [super dealloc];
}

@end
