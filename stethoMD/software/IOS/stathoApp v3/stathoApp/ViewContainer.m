//
//  ViewContainer.m
//  stathoApp
//
//  Created by Pierre Starkov on 10/09/14.
//  Copyright (c) 2014 Pierre Starkov. All rights reserved.
//

#import "ViewContainer.h"
#import "AudioContainer.h"
#import "AudioController.h"
#import "GraphView.h"

@interface ViewContainer () {
    // variables
    int _x0;
    int _x1;
    int _currentIndex;
    int _zoomMaxLimitX;
    int _zoomMinLimitX;
    float _zoomY;
    
    // color
    NSMutableDictionary *_color;
    
    // container
    AudioContainer *_audioContainer;
    
    // controller
    AudioController *_audioController;
    
    // graphView
    GraphView *_graphView;
}

@end

@implementation ViewContainer

//---------------------------------------------------------- SETTERS

-(id) init {
    self = [super init];
    
    _color = [[NSMutableDictionary alloc] init];

    [_color setObject:[[UIColor alloc] initWithRed:30.0f/255.0f green:130.0f/255.0f blue:76.0f/255.0f alpha:1.0] forKey:@"read"];
    [_color setObject:[[UIColor alloc] initWithRed:31.0f/255.0f green:58.0f/255.0f blue:147.0f/255.0f alpha:1.0] forKey:@"listen"];
    [_color setObject:[[UIColor alloc] initWithRed:211.0f/255.0f green:84.0f/255.0f blue:0.0f/255.0f alpha:1.0] forKey:@"write"];
    [_color setObject:[[UIColor alloc] initWithRed:34.0f/255.0f green:49.0f/255.0f blue:63.0f/255.0f alpha:1.0] forKey:@"index"];
    [_color setObject:[[UIColor alloc] initWithRed:249.0f/255.0f green:237.0f/255.0f blue:237.0f/255.0f alpha:1.0] forKey:@"graphViewBackGround"];
    [_color setObject:[[UIColor alloc] initWithRed:202.0f/255.0f green:241.0f/255.0f blue:202.0f/255.0f alpha:1.0] forKey:@"totalViewBackGround"];
    
    return self;
}

-(void) setZoomMaxLimitX:(int)zoomMaxLimitX
           zoomMinLimitX:(int)zoomMinLimitX {
    // limit
    _zoomMaxLimitX = zoomMaxLimitX;
    _zoomMinLimitX = zoomMinLimitX;
    
    // x0 - x1
    _x0 = 0;
    _x1 = _zoomMaxLimitX/10;
    
    // zoom Y
    _zoomY = 2.05;
}

-(void) setAudioController:(AudioController*)audioController {
    _audioController = audioController;
}

-(void) setAudioContainer:(AudioContainer*)audioContainer {
    _audioContainer = audioContainer;
}

-(void) setGraphView:(GraphView*)graphView {
    _graphView = graphView;
}

//---------------------------------------------------------- GETTERS

-(int) x0 {
    @synchronized (self) {
        return _x0;
    }
}

-(int) x1 {
    @synchronized (self) {
        return _x1;
    }
}

-(UIColor*) colorWithName:(NSString*)name {
    return [_color objectForKey:name];
}

-(float) zoomY {
    return _zoomY;
}

-(int) dx {
    return (int)round((_x1 - _x0) / [_graphView viewWidth]);
}

-(int) totalViewDx {
    return (int)round([_audioContainer totalFrames] / [_graphView viewWidth]);
}

//---------------------------------------------------------- TRANSLATION

-(void) translationX0X1CurrentFrameReset {
    @synchronized (self) {
        [_audioContainer setCurrentFrame:0];
        _x1 -= _x0;
        _x0 = 0;
    }
}
//
-(void) translateCurrentFrameWithFrame:(int)translationValue {
    @synchronized (self) {
        [_audioContainer setCurrentFrame:[_audioContainer currentFrame] + translationValue];
    }
}
-(void) translateCurrentFrameWithPosition:(int)translationValue {
    [self translateCurrentFrameWithFrame:translationValue * [self dx]];
}
//
-(void) translateX0X1WithFrames:(int)translationValue {
    @synchronized (self) {
        // var
        if([_audioContainer totalFrames] > [_graphView viewWidth] * [self dx]){
            // left limit
            if (_x0 + translationValue < 0) {
                _x1 -= _x0;
                _x0 = 0;
            }
            // right limit
            else if (_x1 + translationValue >= [_audioContainer totalFrames]) {
                _x0 += [_audioContainer totalFrames] - _x1;
                _x1 = [_audioContainer totalFrames];
            }
            // middle
            else {
                _x0 += translationValue;
                _x1 += translationValue;
            }
        }
    }
}
-(void) translateX0X1WithPosition:(int)translationValue {
    [self translateX0X1WithFrames:translationValue * [self dx]];
}
//


-(void) translateX0X1CurrentFrameWithFrames:(int)translationValue {
    @synchronized (self) {
        // var
        if([_audioContainer totalFrames] > [_graphView viewWidth] * [self dx]){
            // left limit
            if (_x0 + translationValue < 0) {
                [_audioContainer setCurrentFrame:[_audioContainer currentFrame] - _x0];
                _x1 -= _x0;
                _x0 = 0;
            }
            // right limit
            else if (_x1 + translationValue >= [_audioContainer totalFrames]) {
                [_audioContainer setCurrentFrame:[_audioContainer currentFrame] + [_audioContainer totalFrames] - _x1];
                _x0 += [_audioContainer totalFrames] - _x1;
                _x1 = [_audioContainer totalFrames];
            }
            // middle
            else {
                [_audioContainer setCurrentFrame:[_audioContainer currentFrame] + translationValue];
                _x0 += translationValue;
                _x1 += translationValue;
            }
        }
    }
}

-(void) translateX0X1CurrentFrameWithPosition:(int)translationValue {
    [self translateX0X1CurrentFrameWithFrames:translationValue * [self dx]];
}

//---------------------------------------------------------- ZOOM

-(void) zoomXReset {
    
}

-(void) zoomYReset {
    _zoomY = 1.0;
}

-(void) zoomX:(float)zoomX {
    @synchronized (self) {
        int dx = (int)round((_x1 - _x0) / [_graphView viewWidth]);
        if ([_audioController isInReadingMode]) {
            // frame percentage
            float percentageLeft = ((float)[_audioContainer currentFrame] - _x0) / (_x1 - _x0);
            float percentageRight = 1 - percentageLeft;
            if ([_audioContainer totalFrames] < _x1) {
                percentageLeft = 0;
                percentageRight = 1;
            }
            int zoomX1 = -(int)(percentageLeft * zoomX * dx);
            int zoomX2 = (int)(percentageRight * zoomX * dx);
            [self readingModeZoomX1:zoomX1 zoomX2:zoomX2];
        }
        else {
            [self nonReadingModeZoomX:zoomX * dx];
        }
    }
}


-(void) readingModeZoomX1:(int)zoomX1
                   zoomX2:(int)zoomX2 {
    // x0
    if (zoomX1 > 0) {
        if (_x0 + zoomX1 + _zoomMinLimitX < _x1) {
            _x0 += zoomX1;
        } else {
            _x0 = _x1 - _zoomMinLimitX;
        }
    } else {
        if (_x0 + zoomX1 > 0) {
            _x0 += zoomX1;
        } else {
            _x0 = 0;
        }
    }
    // x1
    if (zoomX2 > 0) {
        if (_x1 + zoomX2 < _zoomMaxLimitX) {
            _x1 += zoomX2;
        } else {
            _x1 = _zoomMaxLimitX;
        }
    } else {
        if (_x1 + zoomX2 + _x0 > _zoomMinLimitX) {
            _x1 += zoomX2;
        } else {
            _x1 = _zoomMinLimitX + _x0;
        }
    }
}

-(void) nonReadingModeZoomX:(int)zoomX {
    if (zoomX > 0) {
        if (_x1 + zoomX < _zoomMaxLimitX) {
            _x1 += zoomX;
        } else {
            _x1 = _zoomMaxLimitX;
        }
    } else {
        if (_x1 + zoomX + _x0 > _zoomMinLimitX) {
            _x1 += zoomX;
        } else {
            _x1 = _zoomMinLimitX + _x0;
        }
    }
}

-(void) setZoomY:(float)zoomY {
    _zoomY = zoomY;
}

//---------------------------------------------------------- DEALLOC

-(void) dealloc {
    [_color release];
    [super dealloc];
}


@end
