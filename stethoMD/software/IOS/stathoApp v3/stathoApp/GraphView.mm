//
//  GraphView.mm
//  pierre starkov - plug'n'scope - 2014

#import "GraphView.h"
#import "AudioContainer.h"
#import "AudioController.h"
#import "ViewContainer.h"

@interface GraphView () {
    
    // connections
    AudioContainer *_audioContainer;
    AudioController *_audioController;
    ViewContainer *_viewContainer;
    
    // view
    int _viewPositionWidth;
    int _viewPositionHeight;
    int _lineWidth;
}
@end

@implementation GraphView

//---------------------------------------------------------- INIT

-(void) awakeFromNib {
    [super awakeFromNib];
    _lineWidth           = 1;
    _viewPositionWidth   = 4;
    _viewPositionHeight  = self.frame.size.height/2;
}

-(void) setAudioController:(AudioController*)audioController {
    _audioController = audioController;
}

-(void) setAudioContainer:(AudioContainer*)audioContainer {
    _audioContainer = audioContainer;
}

-(void) setViewContainer:(ViewContainer*)viewContainer {
    _viewContainer = viewContainer;
    self.backgroundColor = (UIColor*)[_viewContainer colorWithName:@"graphViewBackGround"];
}

//---------------------------------------------------------- GETTERS

-(int) viewWidth {
    @synchronized (self) {
        return (int)self.frame.size.width;
    }
}

-(int) viewPosition {
    return (int)round(((float)([_audioContainer currentFrame] - [_viewContainer x0]))
               / ([_viewContainer x1] - [_viewContainer x0]) * self.frame.size.width);
}

//---------------------------------------------------------- FILL POINT ARRAY

-(void)_fillPositivePoints:(NSMutableArray*)positivePoints
            negativePoints:(NSMutableArray*)negativePoints
                 withAudio:(NSMutableArray*)audio {
    for (int i=0; i<[audio count]; i++) {
        float pointX = i;
        float pointYP, pointYN;
        if ([[audio objectAtIndex:i] floatValue] >= 0) {
            pointYP = (self.frame.size.height / 2) * ([_viewContainer zoomY] - [audio[i] floatValue]) / [_viewContainer zoomY];
            pointYN = (self.frame.size.height / 2) * ([_viewContainer zoomY] - ((-1) * [audio[i] floatValue])) / [_viewContainer zoomY];
        } else {
            pointYN = (self.frame.size.height / 2) * ([_viewContainer zoomY] - [audio[i] floatValue]) / [_viewContainer zoomY];
            pointYP = (self.frame.size.height / 2) * ([_viewContainer zoomY] - ((-1) * [audio[i] floatValue])) / [_viewContainer zoomY];
        }
        [positivePoints addObject:[NSValue valueWithCGPoint:CGPointMake(pointX, pointYP)]];
        [negativePoints addObject:[NSValue valueWithCGPoint:CGPointMake(pointX, pointYN)]];
    }
    [positivePoints insertObject:[NSValue valueWithCGPoint:CGPointMake([(NSValue *)[positivePoints firstObject] CGPointValue].x, self.frame.size.height/2)] atIndex:0];
    [positivePoints addObject:[NSValue valueWithCGPoint:CGPointMake([(NSValue *)[positivePoints lastObject] CGPointValue].x, self.frame.size.height/2)]];
    [negativePoints insertObject:[NSValue valueWithCGPoint:CGPointMake([(NSValue *)[negativePoints firstObject] CGPointValue].x, self.frame.size.height/2)] atIndex:0];
    [negativePoints addObject:[NSValue valueWithCGPoint:CGPointMake([(NSValue *)[negativePoints lastObject] CGPointValue].x, self.frame.size.height/2)]];
    
}

//---------------------------------------------------------- DRAWRECT

- (void)drawRect:(CGRect)rect {
    @synchronized (self) {
        [self _drawAudioGraph];
        if([_audioController isInReadingMode]) {
            int currentPosition = (int) round([_audioContainer currentFrame] / [_viewContainer dx]);
            
            int x0Position = (int) round([_viewContainer x0] / [_viewContainer dx]);
            if (currentPosition < x0Position) {
                [_viewContainer translateX0X1WithPosition:currentPosition - x0Position];
            }
            
            int x1Position = (int) round([_viewContainer x1] / [_viewContainer dx]);
            if (currentPosition > x1Position) {
                [_viewContainer translateX0X1WithPosition:currentPosition - x1Position];
            }
            
            [self _drawViewPosition:[self viewPosition]];
        }
    }
}

//---------------------------------------------------------- DRAW AUDIO DATA

-(void)_drawAudioGraph {
    // init array
    NSMutableArray *drawableAudio   = [[NSMutableArray alloc] init];
    NSMutableArray *positivePoints  = [[NSMutableArray alloc] init];
    NSMutableArray *negativePoints  = [[NSMutableArray alloc] init];
    // fill array
    [_audioContainer fillDrawableAudio:drawableAudio
                                withX0:[_viewContainer x0]
                                    x1:[_viewContainer x1]
                             viewWidth:self.frame.size.width];
    // draw
    [self _fillPositivePoints:positivePoints negativePoints:negativePoints withAudio:drawableAudio];
    [self _drawSurfaceFromPoints:positivePoints withColor:(UIColor*)[_viewContainer colorWithName:[_audioContainer audioType]]];
    [self _drawSurfaceFromPoints:negativePoints withColor:(UIColor*)[_viewContainer colorWithName:[_audioContainer audioType]]];
    // remove arrays
    [positivePoints release];
    [negativePoints release];
    [drawableAudio release];
}

//---------------------------------------------------------- DRAW INDEX POSITON

-(void)_drawViewPosition:(int)viewPosition {
    // init array
    NSMutableArray *viewPositionPoints = [[NSMutableArray alloc] init];
    // X variables
    int XIndex0, XIndex1;
    if (viewPosition - _viewPositionWidth < 0) {
        XIndex0 = 0;
        XIndex1 = _viewPositionWidth;
    }
    else if (viewPosition + _viewPositionWidth > self.frame.size.width) {
        XIndex0 = self.frame.size.width - _viewPositionWidth;
        XIndex1 = self.frame.size.width;
    }
    else {
        XIndex0 = viewPosition - _viewPositionWidth/2;
        XIndex1 = viewPosition + _viewPositionWidth/2;
    }
    // fill points
    [viewPositionPoints addObject:[NSValue valueWithCGPoint:CGPointMake(XIndex0, self.frame.size.height/2+_viewPositionHeight/2)]];
    [viewPositionPoints addObject:[NSValue valueWithCGPoint:CGPointMake(XIndex1, self.frame.size.height/2+_viewPositionHeight/2)]];
    [viewPositionPoints addObject:[NSValue valueWithCGPoint:CGPointMake(XIndex1, self.frame.size.height/2-_viewPositionHeight/2)]];
    [viewPositionPoints addObject:[NSValue valueWithCGPoint:CGPointMake(XIndex0, self.frame.size.height/2-_viewPositionHeight/2)]];
    //draw
    [self _drawSurfaceFromPoints:viewPositionPoints withColor:(UIColor*)[_viewContainer colorWithName:@"index"]];
    // release array
    [viewPositionPoints release];
}

//---------------------------------------------------------- DRAW SURFACE

-(void)_drawSurfaceFromPoints:(NSMutableArray*)points
                    withColor:(UIColor*)color{
    if ([points count] > 0){
        // draw
        CGContextRef context = UIGraphicsGetCurrentContext();
        CGContextSetLineWidth(context, _lineWidth);
        CGContextSetFillColorWithColor(context, color.CGColor);
        CGContextMoveToPoint(context, [points[0] CGPointValue].x, [points[0] CGPointValue].y);
        for (NSUInteger i = 1; i < [points count]; i++)
        {
            CGContextAddLineToPoint(context, [points[i] CGPointValue].x, [points[i] CGPointValue].y);
        }
        CGContextFillPath(context);
    }
}

@end
