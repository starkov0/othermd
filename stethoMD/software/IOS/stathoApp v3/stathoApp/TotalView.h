//
//  GraphView.h
//  pierre starkov - plug'n'scope - 2014


#import <UIKit/UIKit.h>

@class AudioContainer;
@class AudioController;
@class GraphView;
@class ViewContainer;

@interface TotalView : UIView

///-----------------------------------------------------------
/// @name GETTERS
///-----------------------------------------------------------

-(int) viewWidth;

///-----------------------------------------------------------
/// @name SETTERS
///-----------------------------------------------------------

/**
 * set controller
 */
-(void) setAudioController:(AudioController*)audioController;

/**
 * set container
 */
-(void) setAudioContainer:(AudioContainer*)audioContainer;

-(void) setViewContainer:(ViewContainer*)viewContainer;

@end
