//
//  AudioSpeaker.h
//  inspired from EZAudio
//  pierre starkov - plug'n'scope - 2014

#import <Foundation/Foundation.h>
#import <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVFoundation.h>
#import "TPCircularBuffer.h"

@class AudioSpeaker;

///-----------------------------------------------------------
/// @name Delegate
///-----------------------------------------------------------

/**
 The AudioSpeakerDelegate (required for the AudioSpeaker) specifies a receiver to provide audio data when the AudioSpeaker is started. Only ONE Delegate method is expected to be implemented and priority is given as such:
   1.) `Speaker:callbackWithActionFlags:inTimeStamp:inBusNumber:inNumberFrames:ioData:`
   2.) `SpeakerShouldUseCircularBuffer:`
   3.) `Speaker:needsBufferListWithFrames:withBufferSize:`
 */
@protocol AudioSpeakerDelegate <NSObject>

@optional

/**
 Provides Speaker using a circular
 @param Speaker The instance of the AudioSpeaker that asked for the data
 @return The AudioSpeakerDelegate's TPCircularBuffer structure holding the audio data in a circular buffer
 */
-(TPCircularBuffer*)audioSpeakerShouldUseCircularBuffer:(AudioSpeaker *)Speaker;

-(void)       audioSpeaker:(AudioSpeaker *)audioSpeaker
 shouldFillAudioBufferList:(AudioBufferList*)audioBufferList
        withNumberOfFrames:(UInt32)frames;
@end

///-----------------------------------------------------------
/// @name Class
///-----------------------------------------------------------

/**
 The AudioSpeaker component provides a generic Speaker to glue all the other EZAudio components together and push whatever sound you've created to the default Speaker device (think opposite of the microphone). The AudioSpeakerDelegate provides the required AudioBufferList needed to populate the Speaker buffer.
 */
@interface AudioSpeaker : NSObject

/**
 The AudioSpeakerDelegate that provides the required AudioBufferList to the Speaker callback function
 */
@property (nonatomic,assign) id<AudioSpeakerDelegate>audioSpeakerDelegate;

///-----------------------------------------------------------
/// @name Initializers
///-----------------------------------------------------------

/**
 Creates a new instance of the AudioSpeaker and allows the caller to specify an AudioSpeakerDelegate.
 @param delegate The AudioSpeakerDelegate that will be used to pull the audio data for the Speaker callback.
 @return A newly created instance of the AudioSpeaker class.
 */
-(id)initWithDelegate:(id<AudioSpeakerDelegate>)delegate;

///-----------------------------------------------------------
/// @name Class Initializers
///-----------------------------------------------------------

/**
 Class method to create a new instance of the AudioSpeaker and allows the caller to specify an AudioSpeakerDelegate.
 @param delegate The AudioSpeakerDelegate that will be used to pull the audio data for the Speaker callback.
 @return A newly created instance of the AudioSpeaker class.
 */
+(AudioSpeaker*)audioSpeakerWithDelegate:(id<AudioSpeakerDelegate>)delegate;

///-----------------------------------------------------------
/// @name Events
///-----------------------------------------------------------

/**
 Starts pulling audio data from the AudioSpeakerDelegate to the default device Speaker.
 */
-(void)start;

/**
 Stops pulling audio data from the AudioSpeakerDelegate to the default device Speaker.
 */
-(void)stop;

@end
