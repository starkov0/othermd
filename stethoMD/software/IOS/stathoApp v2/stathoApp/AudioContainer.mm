//
//  DataController.m
//  stathoApp
//
//  Created by Pierre Starkov on 03/07/14.
//  Copyright (c) 2014 Pierre Starkov. All rights reserved.
//

#import "AudioContainer.h"

@implementation AudioContainer

@synthesize audio;

-(id)init {
    self = [super init];
    audio = [[NSMutableArray alloc] init];
    return self;
}

-(void)addSingleFloat:(float)singleAudioData {
    [audio addObject:@(singleAudioData)];
}

-(void)addSingleNSNumber:(NSNumber*)singleAudioData {
    [audio addObject:singleAudioData];
}

-(void)addFloatArray:(float*)array withLength:(long)length {
    for (long i=0; i<length; i++) {
        [audio addObject:@(array[i])];
    }
}

-(void)addNSMutableArray:(NSMutableArray*)array {
    [audio addObjectsFromArray:array];
}

@end