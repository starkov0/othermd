//
//  PointsFiltering.h
//  stathoApp
//
//  Created by Pierre Starkov on 04/08/14.
//  Copyright (c) 2014 Pierre Starkov. All rights reserved.
//

#import <Foundation/Foundation.h>

@class BasicStatistics;

@interface PointsFiltering : NSObject

@property (retain) BasicStatistics *basicStatistics;

@end
