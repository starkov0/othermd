//
//  BasicStatistics.h
//  stathoApp
//
//  Created by Pierre Starkov on 03/08/14.
//  Copyright (c) 2014 Pierre Starkov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BasicStatistics : NSObject

- (NSNumber *)sum:(NSMutableArray*)array;
- (NSNumber *)mean:(NSMutableArray*)array;
- (NSNumber *)min:(NSMutableArray*)array;
- (NSNumber *)max:(NSMutableArray*)array;
- (NSNumber *)median:(NSMutableArray*)array;
- (NSNumber *)standardDeviation:(NSMutableArray*)array;

@end
