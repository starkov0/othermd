//
//  PointsFiltering.m
//  stathoApp
//
//  Created by Pierre Starkov on 04/08/14.
//  Copyright (c) 2014 Pierre Starkov. All rights reserved.
//

#import "PointsFiltering.h"
#import "BasicStatistics.h"

@implementation PointsFiltering

-(id) init {
    self = [super init];
    
    self.basicStatistics = [[BasicStatistics alloc] init];
    
    return self;
}

-(NSMutableArray*) computeSlidingMeanPointsForPoints:(NSMutableArray*)points withKernelSize:(int)kernelSize {
    
    if (kernelSize > [points count]) {
        kernelSize = (int)[points count];
    }
    
    NSMutableArray *tmpPoints = [NSMutableArray arrayWithArray:points];
    NSMutableArray *slidingMeanPoints = [NSMutableArray array];
    int toAdd = floor(kernelSize/2);
    
    for(int i=1; i<toAdd; i++) {
        [tmpPoints insertObject:[points objectAtIndex:i] atIndex:0];
        [tmpPoints addObject:[points objectAtIndex:[points count]-1-i]];
    }
    
    for (int i=toAdd; i<[tmpPoints count]-toAdd; i++) {
        NSMutableArray *valuesY = [NSMutableArray array];
        for (int j=i-toAdd; j<=i+toAdd; j++) {
            [valuesY addObject:@([(NSValue *)tmpPoints[j] CGPointValue].y)];
        }
        CGPoint p = CGPointMake([(NSValue *)tmpPoints[i] CGPointValue].x, [[self.basicStatistics mean:valuesY] floatValue]);
        [slidingMeanPoints addObject:[NSValue valueWithCGPoint:p]];
    }
    
    return slidingMeanPoints;
}

-(NSMutableArray*) computeSlidingMedianPointsForPoints:(NSMutableArray*)points withKernelSize:(int)kernelSize {
    
    if (kernelSize > [points count]) {
        kernelSize = (int)[points count];
    }
    
    NSMutableArray *tmpPoints = [NSMutableArray arrayWithArray:points];
    NSMutableArray *slidingMeanPoints = [NSMutableArray array];
    int toAdd = floor(kernelSize/2);
    
    for(int i=1; i<toAdd; i++) {
        [tmpPoints insertObject:[points objectAtIndex:i] atIndex:0];
        [tmpPoints addObject:[points objectAtIndex:[points count]-1-i]];
    }
    
    for (int i=toAdd; i<[tmpPoints count]-toAdd; i++) {
        NSMutableArray *valuesY = [NSMutableArray array];
        for (int j=i-toAdd; j<=i+toAdd; j++) {
            [valuesY addObject:@([(NSValue *)tmpPoints[j] CGPointValue].y)];
        }
        CGPoint p = CGPointMake([(NSValue *)tmpPoints[i] CGPointValue].x, [[self.basicStatistics median:valuesY] floatValue]);
        [slidingMeanPoints addObject:[NSValue valueWithCGPoint:p]];
    }
    
    return slidingMeanPoints;
}

-(NSMutableArray*) computeSplinePointsForPoints:(NSMutableArray*)points withGranularity:(int)granularity {
    
    NSMutableArray *tmpPoints = [NSMutableArray arrayWithArray:points];
    NSMutableArray *splinPoints = [NSMutableArray array];
    
    [tmpPoints insertObject:tmpPoints[0] atIndex:0];
    [tmpPoints addObject:[tmpPoints lastObject]];
    
    [splinPoints addObject:[tmpPoints firstObject]];
    
    for (NSUInteger index = 1; index < tmpPoints.count - 2; index++)
    {
        
        CGPoint p0 = [(NSValue *)tmpPoints[index - 1] CGPointValue];
        CGPoint p1 = [(NSValue *)tmpPoints[index] CGPointValue];
        CGPoint p2 = [(NSValue *)tmpPoints[index + 1] CGPointValue];
        CGPoint p3 = [(NSValue *)tmpPoints[index + 2] CGPointValue];
        
        // now add n points starting at p1 + dx/dy up until p2 using Catmull-Rom splines
        for (int i = 1; i < granularity; i++)
        {
            float t = (float) i * (1.0f / (float) granularity);
            float tt = t * t;
            float ttt = tt * t;
            
            CGPoint pi; // intermediate point
            pi.x = 0.5 * (2*p1.x+(p2.x-p0.x)*t + (2*p0.x-5*p1.x+4*p2.x-p3.x)*tt + (3*p1.x-p0.x-3*p2.x+p3.x)*ttt);
            pi.y = 0.5 * (2*p1.y+(p2.y-p0.y)*t + (2*p0.y-5*p1.y+4*p2.y-p3.y)*tt + (3*p1.y-p0.y-3*p2.y+p3.y)*ttt);
            [splinPoints addObject:[NSValue valueWithCGPoint:pi]];
        }
        
        // Now add p2
        [splinPoints addObject:[NSValue valueWithCGPoint:p2]];
    }
    
    // finish by adding the last point
    [splinPoints addObject:[tmpPoints lastObject]];
    
    return splinPoints;
}

@end
