//
//  ViewController.m
//  stathoApp
//
//  Created by Pierre Starkov on 27/06/14.
//  Copyright (c) 2014 Pierre Starkov. All rights reserved.
//

#import "MainController.h"
#import "AudioContainer.h"
#import "AudioController.h"

@interface MainController ()

@end

@implementation MainController
@synthesize audioContainer;
@synthesize audioController;
@synthesize filledGraphView;

- (void)viewDidLoad
{
    [self view];
    [super viewDidLoad];
    
    audioContainer = [[AudioContainer alloc] init];
    audioController = [[AudioController alloc] init];
    [audioController setAudioContainer:audioContainer];
    [filledGraphView setAudioContainer:audioContainer];
    
    [audioController readAudioFileWithName:@"heartSound" withExtension:@"wav"];
    
    [self playPause:0];
}

- (IBAction)playPause:(id)sender {
    if(playYESpauseNO){
        [audioController pause];
        [levelTimer invalidate];
        playYESpauseNO = NO;
    } else {
        [audioController play];
        levelTimer = [NSTimer scheduledTimerWithTimeInterval: 0.02 target: self selector: @selector(updateDisplay:) userInfo: nil repeats: YES];
        playYESpauseNO = YES;
    }
}


-(void) updateDisplay:(NSTimer *)timer  {
    
    [filledGraphView setNeedsDisplay];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
