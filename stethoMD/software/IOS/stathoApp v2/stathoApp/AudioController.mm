//
//  AudioController.m
//  stathoApp
//
//  Created by Pierre Starkov on 12/07/14.
//  Copyright (c) 2014 Pierre Starkov. All rights reserved.
//

#import "AudioController.h"
#import "AudioContainer.h"

@implementation AudioController

@synthesize audioContainer;

-(id) init {
    self = [super init];
    ringBuffer = new RingBuffer(32768, 2);
    audioManager = [Novocaine audioManager];
    
    return self;
}

-(void) readAudioFileWithName:(NSString*)name withExtension:(NSString*)extension {
    
    // get file from name and format
    NSURL *inputFileURL = [[NSBundle mainBundle] URLForResource:name withExtension:extension];
    fileReader = [[AudioFileReader alloc]
                       initWithAudioFileURL:inputFileURL
                       samplingRate:audioManager.samplingRate
                       numChannels:audioManager.numOutputChannels];
    
    // set reading function
    audioManager.outputBlock = ^(float *data, UInt32 numFrames, UInt32 numChannels)
     {
         [fileReader retrieveFreshAudio:data numFrames:numFrames numChannels:numChannels];
         if (fileReader.currentTime >= fileReader.duration-1)
             [fileReader setCurrentTime:0.0f];
         
         for (int i=0; i<numFrames*numChannels; i+=2) {
             [audioContainer addSingleFloat:data[i]];
         }
     };
    
    // set reader and manager to play
    [self play];
}

-(void) play {
    [fileReader play];
    [audioManager play];
}

-(void) pause {
    [fileReader pause];
    [audioManager pause];
}

@end
