
#import "FilledGraphView.h"
#import "AudioContainer.h"
#import "BasicStatistics.h"

@implementation FilledGraphView
@synthesize audioContainer;
@synthesize x0;
@synthesize x1;
@synthesize y0;
@synthesize y1;
@synthesize currentIndex;
@synthesize fillColor;
@synthesize strokeColor;
@synthesize basicStatistics;

/**
 * initialisition method of the filled graph viewer
 */
-(void) awakeFromNib {
    [super awakeFromNib];
    
    // init view cosmetic options
    self.backgroundColor = [UIColor whiteColor];
    [self setStrokeColor:[UIColor blueColor]];
    [self setFillColor:[UIColor redColor]];
    
    // init display options
    [self setX0:0];
    [self setX1:100000];
    [self setY0:-0.5];
    [self setY1:0.5];
    
    // init statistics
    basicStatistics = [[BasicStatistics alloc] init];
}

/**
 * drawing function of the filled graph viewer
 * Audio -> is an array of X points. It can not be used to draw lines.
 * Points -> in an array of X and Y points. It can be used to draw lines.
 */
- (void)drawRect:(CGRect)rect {
    
    // in case there are points to display
    if ([[audioContainer audio] count] > 0){
        
        // get audio to display
        NSMutableArray *totalAudio = [self getTotalAudio];
        NSMutableArray *displayedAudio = [self getDisplayedAudio:totalAudio];
        NSMutableArray *ringLikeAudio = [self rearrangeToRingLikeAudio:displayedAudio withTotalAudio:totalAudio];
        
        // get points to display
//        NSMutableArray *points = [self getPoints:ringLikeAudio];
        NSMutableArray *pPoints = [self getPositivePoints:ringLikeAudio];
        NSMutableArray *nPoints = [self getNegativePoints:ringLikeAudio];
        
        // compute filtered points
        NSMutableArray *pSlidingMeanPoints = [self computeSlidingMeanPointsForPoints:pPoints withKernelSize:5];
        NSMutableArray *nSlidingMeanPoints = [self computeSlidingMedianPointsForPoints:nPoints withKernelSize:5];
        
        // finalize points befor drawing
        NSMutableArray *pfinalizedSlidingMeanPoints = [self finalizePoints:pSlidingMeanPoints];
        NSMutableArray *nfinalizedSlidingMeanPoints = [self finalizePoints:nSlidingMeanPoints];
        
        // draw points
        [self drawSurface:pfinalizedSlidingMeanPoints withLineWidth:0.5];
        [self drawSurface:nfinalizedSlidingMeanPoints withLineWidth:0.5];
        
        // get current position
        NSMutableArray *currentPoints = [self getCurrentPointFromCurentPosition:[self getCurrentRingLikePositionFromTotalAudio:totalAudio]];
        
        //draw currentPosition
        [self drawSurface:currentPoints withLineWidth:10.0];
    }
}

/**
 * Computes the totalAudio array from the the audioContainer.
 * totalAudio contains all the audio data connected to X and Y zooming.
 * \returns totalArray
 */
-(NSMutableArray*) getTotalAudio {
    
    NSMutableArray *totalAudio = [NSMutableArray array];
    float dx = round((x1 - x0) / self.frame.size.width);
    long k0 = x0;
    long kI = k0;
    int i = 0;
    
    while( kI<[[audioContainer audio] count] ) {
        [totalAudio addObject:[[audioContainer audio] objectAtIndex:kI]];
        i++;
        kI = round(k0 + i*dx);
    }
    
    return totalAudio;
}

/**
 * Computes the displayedAudio array based on the totalArray.
 * displayedAudio contains the audio data to display
 * \param NSMutableArray containing the total audio data
 * \return NSMutableArray containing the displayed audio data
 */
-(NSMutableArray*) getDisplayedAudio:(NSMutableArray*)totalAudio {
    
    NSMutableArray *displayedAudio = [NSMutableArray array];
    
    if ([totalAudio count] < self.frame.size.width) {
        [displayedAudio addObjectsFromArray:totalAudio];
    } else {
        int startDisplay = [totalAudio count] - self.frame.size.width;
        [displayedAudio addObjectsFromArray:[totalAudio subarrayWithRange:NSMakeRange(startDisplay, self.frame.size.width)]];
    }
    
    return displayedAudio;
}

-(int) getCurrentRingLikePositionFromTotalAudio:(NSMutableArray*)totalAudio {
    return [totalAudio count] % (int)(self.frame.size.width);
}

/**
 * Rearrange displayedArray to a ring like view. Ring like view is based a EKG like view.
 * \param NSMutableArray containing displayed audio data
 * \param NSMutableArray containing the total audio data
 * \return NSMutableArray containing the rearranged ring like displayed audio data
 */
-(NSMutableArray*) rearrangeToRingLikeAudio:(NSMutableArray*)displayedAudio withTotalAudio:(NSMutableArray*)totalAudio {

    int startDisplay = [self getCurrentRingLikePositionFromTotalAudio:totalAudio];
    NSMutableArray *ringLikeAudio = [NSMutableArray array];
                                                 
    // fill tmp array
    for (int i=0; i<[displayedAudio count]; i++) {
        [ringLikeAudio addObject:@(0)];
    }
    
    for (int i=0; i<[displayedAudio count]; i++) {
        int index = (startDisplay + i) % [displayedAudio count];
        [ringLikeAudio replaceObjectAtIndex:index withObject:[displayedAudio objectAtIndex:i]];
    }
    
    return ringLikeAudio;
}

-(NSMutableArray*) getCurrentPointFromCurentPosition:(int)currentPosition {
    NSMutableArray *currentPoints = [NSMutableArray array];
    
    CGPoint p1 = CGPointMake(currentPosition, self.frame.size.height-100);
    CGPoint p2 = CGPointMake(currentPosition, 100);

    [currentPoints addObject:[NSValue valueWithCGPoint:p1]];
    [currentPoints addObject:[NSValue valueWithCGPoint:p2]];
    
    return currentPoints;
}

/**
 * Creates points to display from the displayed audio data
 * \param NSMutableArray containing displayed audio data
 * \return NSMutableArray containing points to display
 */
-(NSMutableArray*) getPoints:(NSMutableArray*)displayedAudio {
    
    NSMutableArray *points = [NSMutableArray array];
    
    long dy = y1 - y0;
    
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(0, self.frame.size.height/2)]];
    
    for (int i=0; i<[displayedAudio count]; i++) {
        float point1x = i; // start graph x on the right hand side
        float point1y = self.frame.size.height/2 * (dy - [displayedAudio[i] floatValue])/dy;
        CGPoint p = CGPointMake(point1x, point1y);
        [points addObject:[NSValue valueWithCGPoint:p]];
    }
    
    return points;
}

/**
 * Creates positive points to display from the displayed audio data
 * \param NSMutableArray containing displayed audio data
 * \return NSMutableArray containing positive points to display
 */
-(NSMutableArray*) getPositivePoints:(NSMutableArray*)displayedAudio {
    
    NSMutableArray *positivePoints = [NSMutableArray array];
    
    long dy = y1 - y0;
    
    for (int i=0; i<[displayedAudio count]; i++) {
        float pointX = i;
        float pointY;
        if ([[displayedAudio objectAtIndex:i] floatValue] >= 0) {
            pointY = self.frame.size.height/2 * (dy - [displayedAudio[i] floatValue])/dy;
        } else {
            pointY = self.frame.size.height/2;
        }
        CGPoint p = CGPointMake(pointX, pointY);
        [positivePoints addObject:[NSValue valueWithCGPoint:p]];
    }
    
    return positivePoints;
}

/**
 * Creates negative points to display from the displayed audio data
 * \param NSMutableArray containing displayed audio data
 * \return NSMutableArray containing negative points to display
 */
-(NSMutableArray*) getNegativePoints:(NSMutableArray*)displayedAudio {
   
    NSMutableArray *negativePoints = [NSMutableArray array];

    long dy = y1 - y0;
    
    for (int i=0; i<[displayedAudio count]; i++) {
        float pointX = i;
        float pointY;
        if ([[displayedAudio objectAtIndex:i] floatValue] <= 0) {
            pointY = self.frame.size.height/2 * (dy - [displayedAudio[i] floatValue])/dy;
        } else {
            pointY = self.frame.size.height/2;
        }
        CGPoint p = CGPointMake(pointX, pointY);
        [negativePoints addObject:[NSValue valueWithCGPoint:p]];
    }
    
    return negativePoints;
}

-(NSMutableArray*) computeSlidingMeanPointsForPoints:(NSMutableArray*)points withKernelSize:(int)kernelSize {
    
    if (kernelSize > [points count]) {
        kernelSize = (int)[points count];
    }
    
    NSMutableArray *tmpPoints = [NSMutableArray arrayWithArray:points];
    NSMutableArray *slidingMeanPoints = [NSMutableArray array];
    int toAdd = floor(kernelSize/2);
    
    for(int i=1; i<toAdd; i++) {
        [tmpPoints insertObject:[points objectAtIndex:i] atIndex:0];
        [tmpPoints addObject:[points objectAtIndex:[points count]-1-i]];
    }
    
    for (int i=toAdd; i<[tmpPoints count]-toAdd; i++) {
        NSMutableArray *valuesY = [NSMutableArray array];
        for (int j=i-toAdd; j<=i+toAdd; j++) {
            [valuesY addObject:@([(NSValue *)tmpPoints[j] CGPointValue].y)];
        }
        CGPoint p = CGPointMake([(NSValue *)tmpPoints[i] CGPointValue].x, [[basicStatistics mean:valuesY] floatValue]);
        [slidingMeanPoints addObject:[NSValue valueWithCGPoint:p]];
    }
    
    return slidingMeanPoints;
}

-(NSMutableArray*) computeSlidingMedianPointsForPoints:(NSMutableArray*)points withKernelSize:(int)kernelSize {
    
    if (kernelSize > [points count]) {
        kernelSize = (int)[points count];
    }
    
    NSMutableArray *tmpPoints = [NSMutableArray arrayWithArray:points];
    NSMutableArray *slidingMeanPoints = [NSMutableArray array];
    int toAdd = floor(kernelSize/2);
    
    for(int i=1; i<toAdd; i++) {
        [tmpPoints insertObject:[points objectAtIndex:i] atIndex:0];
        [tmpPoints addObject:[points objectAtIndex:[points count]-1-i]];
    }
    
    for (int i=toAdd; i<[tmpPoints count]-toAdd; i++) {
        NSMutableArray *valuesY = [NSMutableArray array];
        for (int j=i-toAdd; j<=i+toAdd; j++) {
            [valuesY addObject:@([(NSValue *)tmpPoints[j] CGPointValue].y)];
        }
        CGPoint p = CGPointMake([(NSValue *)tmpPoints[i] CGPointValue].x, [[basicStatistics median:valuesY] floatValue]);
        [slidingMeanPoints addObject:[NSValue valueWithCGPoint:p]];
    }
    
    return slidingMeanPoints;
}

-(NSMutableArray*) computeSplinePointsForPoints:(NSMutableArray*)points withGranularity:(int)granularity {
    
    NSMutableArray *tmpPoints = [NSMutableArray arrayWithArray:points];
    NSMutableArray *splinPoints = [NSMutableArray array];
    
    [tmpPoints insertObject:tmpPoints[0] atIndex:0];
    [tmpPoints addObject:[tmpPoints lastObject]];
    
    [splinPoints addObject:[tmpPoints firstObject]];
    
    for (NSUInteger index = 1; index < tmpPoints.count - 2; index++)
    {
        
        CGPoint p0 = [(NSValue *)tmpPoints[index - 1] CGPointValue];
        CGPoint p1 = [(NSValue *)tmpPoints[index] CGPointValue];
        CGPoint p2 = [(NSValue *)tmpPoints[index + 1] CGPointValue];
        CGPoint p3 = [(NSValue *)tmpPoints[index + 2] CGPointValue];
        
        // now add n points starting at p1 + dx/dy up until p2 using Catmull-Rom splines
        for (int i = 1; i < granularity; i++)
        {
            float t = (float) i * (1.0f / (float) granularity);
            float tt = t * t;
            float ttt = tt * t;
            
            CGPoint pi; // intermediate point
            pi.x = 0.5 * (2*p1.x+(p2.x-p0.x)*t + (2*p0.x-5*p1.x+4*p2.x-p3.x)*tt + (3*p1.x-p0.x-3*p2.x+p3.x)*ttt);
            pi.y = 0.5 * (2*p1.y+(p2.y-p0.y)*t + (2*p0.y-5*p1.y+4*p2.y-p3.y)*tt + (3*p1.y-p0.y-3*p2.y+p3.y)*ttt);
            [splinPoints addObject:[NSValue valueWithCGPoint:pi]];
        }
        
        // Now add p2
        [splinPoints addObject:[NSValue valueWithCGPoint:p2]];
    }
    
    // finish by adding the last point
    [splinPoints addObject:[tmpPoints lastObject]];
    
    return splinPoints;
}

-(NSMutableArray*) finalizePoints:(NSMutableArray*)points {
    
    [points insertObject:[NSValue valueWithCGPoint:CGPointMake([(NSValue *)[points firstObject] CGPointValue].x, self.frame.size.height/2)] atIndex:0];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake([(NSValue *)[points lastObject] CGPointValue].x, self.frame.size.height/2)]];
    
    return points;
}

-(void) drawLines:(NSMutableArray*)points withLineWidth:(float)lineWidth {
    if ([points count] > 0) {

        UIBezierPath *lineGraph = [UIBezierPath bezierPath];
        [lineGraph moveToPoint:[points[0] CGPointValue]];
        
        for (NSUInteger index = 1; index < points.count; index++)
        {
            CGPoint p = [(NSValue *)points[index] CGPointValue];
            [lineGraph addLineToPoint:p];
        }
        
        [fillColor setFill];
        [strokeColor setStroke];
        
        lineGraph.lineCapStyle = kCGLineCapRound;
        lineGraph.lineJoinStyle = kCGLineJoinRound;
        lineGraph.flatness = 0.5;
        lineGraph.lineWidth = lineWidth; // line width
        [lineGraph stroke];
    }
}

-(void) drawSurface:(NSMutableArray*)points withLineWidth:(float)lineWidth {
    if ([points count] > 0) {
                
        UIBezierPath *lineGraph = [UIBezierPath bezierPath];
        [lineGraph moveToPoint:[points[0] CGPointValue]];
        
        for (NSUInteger index = 1; index < points.count; index++)
        {
            CGPoint p = [(NSValue *)points[index] CGPointValue];
            [lineGraph addLineToPoint:p];
        }

        [fillColor setFill];
        [strokeColor setStroke];
        [lineGraph closePath];
        [lineGraph fill]; // fill color (if closed)
        
        lineGraph.lineCapStyle = kCGLineCapRound;
        lineGraph.lineJoinStyle = kCGLineJoinRound;
        lineGraph.flatness = 0.5;
        lineGraph.lineWidth = lineWidth; // line width
        [lineGraph stroke];
    }
}

@end
