//
//  AudioController.h
//  stathoApp
//
//  Created by Pierre Starkov on 12/07/14.
//  Copyright (c) 2014 Pierre Starkov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Novocaine.h"
#import "RingBuffer.h"
#import "AudioFileReader.h"
#import "AudioFileWriter.h"

@class MainController;
@class AudioContainer;

@interface AudioController : NSObject {
    Novocaine *audioManager;
    AudioFileReader *fileReader;
    AudioFileWriter *fileWriter;
    RingBuffer *ringBuffer;
    AudioContainer *audioContainer;
}

@property (assign) AudioContainer *audioContainer;

-(void) readAudioFileWithName:(NSString*)name withExtension:(NSString*)extension;
-(void) play;
-(void) pause;

@end
