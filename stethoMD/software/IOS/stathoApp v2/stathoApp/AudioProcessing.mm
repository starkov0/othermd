//
//  AudioProcessing.m
//  stathoApp
//
//  Created by Pierre Starkov on 04/08/14.
//  Copyright (c) 2014 Pierre Starkov. All rights reserved.
//

#import "AudioProcessing.h"

@implementation AudioProcessing

/**
 * Computes the totalAudio array from the the audioContainer.
 * totalAudio contains all the audio data connected to X and Y zooming.
 * \returns totalArray
 */
-(NSMutableArray*) getDrawableAudioFromTotalAudio:(NSMutableArray*)totalAudio x0:(int)x0 x1:(int)x1 frameWidth:(float)frameWidth {
    
    NSMutableArray *drawableAudio = [NSMutableArray array];
    float dx = round((x1 - x0) / frameWidth);
    long k0 = x0;
    long kI = k0;
    int i = 0;
    
    while( kI<[totalAudio count] ) {
        [drawableAudio addObject:[totalAudio objectAtIndex:kI]];
        i++;
        kI = round(k0 + i*dx);
    }
    
    return drawableAudio;
}

/**
 * Computes the displayedAudio array based on the totalArray.
 * displayedAudio contains the audio data to display
 * \param NSMutableArray containing the total audio data
 * \return NSMutableArray containing the displayed audio data
 */
-(NSMutableArray*) getAudioFromDisplayableAudio:(NSMutableArray*)drawableAudio withFrameWidth:(float)frameWidth {
    
    NSMutableArray *audio = [NSMutableArray array];
    
    if ([drawableAudio count] < frameWidth) {
        [audio addObjectsFromArray:drawableAudio];
    } else {
        int startDisplay = [drawableAudio count] - frameWidth;
        [audio addObjectsFromArray:[drawableAudio subarrayWithRange:NSMakeRange(startDisplay, frameWidth)]];
    }
    
    return audio;
}

-(int) getCurrentRingLikePositionFromDrawableAudio:(NSMutableArray*)drawableAudio frameWidth:(float)frameWidth {
    return [drawableAudio count] % (int)(frameWidth);
}

/**
 * Rearrange displayedArray to a ring like view. Ring like view is based a EKG like view.
 * \param NSMutableArray containing displayed audio data
 * \param NSMutableArray containing the total audio data
 * \return NSMutableArray containing the rearranged ring like displayed audio data
 */
-(NSMutableArray*) getRingLikeAudioFromDrawable:(NSMutableArray*)drawableAudio TotalAudio:(NSMutableArray*)totalAudio frameWidth:(float)frameWidth {
    
    int startDisplay = [self getCurrentRingLikePositionFromDrawableAudio:drawableAudio frameWidth:frameWidth];
    NSMutableArray *ringLikeAudio = [NSMutableArray array];
    
    // fill tmp array
    for (int i=0; i<[drawableAudio count]; i++) {
        [ringLikeAudio addObject:@(0)];
    }
    
    for (int i=0; i<[drawableAudio count]; i++) {
        int index = (startDisplay + i) % [drawableAudio count];
        [ringLikeAudio replaceObjectAtIndex:index withObject:[drawableAudio objectAtIndex:i]];
    }
    
    return ringLikeAudio;
}

@end
