//
//  PointsProcessing.m
//  stathoApp
//
//  Created by Pierre Starkov on 04/08/14.
//  Copyright (c) 2014 Pierre Starkov. All rights reserved.
//

#import "PointsProcessing.h"

@implementation PointsProcessing

-(NSMutableArray*) getCurrentPointFromCurentPosition:(int)currentPosition frameWidth:(float)frameWidth {
    NSMutableArray *currentPoints = [NSMutableArray array];
    
    CGPoint p1 = CGPointMake(currentPosition, frameWidth-100);
    CGPoint p2 = CGPointMake(currentPosition, 100);
    
    [currentPoints addObject:[NSValue valueWithCGPoint:p1]];
    [currentPoints addObject:[NSValue valueWithCGPoint:p2]];
    
    return currentPoints;
}

/**
 * Creates points to display from the displayed audio data
 * \param NSMutableArray containing displayed audio data
 * \return NSMutableArray containing points to display
 */
-(NSMutableArray*) getPointsFromDrawableAudio:(NSMutableArray*)drawableAudio y1:(float)y1 y0:(float)y0 frameWidth:(float)frameWidth {
    
    NSMutableArray *points = [NSMutableArray array];
    
    long dy = y1 - y0;
    
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(0, frameWidth/2)]];
    
    for (int i=0; i<[drawableAudio count]; i++) {
        float point1x = i; // start graph x on the right hand side
        float point1y = frameWidth/2 * (dy - [drawableAudio[i] floatValue])/dy;
        CGPoint p = CGPointMake(point1x, point1y);
        [points addObject:[NSValue valueWithCGPoint:p]];
    }
    
    return points;
}

/**
 * Creates positive points to display from the displayed audio data
 * \param NSMutableArray containing displayed audio data
 * \return NSMutableArray containing positive points to display
 */
-(NSMutableArray*) getPositivePointsFromDrawableAudio:(NSMutableArray*)drawableAudio y1:(float)y1 y0:(float)y0 frameWidth:(float)frameWidth {
    
    NSMutableArray *positivePoints = [NSMutableArray array];
    
    long dy = y1 - y0;
    
    for (int i=0; i<[drawableAudio count]; i++) {
        float pointX = i;
        float pointY;
        if ([[drawableAudio objectAtIndex:i] floatValue] >= 0) {
            pointY = frameWidth/2 * (dy - [drawableAudio[i] floatValue])/dy;
        } else {
            pointY = frameWidth/2;
        }
        CGPoint p = CGPointMake(pointX, pointY);
        [positivePoints addObject:[NSValue valueWithCGPoint:p]];
    }
    
    return positivePoints;
}

/**
 * Creates negative points to display from the displayed audio data
 * \param NSMutableArray containing displayed audio data
 * \return NSMutableArray containing negative points to display
 */
-(NSMutableArray*) getNegativePointsFromDrawableAudio:(NSMutableArray*)drawableAudio y1:(float)y1 y0:(float)y0 frameWidth:(float)frameWidth {
    
    NSMutableArray *negativePoints = [NSMutableArray array];
    
    long dy = y1 - y0;
    
    for (int i=0; i<[drawableAudio count]; i++) {
        float pointX = i;
        float pointY;
        if ([[drawableAudio objectAtIndex:i] floatValue] <= 0) {
            pointY = frameWidth/2 * (dy - [drawableAudio[i] floatValue])/dy;
        } else {
            pointY = frameWidth/2;
        }
        CGPoint p = CGPointMake(pointX, pointY);
        [negativePoints addObject:[NSValue valueWithCGPoint:p]];
    }
    
    return negativePoints;
}

-(NSMutableArray*) finalizePoints:(NSMutableArray*)points withFrameHeight:(float)frameHeight {
    
    [points insertObject:[NSValue valueWithCGPoint:CGPointMake([(NSValue *)[points firstObject] CGPointValue].x, frameHeight/2)] atIndex:0];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake([(NSValue *)[points lastObject] CGPointValue].x, frameHeight/2)]];
    
    return points;
}

@end
