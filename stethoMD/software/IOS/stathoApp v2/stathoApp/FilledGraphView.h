//
//  GraphView.h
//  DynamicGraphView

#import <UIKit/UIKit.h>

@class AudioContainer;
@class BasicStatistics;

@interface FilledGraphView : UIView {

    long x0;
    long x1;
    float y0;
    float y1;
    long currentIndex;
    
    UIColor *strokeColor;
    UIColor *fillColor;
    
    AudioContainer *audioContainer;
    BasicStatistics *basicStatistics;

}

@property long x0;
@property long x1;
@property float y0;
@property float y1;
@property long currentIndex;

@property (retain) UIColor *strokeColor;
@property (retain) UIColor *fillColor;

@property (retain) AudioContainer *audioContainer;
@property (retain) BasicStatistics *basicStatistics;


@end
