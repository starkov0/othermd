//
//  ViewController.h
//  stathoApp
//
//  Created by Pierre Starkov on 27/06/14.
//  Copyright (c) 2014 Pierre Starkov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FilledGraphView.h"
#import <math.h>

@class AudioContainer;
@class AudioController;

@interface MainController : UIViewController {
    AudioContainer *audioContainer;
    AudioController *audioController;
    NSTimer *levelTimer;
    BOOL playYESpauseNO;
}
@property (retain) AudioContainer *audioContainer;
@property (retain) AudioController *audioController;
@property (assign, nonatomic) IBOutlet FilledGraphView *filledGraphView;

- (IBAction)playPause:(id)sender;

@end
