//
//  DataController.h
//  stathoApp
//
//  Created by Pierre Starkov on 03/07/14.
//  Copyright (c) 2014 Pierre Starkov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AudioContainer : NSObject {
    NSMutableArray *audio;
}

@property (retain) NSMutableArray *audio;

-(void)addSingleFloat:(float)singleAudioData;
-(void)addSingleNSNumber:(NSNumber*)singleAudioData;
-(void)addFloatArray:(float*)array withLength:(long)length;
-(void)addNSMutableArray:(NSMutableArray*)array;

@end
