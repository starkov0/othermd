// zoom X --> dxBegin AND dxEnd
// zoom Y --> dy

#import "EmptyGraphView.h"

@implementation EmptyGraphView
@synthesize pointArray;
@synthesize positiveDisplayArray;
@synthesize negativeDisplayArray;
@synthesize dxBegin;
@synthesize dxEnd;
@synthesize dy;
@synthesize currentPosition;
@synthesize lineWidth;
@synthesize fillGraph;
@synthesize strokeColor;
@synthesize fillColor;
@synthesize zeroLineStrokeColor;
@synthesize granularity;


- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupGraph];
    }
    
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setupGraph];
    }
    
    return self;
}

- (void)setupGraph
{
    // init display option
    self.backgroundColor = [UIColor whiteColor];
    granularity = 1;
    strokeColor = [UIColor redColor];
    fillColor = [UIColor redColor];
    zeroLineStrokeColor = [UIColor greenColor];
    lineWidth = 1;
    
    // init display mode
    dxBegin = 0; // number of points shown in graph
    dxEnd = 5000;
    dy = 2; // default value for dy
    positiveDisplayArray = [[NSMutableArray alloc] init];
    negativeDisplayArray = [[NSMutableArray alloc] init];
}

// here the graph is actually being drawn
- (void)drawRect:(CGRect)rect {
    [self displayZeroLine];
    
    // there are any points
    if ([pointArray count] > 0){
        //clean array
        [positiveDisplayArray removeAllObjects];
        [negativeDisplayArray removeAllObjects];
        // display arrays
        [self splitArrayOfPoints];
        if ([positiveDisplayArray count] > 0) {
            strokeColor = [UIColor redColor];
            fillColor = [UIColor redColor];
            [self displayCurvedLines:positiveDisplayArray];
        }
        if ([negativeDisplayArray count] > 0) {
            strokeColor = [UIColor blueColor];
            fillColor = [UIColor blueColor];
            [self displayCurvedLines:negativeDisplayArray];
        }
    }
}

-(void) displayZeroLine {
    // set zero line
    [zeroLineStrokeColor setStroke];
    UIBezierPath *zeroLine = [UIBezierPath bezierPath];
    [zeroLine moveToPoint:CGPointMake(0, self.frame.size.height/2)];
    [zeroLine addLineToPoint:CGPointMake(self.frame.size.width, self.frame.size.height/2)];
    zeroLine.lineWidth = lineWidth; // line width
    [zeroLine stroke];
}

-(void) displayCurvedLines:(NSMutableArray*)points {
    if ([pointArray count] > 0) {
        
        // Add control points to make the math make sense
        [points insertObject:points[0] atIndex:0];
        [points addObject:[points lastObject]];
        
        UIBezierPath *lineGraph = [UIBezierPath bezierPath];
        
        [lineGraph moveToPoint:[points[0] CGPointValue]];
        
        for (NSUInteger index = 1; index < points.count - 2; index++)
        {
            
            CGPoint p0 = [(NSValue *)points[index - 1] CGPointValue];
            CGPoint p1 = [(NSValue *)points[index] CGPointValue];
            CGPoint p2 = [(NSValue *)points[index + 1] CGPointValue];
            CGPoint p3 = [(NSValue *)points[index + 2] CGPointValue];
            
            // now add n points starting at p1 + dx/dy up until p2 using Catmull-Rom splines
            for (int i = 1; i < granularity; i++)
            {
                float t = (float) i * (1.0f / (float) granularity);
                float tt = t * t;
                float ttt = tt * t;
                
                CGPoint pi; // intermediate point
                pi.x = 0.5 * (2*p1.x+(p2.x-p0.x)*t + (2*p0.x-5*p1.x+4*p2.x-p3.x)*tt + (3*p1.x-p0.x-3*p2.x+p3.x)*ttt);
                pi.y = 0.5 * (2*p1.y+(p2.y-p0.y)*t + (2*p0.y-5*p1.y+4*p2.y-p3.y)*tt + (3*p1.y-p0.y-3*p2.y+p3.y)*ttt);
                [lineGraph addLineToPoint:pi];
            }
            
            // Now add p2
            [lineGraph addLineToPoint:p2];
        }
        
        // finish by adding the last point
        [lineGraph addLineToPoint:[(NSValue *)points[(points.count - 1)] CGPointValue]];
        
        [fillColor setFill];
        [strokeColor setStroke];
        
        if (fillGraph) {
            [lineGraph addLineToPoint:CGPointMake([(NSValue *)[points lastObject] CGPointValue].x, self.frame.size.height/2)];
            [lineGraph addLineToPoint:CGPointMake(self.frame.size.width, self.frame.size.height/2)];
            [lineGraph closePath];
            [lineGraph fill]; // fill color (if closed)
        }
        
        lineGraph.lineCapStyle = kCGLineCapRound;
        lineGraph.lineJoinStyle = kCGLineJoinRound;
        lineGraph.flatness = 0.5;
        lineGraph.lineWidth = lineWidth; // line width
        [lineGraph stroke];
    }
}

- (void)splitArrayOfPoints {
    
    // get frame
    int viewWidth = CGRectGetWidth(self.frame);
    int viewHeight = CGRectGetHeight(self.frame);
    
    // get arrayToDisplay
    NSArray *displayArray = [self getDisplayArray];
    
    int tot = dxEnd / 640;
    
    for (int i = tot; i < [displayArray count]; i+=tot) {
        // create point
        float point1x = viewWidth - (viewWidth / (dxEnd-dxBegin)) * i; // start graph x on the right hand side
        float point1y = viewHeight - ((viewHeight / dy) * [displayArray[i] floatValue] + viewHeight/2);
        CGPoint p = CGPointMake(point1x, point1y);
        
        // set point in array
        if ([[displayArray objectAtIndex:i] floatValue] >= 0) {
            [positiveDisplayArray addObject:[NSValue valueWithCGPoint:p]];
        }
        else {
            [negativeDisplayArray addObject:[NSValue valueWithCGPoint:p]];
        }
    }
}

// get current array to display
-(NSArray*) getDisplayArray {
    int displayStart = MIN(0, dxBegin);
    int displayEnd = MIN([pointArray count], (dxEnd-dxBegin));
    NSArray *displayArray = [pointArray subarrayWithRange:NSMakeRange(displayStart, displayEnd-displayStart)];
    return displayArray;
}

@end
