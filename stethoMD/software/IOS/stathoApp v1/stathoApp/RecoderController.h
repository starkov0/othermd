//
//  MicController.h
//  stathoApp
//
//  Created by Pierre Starkov on 04/07/14.
//  Copyright (c) 2014 Pierre Starkov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioToolbox.h>
#import <CoreAudio/CoreAudioTypes.h>

@class ViewController;

@interface RecoderController : NSObject {
	AVAudioRecorder *recorder;
	NSTimer *levelTimer;
	double lowPassResults;
    ViewController *viewController;
}

@property (retain) ViewController *viewController;

- (id)initWithController:(ViewController*)aViewController;

@end
