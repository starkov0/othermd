//
//  PlayerController.h
//  stathoApp
//
//  Created by Pierre Starkov on 04/07/14.
//  Copyright (c) 2014 Pierre Starkov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#import <CoreAudio/CoreAudioTypes.h>

@interface PlayerController : NSObject {
    AVAudioPlayer *player;
}

@end
