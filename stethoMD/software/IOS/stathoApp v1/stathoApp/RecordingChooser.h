//
//  RecordingChooser.h
//  stathoApp
//
//  Created by Pierre Starkov on 11/07/14.
//  Copyright (c) 2014 Pierre Starkov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RecordingChooser : NSObject

- (void) demonstrateInputSelection;

@end
