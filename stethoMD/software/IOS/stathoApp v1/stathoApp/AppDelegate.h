//
//  AppDelegate.h
//  stathoApp
//
//  Created by Pierre Starkov on 27/06/14.
//  Copyright (c) 2014 Pierre Starkov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
