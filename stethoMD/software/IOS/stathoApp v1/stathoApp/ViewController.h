//
//  ViewController.h
//  stathoApp
//
//  Created by Pierre Starkov on 27/06/14.
//  Copyright (c) 2014 Pierre Starkov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GraphView.h"
#import <math.h>

@class DataController;
@class RecoderController;

@interface ViewController : UIViewController {
    DataController *dataController;
    RecoderController *recorderController;
}
@property (retain) DataController *dataController;
@property (retain) RecoderController *recorderController;
@property (weak, nonatomic) IBOutlet GraphView *originalView;
@property (weak, nonatomic) IBOutlet GraphView *surfaceView;

- (IBAction)addPointNow:(id)sender;

-(void) addPoint:(float)point;

@end
