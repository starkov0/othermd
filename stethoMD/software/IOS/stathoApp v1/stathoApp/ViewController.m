//
//  ViewController.m
//  stathoApp
//
//  Created by Pierre Starkov on 27/06/14.
//  Copyright (c) 2014 Pierre Starkov. All rights reserved.
//

#import "ViewController.h"
#import "DataController.h"
#import "RecoderController.h"
#import "RecordingChooser.h"

@interface ViewController ()

@end

@implementation ViewController
@synthesize dataController;
@synthesize recorderController;
@synthesize originalView;
@synthesize surfaceView;

- (void)viewDidLoad
{
    [self view];
    [super viewDidLoad];
    
    dataController = [[DataController alloc] init];
    //recorderController = [[RecoderController alloc] initWithController:self];
    RecordingChooser *r = [[RecordingChooser alloc] init];
    [r demonstrateInputSelection];
    [originalView setFillGraph:NO];
    [surfaceView setFillGraph:YES];
    for (int i=0; i<100; i++){
        float point = ((float)rand() / RAND_MAX);
//        NSLog(@"point : %f",point);
        [self addPoint:point];
    }
}

-(void) addPoint:(float)point
{
    [dataController addPoint:point];
    [originalView setPointArray:[dataController getPointArray]];
    [surfaceView setPointArray:[dataController getPointArray]];
    [originalView setNeedsDisplay];
    [surfaceView setNeedsDisplay];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)addPointNow:(id)sender {
    float point = ((float)rand() / RAND_MAX) - 0.5;
    NSLog(@"point : %f",point);
    [self addPoint:point];
}
@end
