//
//  DataController.m
//  stathoApp
//
//  Created by Pierre Starkov on 03/07/14.
//  Copyright (c) 2014 Pierre Starkov. All rights reserved.
//

#import "DataController.h"

@implementation DataController

-(id)init {
    self = [super init];
    pointArray = [[NSMutableArray alloc] init];
    return self;
}

// reset the whole graph
-(void)resetGraph {
//    @synchronized(self){
        [pointArray removeAllObjects];
//    }
}

// add point from the sound system
-(void)addPoint:(float)point {
//    @synchronized(self){
        [pointArray insertObject:@(point) atIndex:0];
//    }
}

// load old graph as array
-(void)setPointArray:(NSArray*)array {
//    @synchronized(self){
        [pointArray removeAllObjects];
        [pointArray addObjectsFromArray:array];
//    }
}

-(NSMutableArray*) getPointArray {
//    @synchronized(self){
        return pointArray;
//    }
}

@end
