//
//  DataController.h
//  stathoApp
//
//  Created by Pierre Starkov on 03/07/14.
//  Copyright (c) 2014 Pierre Starkov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DataController : NSObject {
    NSMutableArray *pointArray;
}

-(void)resetGraph;
-(void)addPoint:(float)point;
-(void)setPointArray:(NSArray*)array;
-(NSMutableArray*) getPointArray;


@end
