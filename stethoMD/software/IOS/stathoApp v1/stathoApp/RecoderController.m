//
//  MicController.m
//  stathoApp
//
//  Created by Pierre Starkov on 04/07/14.
//  Copyright (c) 2014 Pierre Starkov. All rights reserved.
//

#import "RecoderController.h"
#import "ViewController.h"

@implementation RecoderController
@synthesize viewController;

- (id)initWithController:(ViewController*)aViewController {
    
    // init
    self = [super init];
    [self setViewController:aViewController];
    [self initRecroder];
    return self;
}

-(void) initRecroder {
    // init mic parameters
	NSURL *url = [NSURL fileURLWithPath:@"/dev/null"];
    NSError *error;
	NSDictionary *settings = [NSDictionary dictionaryWithObjectsAndKeys:
							  [NSNumber numberWithFloat: 44100.0],AVSampleRateKey,
							  [NSNumber numberWithInt: kAudioFormatAppleLossless],AVFormatIDKey,
							  [NSNumber numberWithInt: 1],AVNumberOfChannelsKey,
							  [NSNumber numberWithInt: AVAudioQualityMax],AVEncoderAudioQualityKey,
							  nil];
    
    // init mic
	recorder = [[AVAudioRecorder alloc] initWithURL:url settings:settings error:&error];
    
    if (recorder) {
		[recorder prepareToRecord];
		recorder.meteringEnabled = YES;
		[recorder record];
		levelTimer = [NSTimer scheduledTimerWithTimeInterval: 0.02 target: self selector: @selector(levelTimerCallback:) userInfo: nil repeats: YES];
	} else
		NSLog([error description]);
}


- (void)levelTimerCallback:(NSTimer *)timer {
//	[recorder updateMeters];
//    
//    double linear = pow (10, [recorder peakPowerForChannel:0] / 20);
//    NSLog(@"linear: %f",linear);
    [[self viewController] addPoint:((float)rand() / RAND_MAX)];
}

@end
