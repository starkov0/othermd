\documentclass[a4paper,10pt,titlepage]{article}
\usepackage[margin=3cm]{geometry}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[frenchb]{babel}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{url}
\usepackage{color}
\usepackage{mathrsfs}
\usepackage{setspace}
\usepackage{float}
\usepackage{array}
\usepackage{chngpage}
\newcolumntype{L}[1]{>{\raggedright\let\newline\\\arraybackslash\hspace{0pt}}m{#1}}
\newcolumntype{C}[1]{>{\centering\let\newline\\\arraybackslash\hspace{0pt}}m{#1}}
\newcolumntype{R}[1]{>{\raggedleft\let\newline\\\arraybackslash\hspace{0pt}}m{#1}}
\usepackage{setspace}
\usepackage{capt-of}
\emergencystretch=1em
\renewcommand{\arraystretch}{1.5}
\renewcommand*\rmdefault{ppl}
\usepackage{fancyhdr}
\pagestyle{fancy}



\title{Auscultation Cardiaque}
\author{Pierre Starkov}


\begin{document}

\begin{titlepage}
	\begin{center}
		\begin{table}[h!]
		\centering
		\end{table}
		%\vspace{2cm}
		{\fontsize{20}{20} \textbf{\textsf{Poly M.D.}}} \\
		\vspace*{8cm}

		{\fontsize{100}{100} \textbf{\textsf{Auscultation}}} \\
		\vspace*{0.2cm}
		{\fontsize{100}{100} \textbf{\textsf{Cardiaque}}} \\
		\vspace{9cm}
		
		\begin{minipage}{.45\linewidth}
			\begin{flushleft}
				Auteur:\\
				Pierre \textsc{Starkov}
			\end{flushleft} 
		\end{minipage}
			\hfill
			\begin{minipage}{.45\linewidth}
			\begin{flushright}                                      
				Superviseur: \\
				Prof. Wilhelm \textsc{Rutishauser}
			\end{flushright} 
		\end{minipage}
    
		\vspace{2cm}
		Genève, \today
	\end{center}
\end{titlepage}

\clearpage
\tableofcontents
\newpage






\section{Introduction}

Ce document a pour but la création d'algorithmes d'aide à la décision médicale dans le domaine de l'auscultation cardiaque. 

Le texte a été rédigé par Pierre Starkov et corrigé par le professeur Wilhelm Rutishauser. Il est en majorité basé sur la partie \textit{Auscultation} du livre \textit{Cardiologie clinique écrit par Wilhelm RUTISHAUSER, Juan SZTAJZEL, éditeur ELSEVIER / MASSON, collection Cardiologie pratique, année 1990}.






\section{Types de bruits cardiaques}
Les bruits cardiaques auscultatoires sont regroupés en plusieurs catégories de par leur fréquence et l'intervalle de temps qu'ils occupent. Les clics sont de courts bruits de haute fréquence. Les soufles sont des vibrations de durée prolongée et de fréquence variable.






\section{Bruits physiologiques}

\begin{center}
	\begingroup
		\centering
		\includegraphics[scale=1.2]{img/Normal}
		\captionof{figure}{"B1 et B2 normaux."}
	\endgroup
\end{center}

\subsection{Premier bruit cardiaque}
Deux composantes : M1 fermeture de la valve mitrale et T1 fermeture de la valve tricuspide.

Dédoublement espacé si \textbf{bloc de branche droit}. Pas de dédoublement si \textbf{bloc de branche gauche}.

Forte intensité de B1 si \textbf{hyperthyroïdie, pendant l'effort musculaire (sport), sténose mitrale}. Faible intensité de B1 si \textbf{calcification de la valve mitrale avec mobilité diminuée}. Intensité variable (accentuation intermittente) si \textbf{bloc auriculo-ventriculaire complet}.

\begin{center}
	\begingroup
		\centering
		\includegraphics[scale=0.7]{img/B1}
		\captionof{figure}{"Exemples de B1 anormal."}
	\endgroup
\end{center}

\subsection{Deuxième bruit cardiaque}
Deux composantes : A2 fermeture de la valve aortique et P2 fermeture de la valve pulmonaire. Temps normal A2 - P2: 0.02 – 0.04 sec. En inspiration,  A2 - P2: 0.06 sec. En expiration, A2 - P2: moins de 0.02 sec. 

Intensité de A2 augmentée si \textbf{augmentation de la pression aortique diastolique}. Intensité de P2 augmentée si \textbf{hypertension pulmonaire, thorax en entonnoir}. Intensité de P2 diminuée si \textbf{sténose pulmonaire infundibulaire}. 

Dédoublement large et fixe sans effet de la respiration si \textbf{communication interauriculaire}. Dédoublement large aussi en cas de \textbf{bloc de branche droit, sténose pulmonaire infundibulaire ou valvulaire, embolie massive avec insuffisance ventriculaire droite}. A2 précoce, dû à un raccourcissement de la systole gauche, si \textbf{insuffisance mitrale, communication ventriculaire avec shunt gauche – droit}. Dédoublement diminué ou absent si hypertension pulmonaire sévère (Syndrome d’Eisenmenger). Temps conservé si embolie pulmonaire aigu. Dédoublement étroit ou rarement paradoxal du deuxième bruit (P2 – A2 et pas A2 – P2) si \textbf{bloc de branche gauche large, sténose aortique valvulaire sévère ou canal artériel avec shunt gauche-droit de très gros débit}.

\begin{center}
	\begingroup
		\centering
		\includegraphics[scale=0.7]{img/B2}
		\captionof{figure}{"Exemples de B2 anormal."}
	\endgroup
\end{center}






\section{Bruits systoliques surajoutés}

\subsection{Clic protosystolique}
Clic protosystolique aortique si \textbf{anévrisme de l’aorte ascendante, coarctation de l’aorte, hypertension artérielle avec dilatation de l’aorte ascendante, sténose aortique valvulaire ou insuffisance aortique}. 

Clic protosystolique pulmonaire si \textbf{sténose pulmonaire valvulaire, hypertension pulmonaire ou dilatation idiopathique de l’artère pulmonaire, hyperthyroïdie}.

\subsection{Clic mésotélésystolique}
Clic mésosystolique ou télésystolique si \textbf{ballonnement des valves mitrales dans l’oreillette souvent suivi d'insuffisance mitrale}.






\section{Bruits diastoliques surajoutés}

\subsection{Claquement d’ouverture mitrale (COM)}
Bruit de haute fréquence suivi par un roulement diastolique mitral. Intervalle A2 - COM : 0.04 – 0.12 sec.

Audible si \textbf{épaississement et rétrécissement des valves mitrales; les valves passent de convexe pendant la systole à concave pendant la diastole (changement de forme)}. 

Nonaudible si \textbf{valve totalement rigide}. 

Intervalle A2 - COM court si \textbf{sténose mitrale sévère}. Intervalle A2 - COM long si \textbf{sténose mitrale légère}. 

\subsection{Claquement d’ouverture tricuspidienne (COT)}
Ce bruit peut être plus tardif que COM et d’intensité plus faible que COM suivi d’un roulement diastolique tricuspidien si \textbf{rétrécissement tricuspidien sévère}. 

Souvent, COM et COT arrivent en même temps et le roulement diastolique mitrale cache COT.

\subsection{Troisième bruit physiologique (B3)}
Bruit de \textbf{remplissage ventriculaire} de basse fréquence et apparaissant 0.12 – 0.20 sec après A2. Normal chez les enfants et les jeunes adultes. L'intensité augmente en expiration.

\subsection{Galop protodiastolique (GPD)}
Bruit de basse fréquence et de temps d’apparition semblable à B3. Disparaît si valve artificielle (bruit créé par la valve).

Chez les sujets âgés il traduit une dysfonction ventriculaire : \textbf{insuffisance cardiaque, défaillance ventriculaire, régurgitation mitrale sévère}. Origine gauche si aucune modification avec la respiration; origine droite si l'intensité augmente avec l'inspiration et ne disparaît pas avec l'expiration.

\subsection{Galop présystolique (B4)}
Le bruit de \textbf{contraction auriculaire} n'est pas un signe d'insuffisence cardiaque.

B4 droit augmenté à l’inspiration si \textbf{surcharge ventriculaire droite}.

\subsection{Galop de sommation (GS)}
Lorsque la fréquence cardiaque > 100/min, il y a souvent superposition de \textbf{galops protodiastolique (GPD) et présystolique (B4)}. Intensité plus forte que GPD et B4, car sommation.

\subsection{Bruit protodiastolique d’origine péricardique}
Bruit de fréquence moins haute que COM, apparaissant 0.04 - 0.12 sec après A2 (quasi en même temps que COM) et variant en fonction de la respiration si \textbf{péricardite constrictive}.

\subsection{Frottement péricardique}
Bruit à 3 temps : contraction auriculaire, contraction ventriculaire et remplissage rapide protodiastolique du ventricule si \textbf{péricardite sans épanchement}.

\begin{center}
	\begingroup
		\centering
		\includegraphics[scale=0.4]{img/PER}
		\captionof{figure}{"Frottement péricardique." Contraction atriale (CA), contraction ventriculaire (VC) et relaxation ventriculaire (VR).}
	\endgroup
\end{center}




\section{Souffles systoliques}

\subsection{Fonctionnel}
Chez l’enfant et le jeune adulte un souffle protosystolique peut être physiologique. Maximum d’intensité dans le premier tiers de la systole et décroit rapidement en cas de \textbf{débit augmenté (efforts, fièvre, anémie, grossesse)}. Le souffle commence avec un bruit d’éjection en cas \textbf{d'hypertension pulmonaire avec dilatation de l’artère pulmonaire}.

\subsection{Sténose valvulaire (aortique ou pulmonaire)}
Maximum d’intensité avant le milieu de la systole si \textbf{sténose valvulaire modérée}. Maximum d’intensité vers la fin de la systole si \textbf{sténose valvulaire très serrée}.

\subsubsection{Sténose aortique}
Intensité maximalle au foyer aortique, irradie dans les carotides, varie avec le temps de diastole précédent (extrasystole, fibrillation auriculaire) et pas de différence d’intensité à l'inspiration. Clic d’éjection (CE) au début du souffle (signe de valve bicuspide et de mobilité valvulaire).

Intensité maximale en milieu ou plus tard dans la systole et dédoublement paradoxal du 2ème bruit (P2 – A2) si \textbf{sténose aortique serrée}.

\begin{center}
	\begingroup
		\centering
		\includegraphics[scale=1.2]{img/SVA}
		\captionof{figure}{"Stenose aortique."}
	\endgroup
\end{center}

\subsubsection{Sténose pulmonaire}
Souffle de forme losangique, peut dépasser A2 et intensité augmentée en inspiration.

\begin{center}
	\begingroup
		\centering
		\includegraphics[scale=1.2]{img/SVP}
		\captionof{figure}{"Sténose valvulaire pulmonaire."}
	\endgroup
\end{center}

Bruit d’éjection proche de B1 et souffle se termine après A2 si \textbf{sténose pulmonaire serrée}. Pas de bruit d’éjection si \textbf{sténose très sévère}, car la contraction de l'oreillette droite ouvre la valve pulmonaire précocément. Pas de bruit d’éjection et pas de P2 si \textbf{sténose pulmonaire infundibulaire sévère pure}.

\subsection{Coarctation aortique}
Souffle de haute fréquence, commence après le souffle de la sténose aortique, se termine après A2 et est maximal entre les deux scapulae.

\begin{center}
	\begingroup
		\centering
		\includegraphics[scale=0.3]{img/COA}
		\captionof{figure}{"Coarctation aortique."}
	\endgroup
\end{center}

\subsection{Régurgitation}
Holosystolique; commence à B1, dépasse A2.

\subsubsection{Insuffisance mitrale}
Souffle de haute fréquence commençant avec le premier bruit (souvent absent car la mitrale ne se ferme pas) à la pointe du cœur (5ème espace intercostal, ligne médioclaviculaire jusqu'à la région axillaire gauche). Pas de variation avec le cycle respiratoire, pas de variation avec le temps de diastole précédent (arythmie). 

Souffle télésystolique commençant par un clic mésosystolique ou télésystolique si \textbf{insuffisance mitrale modéré (prolapsus mitral)}. Galop et/ou bref roulement protodiastolique si \textbf{insuffisance mitrale sévère}. Diminution du souffle en post-extrasystole si \textbf{insuffisance mitrale sur origine ischémique du muscle papillaire}.

\begin{center}
	\begingroup
		\centering
		\includegraphics[scale=1.2]{img/IM}
		\captionof{figure}{"Insuffisance mitrale sévère."}
	\endgroup
\end{center}

\subsubsection{Insuffisance tricuspidienne}
Souffle maximal au 4ème espace intercostal droit, n’irradiant pas dans l’aisselle et d’intensité augmentée à l’inspiration. Expansion des veines du cou pendant la systole.

\begin{center}
	\begingroup
		\centering
		\includegraphics[scale=1.2]{img/IT}
		\captionof{figure}{"Insuffisance tricuspidienne."}
	\endgroup
\end{center}

\subsubsection{Communication interventriculaire}
Souflle sans variation à la respiration. Intensité forte, quelquefois frémissement palpable, souffle « râpeux », holosystolique si \textbf{petite communication interventriculaire}. Souffle terminé avant A2 si \textbf{petite communication interventriculaire dans le septum musculaire}.

\begin{center}
	\begingroup
		\centering
		\includegraphics[scale=1.1]{img/CIV1}
		\captionof{figure}{"Communication interventriculaire."}
	\endgroup
\end{center}

\begin{center}
	\begingroup
		\centering
		\includegraphics[scale=0.35]{img/CIV2}
		\captionof{figure}{"Communication interventriculaire."}
	\endgroup
\end{center}

\subsection{Cardiomyopathie hypertrophique obstructive}
Souffle commençant tardivement (0.1 sec après B1), de forme losangique, pas de clic d’éjection et intensité augmente au Valsalva.

\begin{center}
	\begingroup
		\centering
		\includegraphics[scale=1.2]{img/CMPO}
		\captionof{figure}{"Cardiomyopathie hypertrophique obstructive."}
	\endgroup
\end{center}

\subsection{Souffle de tétralogie de Fallot}
Souffle continu de haute fréquence si \textbf{augmentation du débit à travers les artères bronchiques}. L'intensité et  la durée du souffle sont proportionnels à la sténose pulmonaire. Bruit d’éjection aortique si \textbf{tétralogie sévère}.

\begin{center}
	\begingroup
		\centering
		\includegraphics[scale=1.1]{img/Fallot1}
		\captionof{figure}{"Tétralogie de Fallot."}
	\endgroup
\end{center}

Pas de souffle si \textbf{communication interventriculaire large}. Pas de bruit d’éjection si \textbf{sténose infundibulaire}.

\begin{center}
	\begingroup
		\centering
		\includegraphics[scale=1.1]{img/Eisenmerger1}
		\captionof{figure}{"Communication interventriculaire large (réaction d'Eisenmerger). Le deuxième bruit est large et unique."}
	\endgroup
\end{center}






\section{Souffles diastoliques}

\subsection{Insuffisance aortique}
Souffle de haute fréquence commencant avec A2, forme decrescendo, très rarement bruit de fermeture des valves précoce et pas de B1.

Souffle en première partie de diastole si \textbf{petite insuffisance}. Souffle holodiastolique en décroissance si \textbf{insuffisance moyenne}. Souffle se terminant avant la fin de la diastole si \textbf{insuffisance très sévère} à cause de l'équilibration des pressions aortiques et ventriculaire gauche.

\begin{center}
	\begingroup
		\centering
		\includegraphics[scale=1.2]{img/IA}
		\captionof{figure}{"Insuffisance aortique''. On doit le chercher en expiration, penché en avant.}
	\endgroup
\end{center}

\subsection{Insuffisance pulmonaire}
Souffle de haute fréquence commencant avec P2 si \textbf{hypertension pulmonaire chronique sur sténose mitrale très sévère}. 

Souffle retardé par rapport à P2 avec diminution d'intensité progressive et fréquence plus basse si \textbf{souffle d’origine organique sans hypertension pulmonaire}.

\begin{center}
	\begingroup
		\centering
		\includegraphics[scale=1.2]{img/IP}
		\captionof{figure}{"Insuffisance pulmonaire organique."}
	\endgroup
\end{center}

\subsection{Remplissage diastolique}
Roulement bref de basse fréquence. Intensité augmente en inspiration si \textbf{origine droite}; intensité stable avec la respiration si \textbf{origine gauche}.

\subsection{Sténose (mitrale ou tricuspidienne)}
Claquement d’ouverture (CO) suivi par un roulement intense en protodiastole décroissant si \textbf{sténose mitrale}. Roulement holodiastolique si \textbf{sténose sévère}. Intensité augmentée du roulement en télédiastole - renforcement présystolique si \textbf{rythme sinusal}. Un roulement présystolique en losange si \textbf{P - R (ECG) allongé}.

\begin{center}
	\begingroup
		\centering
		\includegraphics[scale=1.2]{img/SM}
		\captionof{figure}{"Sténose mitrale."}
	\endgroup
\end{center}

\subsection{Sans sténose}
Origine droite si \textbf{communication interauriculaire}, à cause du grand débit à travers la tricuspide et la pulmonaire. Origine gauche si \textbf{insuffisance mitrale ou communication interventriculaire}. B3 suivi d’un roulement protodiastolique si \textbf{insuffisance mitrale sévère} simulant un galop protodiastolique.

\begin{center}
	\begingroup
		\centering
		\includegraphics[scale=1.1]{img/CIA1}
		\captionof{figure}{"Communication interauriculaire."}
	\endgroup
\end{center}






\section{Souffles continus (en systole et en diastole)} 
Origine : \textbf{canal artériel persistant, fenêtre aorto-pulmonaire, rupture du sinus de Valsalva dans les cavités droites, fistule artério-veineuse coronaire, coarctation de l’aorte, fort débit bronchique (tétralogie de Fallot sévère)}.

Très rarement souffle continu en sous-clavière, chez les sujet jeunes, intensité maximale en protodiastole avec intensité augmentée à l’inspiration et disparaissant au Valsalva si \textbf{bruit de rouet (origine dans les veines qui rentrent dans le thorax)}.

\begin{center}
	\begingroup
		\centering
		\includegraphics[scale=1]{img/CA1}
		\captionof{figure}{"Canal artériel."}
	\endgroup
\end{center}






\section{Valves artificielles}

\subsection{Valve à disque}
Pas de bruit d’ouverture. Bruit de fermeture moins fort que valve à bille.

En position mitrale, roulement diastolique. Bruit de fermeture diminuée si \textbf{dysfonction ventriculaire}. Absence de bruit de fermeture si {thrombose ou fibrose}.

En position aortique, rare clic protosystolique, faible souffle diastolique et souffle systolique en losange.

\begin{center}
	\begingroup
		\centering
		\includegraphics[scale=1.5]{img/ValveD}
		\captionof{figure}{"Valve à disque en position mitrale avec souffle éjectionnel et roulement diastolique."}
	\endgroup
\end{center}

\subsection{Valve à double feuillet}
Bruit d’ouverture rare, net bruit de fermeture et souffles peu intenses.

\subsection{Valve tissulaire (souvent porcine)}
Bruits « croquants » de haute fréquence (moins que valves mécaniques) lors d’ouverture et de fermeture. Souffle de passage sanguin.

En position mitrale, bruit d’ouverture protodiastolique.

En position aortique, clic d’éjection protosystolique et souffle d’éjection en losange.

\begin{center}
	\begingroup
		\centering
		\includegraphics[scale=1.5]{img/ValveP}
		\captionof{figure}{"Valve porcine en position mitrale avec souffle éjectionnel, bruit d'ouverture protodiastolique et roulement diastolique."}
	\endgroup
\end{center}




\end{document}