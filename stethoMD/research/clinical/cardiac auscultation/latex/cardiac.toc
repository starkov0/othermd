\select@language {french}
\contentsline {section}{\numberline {1}Introduction}{2}
\contentsline {section}{\numberline {2}Types de bruits cardiaques}{2}
\contentsline {section}{\numberline {3}Bruits physiologiques}{2}
\contentsline {subsection}{\numberline {3.1}Premier bruit cardiaque}{2}
\contentsline {subsection}{\numberline {3.2}Deuxi\IeC {\`e}me bruit cardiaque}{3}
\contentsline {section}{\numberline {4}Bruits systoliques surajout\IeC {\'e}s}{3}
\contentsline {subsection}{\numberline {4.1}Clic protosystolique}{3}
\contentsline {subsection}{\numberline {4.2}Clic m\IeC {\'e}sot\IeC {\'e}l\IeC {\'e}systolique}{3}
\contentsline {section}{\numberline {5}Bruits diastoliques surajout\IeC {\'e}s}{4}
\contentsline {subsection}{\numberline {5.1}Claquement d\IeC {\textquoteright }ouverture mitrale (COM)}{4}
\contentsline {subsection}{\numberline {5.2}Claquement d\IeC {\textquoteright }ouverture tricuspidienne (COT)}{4}
\contentsline {subsection}{\numberline {5.3}Troisi\IeC {\`e}me bruit physiologique (B3)}{4}
\contentsline {subsection}{\numberline {5.4}Galop protodiastolique (GPD)}{4}
\contentsline {subsection}{\numberline {5.5}Galop pr\IeC {\'e}systolique (B4)}{4}
\contentsline {subsection}{\numberline {5.6}Galop de sommation (GS)}{4}
\contentsline {subsection}{\numberline {5.7}Bruit protodiastolique d\IeC {\textquoteright }origine p\IeC {\'e}ricardique}{4}
\contentsline {subsection}{\numberline {5.8}Frottement p\IeC {\'e}ricardique}{4}
\contentsline {section}{\numberline {6}Souffles systoliques}{5}
\contentsline {subsection}{\numberline {6.1}Fonctionnel}{5}
\contentsline {subsection}{\numberline {6.2}St\IeC {\'e}nose valvulaire (aortique ou pulmonaire)}{5}
\contentsline {subsubsection}{\numberline {6.2.1}St\IeC {\'e}nose aortique}{5}
\contentsline {subsubsection}{\numberline {6.2.2}St\IeC {\'e}nose pulmonaire}{5}
\contentsline {subsection}{\numberline {6.3}Coarctation aortique}{6}
\contentsline {subsection}{\numberline {6.4}R\IeC {\'e}gurgitation}{6}
\contentsline {subsubsection}{\numberline {6.4.1}Insuffisance mitrale}{6}
\contentsline {subsubsection}{\numberline {6.4.2}Insuffisance tricuspidienne}{6}
\contentsline {subsubsection}{\numberline {6.4.3}Communication interventriculaire}{7}
\contentsline {subsection}{\numberline {6.5}Cardiomyopathie hypertrophique obstructive}{7}
\contentsline {subsection}{\numberline {6.6}Souffle de t\IeC {\'e}tralogie de Fallot}{7}
\contentsline {section}{\numberline {7}Souffles diastoliques}{8}
\contentsline {subsection}{\numberline {7.1}Insuffisance aortique}{8}
\contentsline {subsection}{\numberline {7.2}Insuffisance pulmonaire}{8}
\contentsline {subsection}{\numberline {7.3}Remplissage diastolique}{8}
\contentsline {subsection}{\numberline {7.4}St\IeC {\'e}nose (mitrale ou tricuspidienne)}{8}
\contentsline {subsection}{\numberline {7.5}Sans st\IeC {\'e}nose}{9}
\contentsline {section}{\numberline {8}Souffles continus (en systole et en diastole)}{9}
\contentsline {section}{\numberline {9}Valves artificielles}{9}
\contentsline {subsection}{\numberline {9.1}Valve \IeC {\`a} disque}{9}
\contentsline {subsection}{\numberline {9.2}Valve \IeC {\`a} double feuillet}{10}
\contentsline {subsection}{\numberline {9.3}Valve tissulaire (souvent porcine)}{10}
