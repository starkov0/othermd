function result = myCorrelation(longVector,smallVector)

    longVector = reshape(longVector,max(size(longVector)),min(size(longVector)));
    smallVector = reshape(smallVector,max(size(smallVector)),min(size(smallVector)));
    
    result = zeros(size(longVector,1)+1,1);
    
    for i = 1:size(longVector,1) + 1 - size(smallVector,1)
        i
        i-1+size(smallVector,1) - i 
        result(i) = smallVector' * longVector(i:i-1+size(smallVector,1));
    end
    
end