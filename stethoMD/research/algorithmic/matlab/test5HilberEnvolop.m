t = 0:1e-4:1;
x = [1+cos(2*pi*50*t)].*cos(2*pi*1000*t);

% figure;
% plot(t,x); set(gca,'xlim',[0 0.1]);
% xlabel('Seconds'); ylabel('Amplitude');

y = hilbert(x);
env = abs(y);

% figure;
% plot(t,x); hold on;
% plot(t,abs(y),'r','linewidth',2);
% plot(t,-abs(y),'r','linewidth',2);
% set(gca,'xlim',[0 0.1]);
% xlabel('Seconds'); ylabel('Amplitude');

sound = audioread('sound/aortic stenosis2.wav'); 

sound = imresize(sound,[1000,1]);

sound = sound - mean(sound);

y = hilbert(sound);

subplot(2,1,1);
hold on
plot(sound);
plot(abs(y),'r','linewidth',2);
plot(-abs(y),'r','linewidth',2);
%set(gca,'xlim',[0 20000]);

subplot(2,1,2);
hold on
plot(abs(y),'r','linewidth',2);
plot(-abs(y),'r','linewidth',2);
%set(gca,'xlim',[0 20000]);

