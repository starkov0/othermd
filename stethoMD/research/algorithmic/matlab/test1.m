sound = audioread('sound/af.wav');   
click = sound(18000:21000);
click = click*3;

sound = audioread('sound/mitral-regurgitation.wav');
souffle = sound(8000:13000);
souffle(1:1000) = 0;
souffle(4000:end) = 0;

%sound = audioread('sound/mitral-regurgitation.wav');
sound = audioread('sound/aortic-stenosis.wav');
%sound = audioread('sound/pericardial-friction-rub.wav');
%sound = audioread('sound/af.wav');

clickLocation = xcorr(sound,click);
murmurLocation = xcorr(sound,souffle)*50;

figure;

subplot(6,1,1);
plot(sound);
title('Aortic stenosis','FontSize',20);

subplot(6,1,2);
plot(click);
title('Click','FontSize',20);

subplot(6,1,3);
plot(clickLocation(round(size(clickLocation,1)/2)-round(size(click,1)/2):end));
title('Click location','FontSize',20);

subplot(6,1,4);
plot(souffle);
title('Souffle','FontSize',20);

subplot(6,1,5);
plot(murmurLocation(round(size(murmurLocation,1)/2)-round(size(souffle,1)/2):end));
title('Souffle location','FontSize',20);

subplot(6,1,6);
hold on
plot(clickLocation(round(size(clickLocation,1)/2)-round(size(click,1)/2):end),'r');
plot(murmurLocation(round(size(murmurLocation,1)/2)-round(size(souffle,1)/2):end),'b');
hold off
title('Click - Souffle location','FontSize',20);

