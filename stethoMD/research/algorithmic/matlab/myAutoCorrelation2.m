function autoCorrSound = myAutoCorrelation2(sound)

    autoCorrSound = zeros(size(sound));
    
    for i=0:max(size(sound))-1
        for j=0:max(size(sound))-1
            if abs(j-i) == 0
                autoCorrSound(i+1) = autoCorrSound(i+1) + abs(sound(j+1) * sound(abs(j-i)+1));
            end
        end
    end

end