sound = audioread('sound/myHeartSound1.wav');
sound = sound(:,1);
% sound = sound(1:200000);

lb = -4; ub = 4; n = 3000;
[psi,x] = morlet(lb,ub,n); 

result = xcorr(sound,psi);
result = result(size(result,1)/2-length(psi)/2:end-length(psi)/2);

result = abs(result);
result = result - min(result);
result = result / max(result);

result1 = result > 0.2;
result2 = result .* result1;

result3 = zeros(size(result2));
result3(17:end) = result2(17:end) - result2(1:end-16);

result4 = abs(result3);

result5 = zeros(size(result4));
for i = 1:size(result4,1)-1000
    if sum(result4(i:i+1000)) > 10
        result5(i+500) = 1;
    else
        result5(i+500) = 0;
    end
end

result5Diff = zeros(size(result5));
result5Diff(2:end) = diff(result5);

localisation = [];
k = 1;
for i=1:size(result5Diff)
    if result5Diff(i) > 0
        localisation(k,1) = i;
    elseif result5Diff(i) < 0
        localisation(k,2) = i;
        k = k+1;
    end
end

duree = localisation(:,2) - localisation(:,1);
duree1 = duree - min(duree);
duree1 = duree1 / max(duree1);


energy = [];

for i=1:size(localisation,1)
    energy(i,1) = sum(sound(localisation(i,1):localisation(i,2)).^2);
end
energy1 = energy - min(energy);
energy1 = energy1 / max(energy1);

y = hilbert(sound);
y = y - min(y);
y = y / max(y);

figure;

subplot(2,1,1);
hold on
plot(sound);
plot(result5,'r--');
hold off

subplot(2,1,2);
hold on 
plot(duree1,'g-*');
plot(energy1,'r-o');
hold off



figure;
subplot(3,1,1);
plot(sound);
subplot(3,1,2);
plot(abs(y));
subplot(3,1,3);
plot(result);

