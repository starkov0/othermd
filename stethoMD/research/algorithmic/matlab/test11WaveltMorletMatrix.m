sound = audioread('sound/aortic regurgitation.wav');
sound = sound(:,1);
%sound = sound(1:1000000);
sound = imresize(sound,[100000,1]);
soundMean = mean(sound);
sound1 = sound - mean(sound);

sound = sound - min(sound);
sound = sound / max(sound);

le = 5000;
var = 100:100:1500;
hz = 1000:1000:10000;
waveletMatrix = zeros([max(size(hz)),max(size(var))]);


%%
for j = 1:max(size(var))
    for i = 1:max(size(hz))

        %i = 7; j = 3;
        lb = -var(j); ub = var(j); n = hz(i);
        [psi,~] = morlet(lb,ub,n); 

        result = xcorr(sound,psi);
        result = result(size(result,1)/2-length(psi)/2:end-length(psi)/2);

        result = abs(result);
        result = result - min(result);
        result = result / max(result);
        
        tmp1 = result >= 0.6;
        tmp2 = zeros(size(tmp1));
        for k = 101:max(size(tmp1))-100
            if sum(tmp1(k-100:k+100)) > 5
                tmp2(k) = 1;
            end
        end
        tmp3 = abs(tmp2 - 1);
        
        soundDiff = sound1 .* tmp3;
        
        waveletMatrix(i,j) = sum(soundDiff.^2)
        
    end
end

figure;
imagesc(waveletMatrix);


%%
[a,b] = min(waveletMatrix);
[c,d] = min(a);

q = 8; w = 2;
lb = -var(w); ub = var(w); n = hz(q);
%lb = -var(d); ub = var(d); n = hz(b(d));
[psi,x] = morlet(lb,ub,n); 

result = xcorr(sound,psi);
result = result(size(result,1)/2-length(psi)/2:end-length(psi)/2);

result = abs(result);
result = result - min(result);
result = result / max(result);

tmp1 = result >= 0.4;
tmp2 = zeros(size(tmp1));
for k = 101:max(size(tmp1))-100
    if sum(tmp1(k-100:k+100)) > 5
        tmp2(k) = 1;
    end
end
tmp3 = abs(tmp2 - 1);

soundDiff = sound1 .* tmp3;

figure;
hold on 
plot(sound1,'k');
plot(soundDiff,'r');
