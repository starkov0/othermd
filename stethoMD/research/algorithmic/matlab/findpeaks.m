function Y = findpeaks(X)


    dX = diff(X);
    ddX = diff(dX);
    dX = dX(2:end);
    X = X(3:end);
    
    Y1 = dX == 0;
    Y2 = ddX < 0;
    
    Y = Y1 == Y2;

end