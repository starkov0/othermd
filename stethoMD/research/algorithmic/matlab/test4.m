sound = audioread('sound/myHeartSound.wav'); 
sound = sound(:,1);
% sound = sound(1:max(size(sound))/20);
sound = imresize(sound,[1000,1]);
ll = 1000;
sound = sound(1:ll);

sound(485:510) = 0;
sound(320:350) = 0;
sound(170:195) = 0;
sound(650:680) = 0;
sound(60:80) = 0;

absSound = abs(sound);

energy = sum(sound.^2);

t = 0;
i = 1;
while t == 0
    if sum(sound(1:i).^2) < energy/2 && sum(sound(1:i+1).^2) > energy/2
        t = i;
    end
    i = i+1;
end

display(['t: ',num2str(t)]);

cumulativeEnergy = zeros(size(sound));
cumulativeEnergy(1) = absSound(1);
for i = 2:max(size(sound))
    cumulativeEnergy(i) = cumulativeEnergy(i-1) + absSound(i).^2;
end

cumulativeEnergy = cumulativeEnergy - min(cumulativeEnergy);
cumulativeEnergy = cumulativeEnergy / max(cumulativeEnergy);

figure;

subplot(2,1,1);
hold on
plot(sound)
plot(t,0,'r*')
hold off

subplot(2,1,2);
hold on
plot(cumulativeEnergy);
plot(t,0.5,'r*')



