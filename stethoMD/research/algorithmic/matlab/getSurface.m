function surfaceSpecs = getSurface(vector)

    % 1,: -> values
    % 2,: -> index
    index = 1;
    surfaceSpecs(index,1) = 0;
    surfaceSpecs(index,2) = 1;

    for i=11:size(vector,1)-11
        
        if vector(i-10) < vector(i) || vector(i+10) < vector(i)
            index = index + 1;
            surfaceSpecs(index,1) = vector(i);
            surfaceSpecs(index,2) = i;
        end
        
    end

end