clear


%% click1
sound = audioread('sound/af.wav');   
click1 = sound(18000:21000);

%click1 = imresize(click1,[size(click1,1)*2,1]);
click1 = click1 / max(click1);
% click2 = imresize(click1,[size(click1,1)*0.1,1]);
% click2 = click2 / max(click2);


%% sound
%sound = audioread('sound/aortic-stenosis.wav');
sound = audioread('sound/mitral-regurgitation.wav');
%sound = audioread('sound/05-mitral-regurgitation.mp3');
%sound = audioread('sound/pericardial-friction-rub.wav');   
%sound = audioread('sound/af.wav');
sound = sound / max(sound);


%% clickLocation1
clickLocation1 = xcorr(sound,click1);
clickLocation1 = clickLocation1(round(size(clickLocation1,1)/2)-round(size(click1,1)/2):end-round(size(click1,1)/2));
clickLocation1 = abs(clickLocation1);
clickLocation1 = clickLocation1 - min(clickLocation1);
clickLocation1 = clickLocation1 / max(clickLocation1);

figure;
hold on 
plot(sound,'k');
plot(clickLocation1,'r');
title('clickLocation1','FontSize',20);


%% ifftClickLocation1
fftClickLocation1 = fft(clickLocation1);
fftClickLocation1(300:end) = 0;

ifftClickLocation1 = real(ifft(fftClickLocation1));
ifftClickLocation1 = ifftClickLocation1 - min(ifftClickLocation1);
ifftClickLocation1 = ifftClickLocation1 / max(ifftClickLocation1);

significantIfftClickLocation1 = ifftClickLocation1 > 0.1;
ifftClickLocation1 = ifftClickLocation1 .* significantIfftClickLocation1;

figure;
hold on 
plot(sound,'k');
plot(ifftClickLocation1,'r');
title('ifftClickLocation1','FontSize',20);


%% diffIfftClickLocation1
diffIfftClickLocation1 = diff(ifftClickLocation1);
diffIfftClickLocation1 = diffIfftClickLocation1 / max(diffIfftClickLocation1);

for i=2:size(diffIfftClickLocation1,1)
    if abs(diffIfftClickLocation1(i) - diffIfftClickLocation1(i-1)) > 0.01
        diffIfftClickLocation1(i) = 0;
    end
end
diffIfftClickLocation1 = diffIfftClickLocation1 / max(diffIfftClickLocation1);

figure;
hold on 
plot(sound,'k');
plot(diffIfftClickLocation1,'r');
title('diffIfftClickLocation1','FontSize',20);


%% diffDiffIfftClickLocation1
diffDiffIfftClickLocation1 = diff(diffIfftClickLocation1);
diffDiffIfftClickLocation1 = diffDiffIfftClickLocation1 / max(diffDiffIfftClickLocation1);

for i=2:size(diffDiffIfftClickLocation1,1)
    if abs(diffDiffIfftClickLocation1(i) - diffDiffIfftClickLocation1(i-1)) > 0.01
        diffDiffIfftClickLocation1(i) = 0;
    end
end
diffDiffIfftClickLocation1 = diffDiffIfftClickLocation1 / max(diffDiffIfftClickLocation1);

figure;
hold on 
plot(sound,'k');
plot(diffDiffIfftClickLocation1,'r');
title('diffDiffIfftClickLocation1','FontSize',20);



%% result
clickLocation1 = clickLocation1(3:end);
diffIfftClickLocation1 = diffIfftClickLocation1(2:end);
result = getZeroLocation(diffIfftClickLocation1) & diffDiffIfftClickLocation1 < -0.05;

figure;
hold on
plot(sound,'k');
plot(result,'r');
title('result','FontSize',20);

