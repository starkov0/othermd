function result = myAutoCorrelation(vector)

    vector1 = reshape(vector,max(size(vector)),min(size(vector)));
    vector2 = [vector1;vector1];
    result = zeros(size(vector1,1)+1,1);
    
    for i = 1:size(vector1,1)+1
        result(i) = vector1' * vector2(i:i-1+size(vector1,1));
    end
    
end