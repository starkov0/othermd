%% sound
soundTot = audioread('../sound/myHeartSound2.wav');
soundTot = soundTot(:,1);
sound = soundTot(213000:985000)';

%% classes
s1 = [1.9,3.05,5.2,6.4,8.6,9.75] * 10^4;
s2 = [1.2,1.32,1.54,1.67,1.88,2,2.2,2.31,2.50,2.62,2.81,2.92,3.12,3.23] * 10^5;
s3 = [3.44,3.56,3.77,3.895,4.12,4.24] * 10^5;
s4 = [4.46,4.585,4.803,4.92,5.13,5.25,5.45,5.579,5.77,5.89,6.085,6.2,6.4,6.513,6.73,6.84,7.07,7.18,7.41,7.53] * 10^5;
s = [s1,s2,s3,s4];
e1 = [2.22,3.25,5.65,6.6,8.9,9.95] * 10^4;
e2 = [1.25,1.35,1.58,1.685,1.91,2.01,2.225,2.33,2.535,2.64,2.84,2.94,3.14,3.25] * 10^5;
e3 = [3.475,3.57,3.81,3.91,4.155,4.255] * 10^5;
e4 = [4.5,4.6,4.835,4.94,5.16,5.27,5.5,5.591,5.8,5.905,6.115,6.22,6.43,6.53,6.75,6.86,7.085,7.20,7.44,7.55] * 10^5;
e = [e1,e2,e3,e4];

wclasses = zeros(size(sound))';
for i=1:size(s,2)
    wclasses(s(i):e(i)) = ones(size(s(i):e(i)));
end
%wclasses = wclasses(1:770000);

sound = imresize(sound,[1,100000]);
wclasses = imresize(wclasses,[100000,1]);
wclasses = wclasses > 0;

% %% attributes
[Y,X] = pat2cwav(sound(32500:32800), 'orthconst', 1, 'none');

% Save the adapted wavelet and add it to the toolbox
delete adp_FRM2.mat
save adp_FRM2 X Y
wavemngr('del','AdapF1');
wavemngr('add','AdapF1','adpf1',4,'','adp_FRM2.mat',[0 1]);



wattributes11 = cwt(sound1,1:5:100,'db1')';
wattributes21 = cwt(sound1,20:3:60,'morl')';
wattributes31 = cwt(sound1,200:10:500,'adpf1')';
wattributes41 = cwt(sound1,10:3:80,'sym2')';
wattributes51 = cwt(sound1,10:3:80,'coif1')';
wattributes61 = cwt(sound1,10:3:70,'bior1.1')';
wattributes71 = cwt(sound1,1:30,'gaus1')';
wattributes81 = cwt(sound1,1:30,'mexh')';
wattributes91 = cwt(sound1,1:30,'cgau1')';
wattributes101 = cwt(sound1,30:2:100,'cmor1-1.5')';
wattributesTrain = [wattributes11, wattributes21, wattributes31, wattributes41,wattributes51,...
    wattributes61,wattributes71,wattributes81,wattributes91,wattributes101];
wattributesTrain = abs(wattributesTrain);
for i = 1:size(wattributesTrain,2)
    wattributesTrain(:,i) = wattributesTrain(:,i) - min(wattributesTrain(:,i));
    wattributesTrain(:,i) = wattributesTrain(:,i) / max(wattributesTrain(:,i));
end
wattributes = wattributesTrain;

% 
% wattributes1 = cwt(sound,1:0.5:100,'db1')';
% wattributes2 = cwt(sound,20:0.25:60,'morl')';
% wattributes3 = cwt(sound,200:2:500,'adpf1')';
% wattributes = [wattributes1, wattributes2, wattributes3];
% wattributes = abs(wattributes);
% for i = 1:size(wattributes,2)
%     wattributes(:,i) = wattributes(:,i) - min(wattributes(:,i));
%     wattributes(:,i) = wattributes(:,i) / max(wattributes(:,i));
% end
% figure;
% imagesc(wattributes);

%% data set creation
windowSize = 50;
iterations = size(wclasses,1) / windowSize;
classes = [];
attributes = [];

for i=1:iterations
    % classes
    tmpClasses = wclasses((i-1)*windowSize + 1:i*windowSize);
    classes(i) = sum(tmpClasses)/size(tmpClasses,1) > 0.99;
    
    % attributes
    tmpAttributes = wattributes((i-1)*windowSize + 1:i*windowSize,:)';
    attributes(i,:) = mean(tmpAttributes);
end
figure;
imagesc(attributes);

%% classify
kernel = {'linear'};
predictionsTot = [];
kfold = size(classes,2)/10;


for i = 1:size(kernel,1)
    for j = 1:10
        test = (j-1)*kfold+1:j*kfold;
        train = setdiff(1:size(classes,2),test);
        trainClasses = classes(train);
        trainAttributes = attributes(train,:);
        testClasses = classes(test);
        testAttributes = attributes(test,:);

        options.MaxIter = 100000;
        B = svmtrain(trainAttributes,trainClasses,'kernel_function',kernel{i},'Options', options);
        predictions = svmclassify(B,testAttributes);
        predictionsTot(i,test) = predictions';
    end
end

%% error
for i = 1:size(predictionsTot,1)
    error = 0;
    for j=1:size(predictionsTot,2)
        if predictionsTot(i,j) ~= classes(j)
            error = error + 1;
        end
    end
    display(['kernel : ', kernel{i}, ', error : ', num2str(error), ', error percent : ', num2str(error/size(predictionsTot,2))]);
end



%% display
predictionToDisplay = imresize(predictionsTot(1,:),size(sound));
predictionToDisplay = predictionToDisplay > 0;

tmp1 = sound .* predictionToDisplay;
tmp2 = sound .* wclasses';

figure;
subplot(211);
hold on
plot(sound,'k');
plot(tmp2,'b');
subplot(212);
hold on
plot(sound,'k');
plot(tmp1,'r');







