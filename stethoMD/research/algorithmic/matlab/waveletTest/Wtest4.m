x = [1 1 2 2 -3.5 -3.5 4.3 4.3 6 6 -4.5 -4.5 2.2 2.2 -1.5 -1.5];
x = repmat(x,1,64);
rng default;
x = x + 1000*randn(size(x));

figure;
plot(x);
set(gca,'xlim',[0 100]);

LS = liftwave('lazy');
[A,D] = lwt(x,LS);
x1 = ilwt(A,D,LS);

els = {'d',-1,0};
LSnew = addlift(LS,els);
[A,D] = lwt(x,LSnew);
x2 = ilwt(A,D,LSnew);

els = {'p',1/2, 0};
LSnew = addlift(LSnew,els);
[A,D] = lwt(x,LSnew);
x3 = ilwt(A,D,LSnew);

LSnew(end,:) = {sqrt(2),sqrt(2)/2,[]};
[A,D] = lwt(x,LSnew);
x4 = ilwt(A,D,LSnew);

x5 = ilwt(A,D,LSnew);
max(abs(x1-x))
max(abs(x2-x))
max(abs(x3-x))
max(abs(x4-x))
max(abs(x5-x))



figure;
subplot(5,1,1);
plot(x1);
set(gca,'xlim',[0 100]);
subplot(5,1,2);
plot(x2);
set(gca,'xlim',[0 100]);
subplot(5,1,3);
plot(x3);
set(gca,'xlim',[0 100]);
subplot(5,1,4);
plot(x4);
set(gca,'xlim',[0 100]);
subplot(5,1,5);
plot(x5);
set(gca,'xlim',[0 100]);










