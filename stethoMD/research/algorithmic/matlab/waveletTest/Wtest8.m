load vonkoch;

soundTot = audioread('../sound/myHeartSound2.wav');
soundTot = soundTot(:,1);
sound = imresize(soundTot,[100000,1]);
sound = sound(1:10000)';

scales = 2:2:128;
wname = 'coif3';
cwt(sound,scales,wname,'abslvl');
xlim([3250 4120]);
colormap(pink(128));