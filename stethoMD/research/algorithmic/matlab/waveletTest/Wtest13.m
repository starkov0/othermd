% Define the form to detect.
C1 = 1;
C2 = 0.9;
X = linspace(-1,1,256);
F = C1*(sin(pi*X)+abs(sin(pi*X)))/2 ...
    - C2*(sin(-pi*X)+abs(sin(-pi*X)))/2;

% Construct the signal containing two similar forms.
Z = zeros(1,2048);

% Insert the first form.
L = 128;
long  = 2*L;
first = 513;
last = first+long-1;
middle = (first+last)/2;
Z(first:last) = F;

% Insert the second form.
long2  = L;
first2 = 1473;
last2 = first2+long2-1;
middle2 = (first2+last2)/2;
Z(first2:last2) = sqrt(2)*F(1:2:end);

[Y,X] = pat2cwav(F,'orthconst',0,2);
% figure;
% plot(X,F/max(abs(F)),'b');
% hold on
% plot(X,Y/max(abs(Y)),'r');
% title('Form to detect (b) and adapted Wavelet (r)');

locdir = cd;
cd(tempdir);
save adp_FRM1 X Y
wavemngr('add','AdapF1','adpf1',4,'','adp_FRM1.mat',[0 1]);
addpath(tempdir,'-begin');
cd(locdir);

time = linspace(0,64,length(Z));
% figure;
% plot(time,Z);
% grid on;
% ax = gca;
% ax.XTick = [0 10 16 20 24 30 40 46 50];


stepSIG = 1/32;
stepWAV = 1/256;
wname = 'adpf1';
scales  = (1:2*long)*stepSIG;
WAV = {wname,stepWAV};
SIG = {Z,stepSIG};
figure;
tmp = cwt(SIG,scales,WAV);
imagesc(tmp);

