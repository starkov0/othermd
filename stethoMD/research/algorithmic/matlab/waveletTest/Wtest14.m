% Construct the signal containing two similar forms.
Z = zeros(1,768);

% Insert the first form.
L = 128;
long  = 2*L;
first = 253;
last = first+long-1;
Z(first:last) = F;

% Insert the second form.
L2 = 16;
long2  = 2*L2;
first2 = 353;
last2 = first2+long2-1;
Z(first2:last2) = Z(first2:last2)+2*sqrt(2)*F(1:8:end);

wname = 'adpf1';
stepSIG = 1/8;
stepWAV = 1/256;
scales = (1:2*long)*stepSIG;

figure;
SIG = {Z,stepSIG};
WAV = {wname,stepWAV};
tmp = cwt(SIG,scales,WAV);
imagesc(tmp);