% Define the signal to analyze.
clear
load sensor1;
long  = 4000;
first = 5000;
last  = first + long-1;
indices = (first:last);
dt = 1;
signal = sensor1(indices);
s1{1} = signal;
s1{2} = dt;

scales = 1:0.1:50;
wname  = 'mexh';
par    = 6;
WAV    = {wname,par};
cwt_s1_lin = cwtft(s1,'scales',scales,'wavelet',WAV);

cwt_s1_pow = cwtft(s1);

% Compute the energy distribution over scales.
cfs = cwt_s1_lin.cfs;
energy = sum(abs(cfs),2);
percentage = 100*energy/sum(energy);

[maxpercent,maxScaleIDX] = max(percentage)
scaMaxEner = scales(maxScaleIDX)

cwt_anomaly = cwt_s1_pow;

% Find the index of logarithmic scale detecting the anomaly.
[valMin,anomaly_index_scales] = min(abs(cwt_s1_pow.scales-scaMaxEner))


anomaly_cfs = cwt_s1_pow.cfs(anomaly_index_scales,:);
newCFS = zeros(size(cwt_s1_pow.cfs));
newCFS(anomaly_index_scales,:) = anomaly_cfs;
cwt_anomaly.cfs = newCFS;

% Reconstruction from the modified structure.
anomaly = icwtft(cwt_anomaly,'plot','signal',s1);









% First step for building the new structure corresponding to the anomaly.
cwt_anomaly = cwt_s1_lin;

% Choose a vector of scales centered on the most energetic scale.
dScale = 5;
anomaly_index_scales = (maxScaleIDX-dScale:maxScaleIDX+dScale);
anomaly_cfs = cwt_s1_lin.cfs(anomaly_index_scales,:);
newCFS = zeros(size(cwt_s1_lin.cfs));
newCFS(anomaly_index_scales,:) = anomaly_cfs;
cwt_anomaly.cfs = newCFS;

% Reconstruction from the modified structure.
anomaly = icwtlin(cwt_anomaly,'plot');





