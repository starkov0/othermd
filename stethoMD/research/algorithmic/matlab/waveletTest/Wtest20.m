%% sound
soundTot = audioread('../sound/myHeartSound2.wav');
soundTot = soundTot(:,1);
sound1 = soundTot(213000:985000)';

soundTot2 = audioread('../../sounds/newSounds1/a3.m4a');
soundTot2 = soundTot2(:,1);
ll = size(soundTot2);
soundTot2 = soundTot2(15000000:20000000);


start = 450000;
start = start + 772001;
start = start + 772001;
start = start + 772001;
start = start + 772001;
sound2 = soundTot2(start:start + 772001)';

figure;
plot(sound2);

%% classes
s1 = [1.9,3.05,5.2,6.4,8.6,9.75] * 10^4;
s2 = [1.2,1.32,1.54,1.67,1.88,2,2.2,2.31,2.50,2.62,2.81,2.92,3.12,3.23] * 10^5;
s3 = [3.44,3.555,3.77,3.895,4.12,4.233] * 10^5;
s4 = [4.46,4.585,4.803,4.92,5.135,5.25,5.45,5.575,5.77,5.89,6.085,6.2,6.4,6.513,6.72,6.84,7.07,7.18,7.41,7.53] * 10^5;
s = [s1,s2,s3,s4];
e1 = [2.22,3.25,5.65,6.6,8.9,9.95] * 10^4;
e2 = [1.25,1.35,1.59,1.685,1.92,2.01,2.235,2.33,2.54,2.64,2.845,2.94,3.15,3.25] * 10^5;
e3 = [3.475,3.57,3.817,3.91,4.155,4.255] * 10^5;
e4 = [4.5,4.6,4.851,4.94,5.17,5.275,5.5,5.591,5.81,5.905,6.115,6.22,6.44,6.53,6.76,6.86,7.105,7.20,7.44,7.55] * 10^5;
e = [e1,e2,e3,e4];

wclasses = zeros(size(sound1))';
for i=1:size(s,2)
    wclasses(s(i):e(i)) = ones(size(s(i):e(i)));
end

sound1 = imresize(sound1,[1,100000]);
sound2 = imresize(sound2,[1,100000]);
wclasses = imresize(wclasses,[100000,1]);
wclasses = wclasses > 0;

% %% attributes
[Y,X] = pat2cwav(sound1(32500:32800), 'orthconst', 1, 'none');

% Save the adapted wavelet and add it to the toolbox
delete adp_FRM2.mat
save adp_FRM2 X Y
wavemngr('del','AdapF1');
wavemngr('add','AdapF1','adpf1',4,'','adp_FRM2.mat',[0 1]);

wattributes1 = cwt(sound1,1:5:100,'db1')';
wattributes2 = cwt(sound1,20:3:60,'morl')';
wattributes3 = cwt(sound1,200:10:500,'adpf1')';
wattributes4 = cwt(sound1,10:3:80,'sym2')';
wattributes5 = cwt(sound1,10:3:80,'coif1')';
wattributes6 = cwt(sound1,10:3:70,'bior1.1')';
wattributes7 = cwt(sound1,1:30,'gaus1')';
wattributes8 = cwt(sound1,1:30,'mexh')';
wattributes9 = cwt(sound1,1:30,'cgau1')';
wattributes10 = cwt(sound1,30:2:100,'cmor1-1.5')';
wattributes11 = cwt(sound1,30:2:100,'shan1-1.5')';
wattributes12 = cwt(sound1,30:2:100,'fbsp1-1-1.5')';
wattributesTrain = [wattributes1, wattributes2, wattributes3, wattributes4,wattributes5,...
    wattributes6,wattributes7,wattributes8,wattributes9,wattributes10,wattributes11,wattributes12];
wattributesTrain = abs(wattributesTrain);

wattributes1 = cwt(sound2,1:5:100,'db1')';
wattributes2 = cwt(sound2,20:3:60,'morl')';
wattributes3 = cwt(sound2,200:10:500,'adpf1')';
wattributes4 = cwt(sound2,10:3:80,'sym2')';
wattributes5 = cwt(sound2,10:3:80,'coif1')';
wattributes6 = cwt(sound2,10:3:70,'bior1.1')';
wattributes7 = cwt(sound2,1:30,'gaus1')';
wattributes8 = cwt(sound2,1:30,'mexh')';
wattributes9 = cwt(sound2,1:30,'cgau1')';
wattributes10 = cwt(sound2,30:2:100,'cmor1-1.5')';
wattributes11 = cwt(sound2,30:2:100,'shan1-1.5')';
wattributes12 = cwt(sound2,30:2:100,'fbsp1-1-1.5')';
wattributesTest = [wattributes1, wattributes2, wattributes3, wattributes4,wattributes5,...
    wattributes6,wattributes7,wattributes8,wattributes9,wattributes10,wattributes11,wattributes12];
wattributesTest = abs(wattributesTest);

%% data set creation
% train & test
windowSize = 50;
iterations = size(wclasses,1) / windowSize;
classes = [];
attributesTrain = [];
attributesTest = [];

for i=1:iterations
    % classes
    tmpClasses = wclasses((i-1)*windowSize + 1:i*windowSize);
    classes(i) = sum(tmpClasses)/size(tmpClasses,1) > 0;
    
    % attributes
    tmpAttributes = wattributesTrain((i-1)*windowSize + 1:i*windowSize,:)';
    attributesTrain(i,:) = mean(tmpAttributes);
end

for i=1:iterations
    % attributes
    tmpAttributes = wattributesTest((i-1)*windowSize + 1:i*windowSize,:)';
    attributesTest(i,:) = mean(tmpAttributes);
end

%% classify
options.MaxIter = 100000;
B = svmtrain(attributesTrain,classes,'kernel_function','linear','Options', options);
predictionsTot = svmclassify(B,attributesTest)';

%% display
learningData = sound1 .* wclasses';
predictionToDisplay = imresize(predictionsTot,size(sound2));
predictionToDisplay = predictionToDisplay > 0;
predictionToDisplay = sound2 .* predictionToDisplay;

for i=1:size(learningData,2)
    if learningData(i) == 0
        learningData(i) = NaN;
    end
end
for i=1:size(predictionToDisplay,2)
    if predictionToDisplay(i) == 0
        predictionToDisplay(i) = NaN;
    end
end



figure;
subplot(211);
hold on
plot(sound1,'k');
plot(learningData,'r');
title('Training','FontSize',30);
subplot(212);
hold on
plot(sound2,'k');
plot(predictionToDisplay,'r');
title('Test','FontSize',30);

