rng default;
dwtmode('per');
n = 0:1023;
indices = (n>127 & n<=512);
x = cos(7*pi/8*n).*indices+0.5*randn(size(n));

T = cwt(x,1:100:1000,'sym4');

for i = 1:size(T,1)
    T(i,:) = sqrt(real(T(i,:)).^2 + imag(T(i,:)).^2);
    T(i,:) = T(i,:) - min(T(i,:));
    T(i,:) = T(i,:) / max(T(i,:));
end

imagesc(T);

