load eeg_3_01;
Z = eeg_3_01;

stepSIG = 1/200;       % sampling period of the EEG.
stepWAV = 1/1024;
stepSCA = 0.1;
lenSIG = length(Z);    % length of the record.
first  = 645;          % beginning of the pattern (spike).
middle = 663;          % middle of the pattern.
long   = 36;           % length of the pattern.
last   = first+long-1;
F = Z(first:last);     % pattern.

cd(tempdir);
wavemngr('add','AdpWave','adp',4,'7 8 9 10','adpwavf',[0 1]);
cd(locdir);

% Detection of the spike using the adapted wavelet.
scales = (1:stepSCA:2*long)*stepSIG;
positions = (0:lenSIG-1)*stepSIG;
SIG = {Z,stepSIG};          % Signal and sampling period.
WAV = {'adp8',stepWAV};     % Wavelet and sampling period.

% Find the CWT of the EEG signal using the pattern adapted wavelet.
fig = figure;
c = cwt(SIG,scales,WAV);
imagesc(c);
