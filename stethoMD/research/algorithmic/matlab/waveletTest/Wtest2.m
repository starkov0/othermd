Fs = 1000;
t = 0:1/Fs:1-1/Fs;
x = zeros(size(t));
x([625,750]) = 2.5;
x = x+ cos(2*pi*100*t).*(t<0.25)+cos(2*pi*50*t).*(t>=0.5)+0.15*randn(size(t));
figure;
plot(t,x);

ds = 0.15;
J = fix((1/ds)*log2(length(x)/8));
dt = 1/Fs;
scales = 2*dt*2.^((0:J).*ds);

cwtstruct = cwtft({x,0.001},'Scales',scales,'Wavelet','morl');
periods = cwtstruct.scales.*(4*pi)/(6+sqrt(38));
freq  = 1./periods;
cfs = cwtstruct.cfs;

figure;
contour(t,freq,abs(cfs));
set(gca,'xtick',[0 0.25 0.4 0.5 0.6 0.75 1]); grid on;
xlabel('Time (seconds)'); ylabel('Hz'); title('CWT Coefficient Moduli');

indices = find(scales>=0.007 & scales<=0.014);

icwtsin = cwtstruct;
icwtsin.cfs = zeros(size(cwtstruct.cfs));
icwtsin.cfs(indices,:) = cwtstruct.cfs(indices,:);
xrec = icwtft(icwtsin);
plot(t,x);
hold on;
plot(t,xrec,'r');
set(gca,'xlim',[0 0.4]);
legend('Original Signal','Inverse CWT Approximation',...
    'Location','NorthEast');

