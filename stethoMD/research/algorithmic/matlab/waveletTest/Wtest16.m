soundTot = audioread('../sound/myHeartSound2.wav');
soundTot = soundTot(:,1);
%sound = imresize(soundTot,[100000,1]);
sound = soundTot(100000:500000);

first  = 177000;
last   = 178500;
% first  = 4100;
% last   = 4450;
hold on
plot(sound,'k');
plot(first,0,'r*');
plot(last,0,'g*');

soundSegm = sound(first:last)';

[Y,X] = pat2cwav(soundSegm, 'orthconst', i, 'none');

% Save the adapted wavelet and add it to the toolbox
delete adp_FRM2.mat
save adp_FRM2 X Y
wavemngr('del','AdapF1');
wavemngr('add','AdapF1','adpf1',4,'','adp_FRM2.mat',[0 1]);

c = cwt(sound,1:10:1000,'adpf1');
c = abs(c);

for i = 1:size(c,1)
    c(i,:) = c(i,:) - min(c(i,:));
    c(i,:) = c(i,:) / max(c(i,:));
end

sound1 = abs(sound);
sound1 = sound1 - min(sound1);
sound1 = sound1 / max(sound1);

figure;
subplot(211);
plot(abs(sound));
subplot(212);
imagesc(c);

figure;
subplot(211);
plot(abs(sound));
subplot(212);
plot(c(80,:));

figure;
hold on
plot(c(80,:) > 0.1,'r');
plot(sound1,'b');
