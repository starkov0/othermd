% ===================================         
% Haar              		haar                    
% ===================================         
% Daubechies        		db                      
% ------------------------------              
% db1	db2	db3	db4	                            
% db5	db6	db7	db8	                            
% db9	db10	db**	                              
% ===================================         
% Symlets           		sym                     
% ------------------------------              
% sym2	sym3	sym4	sym5	                        
% sym6	sym7	sym8	sym**	                       
% ===================================         
% Coiflets          		coif                    
% ------------------------------              
% coif1	coif2	coif3	coif4	                    
% coif5	                                      
% ===================================         
% BiorSplines       		bior                    
% ------------------------------              
% bior1.1	bior1.3	bior1.5	bior2.2	            
% bior2.4	bior2.6	bior2.8	bior3.1	            
% bior3.3	bior3.5	bior3.7	bior3.9	            
% bior4.4	bior5.5	bior6.8	                    
% ===================================         
% ReverseBior       		rbio                    
% ------------------------------              
% rbio1.1	rbio1.3	rbio1.5	rbio2.2	            
% rbio2.4	rbio2.6	rbio2.8	rbio3.1	            
% rbio3.3	rbio3.5	rbio3.7	rbio3.9	            
% rbio4.4	rbio5.5	rbio6.8	                    
% ===================================         
% Meyer             		meyr                    
% ===================================         
% DMeyer            		dmey                    
% ===================================         
% Gaussian          		gaus                    
% ------------------------------              
% gaus1	gaus2	gaus3	gaus4	                    
% gaus5	gaus6	gaus7	gaus8	                    
% gaus**	                                     
% ===================================         
% Mexican_hat       		mexh                    
% ===================================         
% Morlet            		morl                    
% ===================================         
% Complex Gaussian  		cgau                    
% ------------------------------              
% cgau1	cgau2	cgau3	cgau4	                    
% cgau5	cgau**	                               
% ===================================         
% Shannon           		shan                    
% ------------------------------              
% shan1-1.5	shan1-1	shan1-0.5	shan1-0.1	      
% shan2-3	shan**	                             
% ===================================         
% Frequency B-Spline		fbsp                    
% ------------------------------              
% fbsp1-1-1.5	fbsp1-1-1	fbsp1-1-0.5	fbsp2-1-1	
% fbsp2-1-0.5	fbsp2-1-0.1	fbsp**	             
% ===================================         
% Complex Morlet    		cmor                    
% ------------------------------              
% cmor1-1.5	cmor1-1	cmor1-0.5	cmor1-1	        
% cmor1-0.5	cmor1-0.1	cmor**	                 
% ===================================   




soundTot = audioread('../sound/myHeartSound2.wav');
soundTot = soundTot(:,1);
sound = imresize(soundTot,[100000,1]);
%sound = sound(1:50000);

scales = 1:20:400;
% scales = 7;
Coeffs = cwt(sound,scales,'morl');

for i = 1:size(Coeffs,1)
    % Coeffs(i,:) = sqrt(real(Coeffs(i,:)).^2 + imag(Coeffs(i,:)).^2);
    Coeffs(i,:) = abs(Coeffs(i,:));
    Coeffs(i,:) = Coeffs(i,:) - min(Coeffs(i,:));
    Coeffs(i,:) = Coeffs(i,:) / max(Coeffs(i,:));
end

figure;
subplot(2,1,1);
plot(sound);
title('Original Sound','fontsize',20);
subplot(2,1,2);
imagesc(Coeffs);
title('Wavelet coefficients','fontsize',20);




% amplitude = sqrt(real(Coeffs).^2 + imag(Coeffs).^2);
% amplitude = amplitude - min(amplitude);
% amplitude = amplitude / max(amplitude);
% 
% sound = abs(sound);
% soudn = sound - min(sound);
% sound = sound / max(sound);
% 
% figure;
% subplot(3,1,1);
% plot(sound,'k');
% subplot(3,1,2);
% plot(amplitude,'r');
% subplot(3,1,3);
% hold on
% plot(sound,'k');
% plot(amplitude,'r');