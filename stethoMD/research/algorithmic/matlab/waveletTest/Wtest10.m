wname  = 'cgau2';
scales = 1:512;
ntw = 21;
t  = linspace(0,1,2048);
x = sin(16*pi*t)+0.25*randn(size(t));
y = 10*sin(16*pi*t+pi)+0.25*randn(size(t));
%wcoher(x,y,scales,wname,'ntw',ntw,'plot','cwt');

Freq = scal2frq(128,'cgau3',1/2048);
