
soundTot = audioread('../sound/myHeartSound2.wav');
soundTot = soundTot(:,1);
sound = imresize(soundTot,[1048576,1]);
%sound = sound(1:10112)';

swc = swt(sound,20,'sym8');


for i = 1:size(swc,1)
    swc(i,:) = sqrt(real(swc(i,:)).^2 + imag(swc(i,:)).^2);
    swc(i,:) = swc(i,:) - min(swc(i,:));
    swc(i,:) = swc(i,:) / max(swc(i,:));
end

subplot(211)
plot(sound);
subplot(212);
imagesc(swc);