load elec35_nor;       % Load normalized original data.
X = signals;
[nbSIG,nbVAL] = size(X);
plot(X','r');
axis tight
title('Original Data: 35 days of electrical consumption');
xlabel('Minutes from 1 to 1440');

dirDec = 'r';         % Direction of decomposition
level  = 9;           % Level of decomposition
wname  = 'sym4';      % Near symmetric wavelet
decROW = mdwtdec(dirDec,X,level,wname);

A7_ROW  = mdwtrec(decROW,'a',level);

subplot(2,1,1);
plot(X(:,:)','r');
title('Original Data');
axis tight
subplot(2,1,2);
plot(A7_ROW(:,:)','b');
axis tight
title('Corresponding approximations at level 7');