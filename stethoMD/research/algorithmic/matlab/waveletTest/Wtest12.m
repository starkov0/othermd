% Length of the signal and half-length of the pattern.
lenSIG = 897;
L = 45;

% Length, beginning, end and middle of the pattern.
long  = 2*L;
first = 340;
last  = first+long-1;
pat = [-ones(1,L) ones(1,L)];