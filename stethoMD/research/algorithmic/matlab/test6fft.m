sound = audioread('sound/aortic regurgitation.wav'); 
sound = sound(1:200000,1);
sound = imresize(sound,[1000,1]);


%% 

soundFFT = fft(sound);
soundFFTamplitude = sqrt(real(soundFFT).^2 + imag(soundFFT).^2);
soundFFTphase = atan(- imag(soundFFT) ./ real(soundFFT));

figure;
subplot(5,1,1);
plot(sound);
title('Simple FFT analysis 1','fontsize',20);
subplot(5,1,2);
plot(real(soundFFT));
subplot(5,1,3);
plot(imag(soundFFT));
subplot(5,1,4);
plot(soundFFTamplitude);
subplot(5,1,5);
plot(soundFFTphase);


%%

soundFFT(300:700) = 0;
soundFFTamplitude = sqrt(real(soundFFT).^2 + imag(soundFFT).^2);
soundFFTphase = atan(- imag(soundFFT) ./ real(soundFFT));

figure;
subplot(5,1,1);
plot(sound);
title('Simple FFT analysis 1','fontsize',20);
subplot(5,1,2);
plot(real(soundFFT));
subplot(5,1,3);
plot(imag(soundFFT));
subplot(5,1,4);
plot(soundFFTamplitude);
subplot(5,1,5);
plot(soundFFTphase);


%%

soundIFFT = ifft(soundFFT);
soundIFFTamplitude = sqrt(real(soundIFFT).^2 + imag(soundIFFT).^2);
soundIFFTphase = atan(- imag(soundIFFT) ./ real(soundIFFT));

figure;
subplot(5,1,1);
plot(sound);
title('Simple FFT analysis 2','fontsize',20);
subplot(5,1,2);
plot(real(soundIFFT));
subplot(5,1,3);
plot(imag(soundIFFT));
subplot(5,1,4);
plot(soundIFFTamplitude);
subplot(5,1,5);
plot(soundIFFTphase);

