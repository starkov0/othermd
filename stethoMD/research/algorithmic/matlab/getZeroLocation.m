function zeroLocation = getZeroLocation(vector)

    zeroLocation = zeros(size(vector));

    for i = 1:size(vector,1)-1
        if vector(i+1) < 0 || vector(i) < 0
            zeroLocation(i) = 1;
        end
    end

end