
module outter_large() {
	translate([-12.5,-12.5,0]) cube([25,25,60]);
	//cylinder(h=60,d=25);
}

module outter_small() {
	difference () {
		cylinder(h=10,d=5.5);
		translate([0,0,-1]) cylinder(h=12,d=3.0);
	}
}

module inner_small() {
	cylinder(h=3.3,d=3);
}

module inner_small_diagonal() {
	for(i = [0:0.05:7]) 
	{
		translate([0,-(6.5/7)*i,i]) cylinder(h=1,d=4);
	}
}

module inner_straight() {
	cylinder(h=40.2, d=4);
}


module inner_large_diagonal() {
	for(i = [0:0.05:7]) {
		translate([0,(i*9.5/7)/2,i]) cylinder(h=1,d=(i*9.5/7)+4);
	}
}

module inner_space() {
	inner_small_diagonal();
	inner_large_diagonal();
	translate([0,-6.5,7.1]) inner_straight();
	translate([0,-6.5,47]) rotate([0,0,180]) inner_small_diagonal();
	translate([0,0,-2.9]) inner_small();
	translate([0,0,47+7.5]) inner_small();
}

module in_out_transparant() {
	%outter_large();
	translate([0,-2,2.5]) inner_space();
}

module in_out_difference() {
	difference () {
		outter_large();
		translate([0,-2,2.5]) inner_space();
	}
}

module in_out_small_large() {
	in_out_difference();
	translate([0,-2,-9.9]) outter_small();
	translate([0,-2,59.9]) outter_small();
}

module final() {
	difference() {
		in_out_small_large();
		translate([-7,9.5/2-9,10.3]) cube([14,20,40]);
	}
}
rotate([0,90,0]) final();