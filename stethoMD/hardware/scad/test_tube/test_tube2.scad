$fn=100;

module piece() {
	cylinder(h=50,d=5.5);
	translate([-2.75,-2.75,-5]) cube([5.5,5.5,5]);
	translate([-2.75,-2.75,50]) cube([5.5,5.5,5]);
}



difference(){
	piece();
	translate([0,0,-10]) cylinder(h=70,d=3);
}
