$fn=100;

module small() {
	difference(){
		cylinder(h=10,d=5.5);
		translate([0,0,-20]) cylinder(h=40,d=3);
	}
}

module large() {
	difference(){
		cylinder(h=5,d=16);
		translate([0,0,-20]) cylinder(h=40,d=3);
	}
}

module cone() {
	for(i = [0:0.5:3]) 
		{
			translate([0,0,i]) cylinder(h=1,d=6+2*i);
		}
}

module cone_trou() {
	difference() {
		cone();
		translate([0,0,-1]) cylinder(h=6,d=3);
	}
}

module final() {
	small();
	translate([0,0,6]) cone_trou();
	translate([0,0,10]) large();
}

small();
translate([0,0,10]) small();
translate([0,0,-25]) final();
rotate([0,180,0]) final();
translate([0,0,-27.5]) cylinder(h=3,d=5.5);